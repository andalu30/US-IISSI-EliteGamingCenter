<?php
session_start();

    //Comprobamos si hay algun login abierto
    if (isset($_SESSION["login"])) {
        $login = $_SESSION["login"];
    }else{
        $login = 'No se ha iniciado sesión';
    }
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <link rel="stylesheet" href="css/contacto.css" type="text/css" />

	<title>Contacto - Elite Gaming Center</title>
</head>
	<body>
    <header>
      <?php
				include_once("cabecera.php");
			?>
    </header>

		<main>
      <div class="compañiaReal">
        <h1>Compañia real:</h1>

        <div class="persona">
          <p>
            Para contactar con la compañia real, Elite Gaming Center, por favor dirigase al siguiente enlace.</br>
            Tenga en cuenta que este trabajo completo solo esta inspirado por la empresa
            y dado que los integrantes del grupo no guardan relacion alguna con ella,
            absolutamente nada de lo que se puede encontrar en el trabajo se puede atribuir a la empresa.</br>
            En resumen, que si le das a comprar a algo en esta tienda no te vayas a quejar a la empresa real
            si los pedidos no te los entregan ya que este trabajo no tiene conexion alguna con la tienda real de la empresa.

          </p>
          <p>
            Para contactar con la compañia real por favor contacte a través de este enlace que lleva a su pagina web.
          </p>
          <a href="http://elitegamingcenter.com/contacto">www.elitegamingcenter.com/contacto</a>
        </div>
        </div>



      <div>
          <h1>Creadores del trabajo:</h1>

            <div class="persona">
                <h2>Juan Arteaga Carmona</h2>
                <div class="text">
                  <h4>Estudiante de ingeniería informática de la Universidad de Sevilla.</h4>
                  <p>
                    Apasionado de la informática y los videojuegos desde pequeño,
                     Juan siempre ha tenido un teclado entre sus manos, cosa que
                     no cambió cuando entro a la universidad en 2015. Durante este
                     periodo de formación, afianzó sus conocimientos sobre
                     programacion y comenzo a desarrollar en Java.</br>
                    Actualmente se encuentra estudiando otros lenguajes de programación como Python
                    y Kotlin y tiene la mirada puesta en proyectos de inteligencia artificial, análisis
                    de video en tiempo real, domótica y programacion en Android.
                  </p>
                </div>

                <div class="contenedor_imagen">
                    <img class="avatar" src="res/images/avatares/juan.jpg"/>
                    <div class="enlaces">
                      <a href="https://twitter.com/andalu30">Twitter</a>
                      <a href="https://github.com/andalu30">GitHub</a>
                      <a href="https://andalu30.github.io">www.andalu30.github.io</a>
                    </div>
                </div>
            </div>

            <div class="persona">
                <h2>Francisco José Alonso Parejo</h2>
                <div class="text">
                  <h4>Estudiante de ingeniería informática de la Universidad de Sevilla.</h4>
                  <p><!-- Intenta poner las mismas lineas que yo para que se quede igualado -->
                 Fran es un chico con intelecto, pero reconoce que llegó al mundo de la informática
                 no por una pasión contínua desde pequeño, el simplemente llegó al mundo de las TIC
                 teniendo un conocimiento muy limitado sobre la misma, y acto
                 seguido de conocer tal mundo, se enamoró de las Tecnologías. Es un chico con iniciativa, y sobre todo,
                 con ganas de aprender para alcanzar grandes metas. </br> </br> </br>

                  </p>
                </div>

                <div class="contenedor_imagen">
                    <img class="avatar" src="res/images/avatares/fran.jpg"/>
                    <div class="enlaces">
                      <a href="https://www.facebook.com/franalonso13">Facebook</a>
                      <a href="https://www.instagram.com/franalonso13/">Instagram</a>
                    </div>
                </div>
            </div>

      </div>


		</main>
    <footer>
      <?php
        include_once("pie.php");
      ?>
    </footer>
	</body>
</html>
