<?php
session_start();
require_once('gestionBD.php');
require_once('gestionEntradas/gestionReservas.php');

    if (isset($_SESSION["login"])) {
        $login = $_SESSION["login"];
    }else{
        $login = 'No se ha iniciado sesión';
    }


    $conexion = crearConexionBD();
    $pcs = consultarTodosPCS($conexion);
    $numeroPCs = count($pcs);
    cerrarConexionBD($conexion);


require_once("cabecera.php");
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Realiza reserva</title>
        <meta name="description" content="">
        <meta name="author" content="andalu30">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/realizaReserva.css" type="text/css" />
    </head>

    <body>
        <div>
            <header>
                <h1>Realiza reserva</h1>
                <p>Solo podrás reservar un dispositivo cada hora.</br>
                  Las reservas se reiniciarán a las 12 de la noche.</br>
                  Obviamente no podrás reservar un dispositivo a una hora que ya ha pasado.</p>
            </header>

              <div id="containerTable-responsive">
                <table id="horasAM">
                  <tr>
                    <th>Dispositivo</th>
                    <?php for ($i=0; $i < 24; $i++) {
                      ?><th><?php echo $i ?>:00 - <?php echo $i+1?>:00</th><?php
                    } ?>
                  </tr>

                  <?php
                    foreach ($pcs as $pc) {
                      ?>

                      <tr>
                      <td><img width="100em" src="res/images/tipo_pc/<?php echo $pc["TIPO_PC"];?>.jpg"/></td>
                      <?php for ($i=0; $i < 24; $i++) {
                        ?><td><a href="reserva_form.php?oid=<?php echo $pc["OID_PC"]?>&horainicio=<?php echo $i ?>:00"><button id="botonReserva<?php echo $pc["OID_PC"].$i ?>" class="disponible">Reservar</button></a></td>
                        <?php
                        $conexion = crearConexionBD();
                        $hoy = date("d-m-Y");
                        $horainicio = $i.':00';
                        $horainicioFuncion = $hoy.' '.$horainicio;
                        //echo $horainicioFuncion ."<- Hoy \n  Consulta-> \n";
                        $consulta = consultaEstadoReserva($conexion, $pc["OID_PC"], $horainicioFuncion);
                        //echo $consulta ."\n";
                        cerrarConexionBD($conexion);

                        $ahora = date('H');
                        if ($ahora+1 > $i) {
                          $consulta=1;
                        }

                        if ($consulta==1) {
                          ?><script>
                            var boton = document.getElementById("botonReserva<?php echo $pc["OID_PC"].$i ?>");
                            boton.className = 'ocupado';
                            boton.innerHTML = 'No Disponible';
                            boton.disabled = true;
                          </script>
                          <?php
                        }
                      } ?>
                      </tr>
                      <?php
                    }
                   ?>
                </table>
              </div>

            <footer>
                <?php require_once 'pie.php'; ?>
            </footer>
        </div>
    </body>
</html>
