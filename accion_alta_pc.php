<?php
	session_start();

   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';

	 if (isset($_SESSION['errores'])) {
	 	unset($_SESSION['errores']);
	 }


	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevaAmonestacion["estado"] = $_REQUEST["estado"];
		$nuevaAmonestacion["tipo"] = $_REQUEST["tipo"];
		$nuevaAmonestacion["precio"] = $_REQUEST["precio"];

	}
	else{ // En caso contrario, vamos al formulario
		header("Location: AnadirPCs_form.php");
	}
	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevaAmonestacion;

	// Validamos el formulario en servidor
	$errores = validarPC($nuevaAmonestacion);


	// Si se han detectado errores
	if (count($errores)>0) {
		foreach ($errores as $err){
			echo $err;
		}



		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirPCs_form.php');
	} else {

		//echo $_SESSION['estado'],$_SESSION['tipo'],$_SESSION['precio'];
		//echo $nuevaAmonestacion["estado"];
		// Si todo va bien, vamos a la página de éxito
		Header("Location: AnadirPC_submit.php");
	}






	function validarPC($nuevaAmonestacion){
		// Validación del estado
		if($nuevaAmonestacion["estado"]!="Disponible"){
			if ($nuevaAmonestacion["estado"]!="No Disponible") {
				if ($nuevaAmonestacion["estado"]!="En reparacion") {
					$errores['estado'] = "<p>No se como te las has apañado pero ese estado no es valido.
																Usa el desplegable, no toques las URLs</p>";
					}
			}
		}

		//Validacion del tipo
		if($nuevaAmonestacion["tipo"]!="Normal"){
			if ($nuevaAmonestacion["tipo"]!="VIP") {
				if ($nuevaAmonestacion["tipo"]!="Switch") {
					if ($nuevaAmonestacion["tipo"]!="PS3") {
						if ($nuevaAmonestacion["tipo"]!="PS4") {
							if ($nuevaAmonestacion["tipo"]!="XBONE") {
								if ($nuevaAmonestacion["tipo"]!="Oculus") {
									if ($nuevaAmonestacion["tipo"]!="HTCViv") {
										$errores['tipo'] = "<p>Ya la has liado... usa el desplegable por dios, que no es tan dificil...</p>";
									}
								}
							}
						}
					}
				}
			}
		}

		//Validacion del precio
		if($nuevaAmonestacion["precio"]<=0){
			$errores["precio"]="<p>Un dispositivo con precio igual a 0 o negativo...
													que quieres que entremos en quiebra? Reza para que
													no te despida el jefe... </p>";
		}

		return $errores;
	}

?>
