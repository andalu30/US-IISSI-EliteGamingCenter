<?php

require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

session_start();

if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
	// Recogemos los datos del formulario
		$nuevaReparacion= $_SESSION["formulario"];
		unset($_SESSION["formulario"]);
		
		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
		}	
		
	$conexion = crearConexionBD();
	$insercion = insertarReparacion($conexion,$nuevaReparacion);
	cerrarConexionBD($conexion);
	
	}else{	
		Header("Location: AnadirReparaciones_form.php");
	}

?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/testcss.css" type="text/css" />

  <title>Añadir REPARACION - Submit</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>
	
		<?php if ($insercion <> "") {
		?><h1>Error al meter los datos en la base de datos.</h1>
			<?php	 echo $insercion;
	}else{?>
  <script>
    window.alert("Se ha añadido la Reparacion");
    window.location.replace("reparacionesPaginado.php");
  </script>
  <?php } ?>
</body>
</html>
