<?php


	
	session_start();
	
   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevoCliente["dni"] = $_REQUEST["dni"];
		$nuevoCliente["nombre"] = $_REQUEST["nombre"];
		$nuevoCliente["apellidos"] = $_REQUEST["apellidos"];
		$nuevoCliente["email"] = $_REQUEST["email"];
		$nuevoCliente["direccion"] = $_REQUEST["direccion"];
		$nuevoCliente["fechanac"] = $_REQUEST["fechanac"];
		$nuevoCliente["pass"] = $_REQUEST["pass"];
		$nuevoCliente["confirmpass"] = $_REQUEST["confirmpass"];
		$nuevoCliente["telefono"] = $_REQUEST["telefono"];
		$nuevoCliente["nick"] = $_REQUEST["nick"];
	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirClientes_formulario.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevoCliente;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevoCliente);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirClientes_formulario.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirClientes_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevoCliente){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarEmail=consultarEmail($conexion,$nuevoCliente["email"]);
	 $consultarNickname=consultarNickname($conexion,$nuevoCliente['nick']);
	 cerrarConexionBD($conexion);
	 
		// Validación del DNI 
		if($nuevoCliente["dni"]=="") 
			$errores['dni'] = "<p>El DNI no puede estar vacío</p>";
		else if(!preg_match("/^[0-9]{8}[A-Z]$/", $nuevoCliente["dni"])){
			$errores['dni'] = "<p>El NIF debe contener 8 números y una letra mayúscula: " . $nuevoCliente["dni"]. "</p>";
		}

		// Validación del Nombre			
		if($nuevoCliente["nombre"]=="") 
			$errores['nombre'] = "<p>El nombre no puede estar vacío</p>";
		//	// Validación de los apellidos			
		if($nuevoCliente["nombre"]=="") 
			$errores['apellidos'] = "<p>Los apellidos no puede estar vacío</p>";
	
		// Validación del email
		if($nuevoCliente["email"]==""){ 
			$errores['email'] = "<p>El email no puede estar vacío</p>";
		}else if(!filter_var($nuevoCliente["email"], FILTER_VALIDATE_EMAIL)){
			$errores['email'] = $error . "<p>El email es incorrecto: " . $nuevoCliente["email"]. "</p>";
			
		}else if($consultarEmail==1 ){
			$errores['email'] = "<p>YA ESXISTE UN USUARIO CON ESE EMAIL REGISTRADO</p>";
		}
		// Validación del direccion			
		if($nuevoCliente["direccion"]=="") 
			$errores['direccion'] = "<p>La direccion no puede estar vacía</p>";
		
		// Validación del Nickname			
		if($nuevoCliente["nick"]==""){
			$errores['nick'] = "<p>El nickname no puede estar vacío</p>";
		}else if($consultarNickname==1){
			$errores['nick'] = "<p>YA ESXISTE UN CLIENTE CON ESE NICKNAME REGISTRADO</p>";
		}
			
		
		// Validación del numero de telefono
		if($nuevoCliente['telefono']==""){
		     $errores['telefono']= "<p>El teléfono tiene que completarse</p>";
		}else if (!preg_match("/^[0-9]{9}$/",$nuevoCliente["telefono"])){
			$errores['telefono']="<p> El telefono tiene menos de 9 cifras:".$nuevoCliente["telefono"]."</p>";
		}
		
		// Validación de la contraseña
		if($nuevoCliente['pass']==""){
			$errores['contraseña']="<p> La contraseña tiene que completarse</p>";
         }else if(!isset($nuevoCliente["pass"]) || strlen($nuevoCliente["pass"])<8){
			$errores ['contraseña'] = "<p>Contraseña no válida: debe tener al menos 8 caracteres</p>";
		}else if(!preg_match("/[a-z]+/", $nuevoCliente["pass"]) || 
			 !preg_match("/[0-9]+/", $nuevoCliente["pass"])){
			$errores['contraseña'] = "<p>Contraseña no válida: debe contener letras y dígitos</p>";
		}else if($nuevoCliente["pass"] != $nuevoCliente["confirmpass"]){
			$errores['contraseña1'] = "<p>La confirmación de contraseña no coincide con la contraseña</p>";
		}
	
		return $errores;
	}

?>

