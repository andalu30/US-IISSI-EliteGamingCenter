<?php
	session_start();
	require_once 'gestionEntradas/gestionFormularios.php';
	require_once 'gestionBD.php';
	
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevoTrabajador["dni"] = $_REQUEST["dni"];
		$nuevoTrabajador["nombre"] = $_REQUEST["nombre"];
		$nuevoTrabajador["apellidos"] = $_REQUEST["apellidos"];
		$nuevoTrabajador["email"] = $_REQUEST["email"];
		$nuevoTrabajador["direccion"] = $_REQUEST["direccion"];
		$nuevoTrabajador["fechanac"] = $_REQUEST["fechanac"];
		$nuevoTrabajador["pass"] = $_REQUEST["pass"];
		$nuevoTrabajador["confirmpass"] = $_REQUEST["confirmpass"];
		$nuevoTrabajador["telefono"] = $_REQUEST["telefono"];
		$nuevoTrabajador["numSS"] = $_REQUEST["numSS"];
		$nuevoTrabajador["cuenta"] = $_REQUEST["cuenta"];
		$nuevoTrabajador["inicioContrato"] = $_REQUEST["inicioContrato"];
		$nuevoTrabajador["finContrato"] = $_REQUEST["finContrato"];
		$nuevoTrabajador["salario"] = $_REQUEST["salario"];
		$nuevoTrabajador["esAdmin"] = $_REQUEST["esAdmin"];

	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirTrabajadores_formulario.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevoTrabajador;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevoTrabajador);
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirTrabajadores_formulario.php');
	} else{
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirTrabajadores_submit.php');

	}
	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevoTrabajador){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarEmail=consultarEmail($conexion,$nuevoTrabajador["email"]);
	 $consultarNumSS=consultarNumSS($conexion,$nuevoTrabajador["numSS"]);
	 cerrarConexionBD($conexion);
	 
		// Validación del DNI 
		if($nuevoTrabajador["dni"]=="") 
			$errores['dni'] = "<p>El DNI no puede estar vacío</p>";
		else if(!preg_match("/^[0-9]{8}[A-Z]$/", $nuevoTrabajador["dni"])){
			$errores['dni'] = "<p>El NIF debe contener 8 números y una letra mayúscula: " . $nuevoTrabajador["dni"]. "</p>";
		}

		// Validación del Nombre			
		if($nuevoTrabajador["nombre"]=="") 
			$errores['nombre'] = "<p>El nombre no puede estar vacío</p>";
		//	// Validación de los apellidos			
		if($nuevoTrabajador["nombre"]=="") 
			$errores['apellidos'] = "<p>Los apellidos no puede estar vacío</p>";
	
		// Validación del email
		if($nuevoTrabajador["email"]==""){ 
			$errores['email'] = "<p>El email no puede estar vacío</p>";
		}else if(!filter_var($nuevoTrabajador["email"], FILTER_VALIDATE_EMAIL)){
			$errores['email'] = $error . "<p>El email es incorrecto: " . $nuevoTrabajador["email"]. "</p>";
		}else if($consultarEmail==1 ){
			$errores['email'] = "<p>YA ESXISTE UN TRABAJADOR CON ESE EMAIL REGISTRADO</p>";
		}
		// Validación del direccion			
		if($nuevoTrabajador["direccion"]=="") 
			$errores['direccion'] = "<p>La direccion no puede estar vacía</p>";
		
		// Validación del numero de telefono
		if($nuevoTrabajador['telefono']==""){
		     $errores['telefono']= "<p>El teléfono tiene que completarse</p>";
		}else if (!preg_match("/^[0-9]{9}$/",$nuevoTrabajador["telefono"])){
			$errores['telefono']="<p> El telefono tiene menos de 9 cifras:".$nuevoTrabajador["telefono"]."</p>";
		}
		
		// Validación de la contraseña
		if($nuevoTrabajador['pass']==""){
			$errores['contraseña']="<p> La contraseña tiene que completarse</p>";
         }else if(!isset($nuevoTrabajador["pass"]) || strlen($nuevoTrabajador["pass"])<8){
			$errores ['contraseña'] = "<p>Contraseña no válida: debe tener al menos 8 caracteres</p>";
		}else if(!preg_match("/[a-z]+/", $nuevoTrabajador["pass"]) || 
			 !preg_match("/[0-9]+/", $nuevoTrabajador["pass"])){
			$errores['contraseña'] = "<p>Contraseña no válida: debe contener letras y dígitos</p>";
		}else if($nuevoTrabajador["pass"] != $nuevoTrabajador["confirmpass"]){
			$errores['contraseña1'] = "<p>La confirmación de contraseña no coincide con la contraseña</p>";
		}
		
		//validación del númerod e Seguridad Social
	    if($nuevoTrabajador['numSS']==""){
	    	$errores['numSS']="<p> El número de Seguridad Social tiene que completarse";
		}else if (!preg_match("/[0-9]+/",$nuevoTrabajador["numSS"])){
			$errores['numSS']="<p> Solo se pueden introducir digitos:".$nuevoTrabajador["numSS"]."</p>";
		}else if(!isset($nuevoTrabajador["numSS"]) || strlen($nuevoTrabajador["numSS"])<11){
			$errores['numSS']="<p> Se han introducido menos de 11 dígitos:".$nuevoTrabajador["numSS"]."</p>";	
		}else if($consultarNumSS==1){
			$errores['numSS'] = "<p>YA ESXISTE UN TRABAJADOR CON ESE NUMERO DE SS </p>";
		}
		//validación de numero de cuenta (IBAN)
		
	   if($nuevoTrabajador['cuenta']==""){
	   		$errores['cuenta']="<p> La cuenta no puede estar vacia</p>";
		
	   }else if(!preg_match("/[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}/",$nuevoTrabajador['cuenta'])){
	   		$errores['cuenta']="<p> Esto no es una cuenta IBAN</p>";
	   }
	   //validacion fecha contrato
	   if($nuevoTrabajador['inicioContrato']=="" or $nuevoTrabajador['finContrato']==""){
	   		$errores['contrato']="<p> Las fecha de contrato tiene que completarse</p>";
	   	}else if($nuevoTrabajador['finContrato']<$nuevoTrabajador['inicioContrato']){
	   		$errores['contratoTiempo']="<p>La fecha de fin del contrato tiene que ser posterior a la fecha de inicio</p>";
	   }
	 //validación salario
	  if($nuevoTrabajador['salario']==""){
	   		$errores['salario']="<p> El salario no puede estar vacio</p>";
	  }
	//validación es admin
	 if($nuevoTrabajador['esAdmin']!="SI" && $nuevoTrabajador['esAdmin']!="NO"){
	 	$errores['esAdmin']="<p>Solo se puede insertar si o no </p>";
	 }
	 
	 return $errores;
	}

	
?>

