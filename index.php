<?php
session_start();

    if (isset($_SESSION["error"])) {
        unset($_SESSION["error"]);
    }

		if(isset($_SESSION["formulario"])){
	      unset($_SESSION["formulario"]);
		}
		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
		}


    //Comprobamos si hay algun login abierto
    if (isset($_SESSION["login"])) {
        $login = $_SESSION["login"];
    }else{
        $login = 'No se ha iniciado sesión';
    }
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <link rel="stylesheet" href="css/index.css" type="text/css" />
  <!--Iconos de Google-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<title>Inicio - Elite Gaming Center</title>
</head>


	<body>
			<?php
				include_once("cabecera.php");
			?>
		<main>

      <div id="contenedorVideo">
        <iframe id="video" width="560" height="315" src="https://www.youtube-nocookie.com/embed/bwFRGcq2TJc?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>



    <div class="columnas" id="TituloSlogan">
      <h1 class="nombre">Elite Gaming Center</h1>
      <h3 class="slogan">Tu centro de alto rendimiento</h3>
    </div>

    <div class="columnas" id="tablatres">
      <div class="trescolumnas">
          <div>
              <h1 class="iconosgoogle">
                  <i class="material-icons">flash_on</i>
              </h1>
              <h2>Potencia como filosofía</h2>
              <p>En Elite Gaming Center creemos que debemos de ofrecerte lo mejor,
                  por eso tienes a tu disposicion los mejores equipos de Sevilla. Actualizados, optimizados
                  y puestos al dia para que no te den ningun problema durante tus intensas partidas de promocion a diamante.</p>
          </div>
      </div>

      <div class="trescolumnas">
          <div>
              <h1 class="iconosgoogle">
                  <i class="material-icons">games</i>
              </h1>
              <h2>Gran variedad de juegos</h2>
              <p>Los eSports no son solamente LoL y CS:GO, lo entendemos. Por eso te ofrecemos
                  una amplia variedad de juegos de PC, desde Battlefield hasta osu!. Además estamos incorporando consolas para que puedas
                  difrutar de otros titulos como Uncharted 4 o The Legend of Zelda: Breath of the Wild</p>
          </div>
      </div>


      <div class="trescolumnas">
          <div>
              <h1 class="iconosgoogle">
                  <i class="material-icons">store</i>
              </h1>
              <h2>Tienda [online]</h2>
              <p>Cuando tienes en tu cabeza un gran headset lo notas, y si es muy muy bueno quizas quieras comprarlo.
                   En nuestra tienda encontrarás los perifericos con los que estas jugando. Simplemente levanta la vista y encuéntralo.
                   Tambien es online yay!</p>
          </div>
      </div>
    </div>

    <div class="columnas">

      <div class="doscolumnas">
        <h2><a href="tienda.php">Compra en nuestra tienda</a></h2>
        <div class="descripcion_tienda_reserva">
          <p>Descubre los productos de los jugones!. Ponemos a tu disposicion una tienda para que puedas comprar los productos
          que quieras. Tenemos perifericos profesionales para los que tienen que jugar la mejor partida de su vida y otros un
          un poco mas modestos para los que quieren jugar pero no quieren dejarse mucha dinero.
          Si ya eres usuario de Elite Gaming Center estas de suerte, podras usar tu cuenta para comprar en nuestra nueva tienda online.
          </p>
        </div>

        <div class="imagenes_tienda_reserva">
          <img src="res/images/galeria/galeria2.jpg" />
        </div>
      </div>


      <div class="doscolumnas">
        <h2><a href="realizaReserva.php">Reserva tu dispositivo antes de tiempo</a></h2>
        <div class="descripcion_tienda_reserva">
          <p>No te arriesges a quedarte sin tu dispositivo favorito. A partir de ahora podras realizar reservas para que nada
            se ponga interponga entre tu skill y tus promos. Si tienes una cuenta con nosotros podrás reservar tu PC o consola favorita en un abrir y
            cerrar de ojos, 3 clicks como mucho. ¿Lo mejor de todo? que no te va a costar mas y que solo tendras que llegar y
            ponerte a jugar. Llega, sientate, ponte los cascos y no hables con nadie, no socialices, lo unico que importa es subir a diamante. A jugar!
          </p>
        </div>

        <div class="imagenes_tienda_reserva">
          <img src="res/images/galeria/galeria7.jpg" />
        </div>
      </div>
    </div>

    <div class="columnas" id="donde">
      <div>


          <h3>¿Dónde estamos?</h3>
          <p>
            Podriamos explicarlo de una forma muy compleja, pero una imagen vale mas que 1000 palabras y un mapa vale mas que 1000 imagenes!
          </p>
          <iframe class="mapagoogle"
            width="100%"
            height="400"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?key=[YOUR_GOOGLEMAPS_APIKEY]
            &q=Elite Gaming Center, Sevilla" allowfullscreen>
          </iframe>
</div>
    </div>

		</main>
			<?php
				include_once("pie.php");
			?>
	</body>
</html>
