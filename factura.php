<?php
    session_start();
    require_once("gestionBD.php");
    require_once("gestionEntradas/gestionMiCuentaCarrito.php");

    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }


        if (isset($_GET["factura"])) {
            $factura = $_GET["factura"];

            $conexion = crearConexionBD();
            $productos = productosDeFactura($conexion, $factura);
            cerrarConexionBD($conexion);

        }else{
            header("miCuentaCarrito.php");
        }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Factura #<?php echo $factura ?> - Elite Gaming Center</title>
        <meta name="description" content="">
        <meta name="author" content="andalu30">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/verFactura.css" type="text/css" />

    </head>

    <body>
      <a href="facturasPaginado.php"><img src="res/images/logo.png" /></a>
      <h1>Factura #<?php echo $factura ?></h1>

          <div id="empresa">
              <div>
                  <h4>Elite Gaming Center</h4>
                  <h4>Av. San Francisco Javier, 23</h4>
                  <h4>41005 Sevilla, España</h4>
              </div>

          </div>

          <div id="cliente">
              <div class="columnaCliente">
              <h4>Nombre: <?php echo $_REQUEST['NOMBRE'] ?></h4>
              <h4>Apellidos: <?php echo $_REQUEST['APELLIDOS'] ?></h4>
              <h4>DNI: <?php echo $_REQUEST['DNI'] ?></h4>
              </div>
              <div class="columnaCliente">
              <h4>Email: <?php echo $_REQUEST['EMAIL'] ?></h4>
              <h4>Tel: <?php echo $_REQUEST['TELEFONO'] ?></h4>
              <h4>Direccion: <?php echo $_REQUEST['DIRECCION'] ?></h4>
              </div>

          </div>

          <div id="tablaProductosyPrecio">
            <table>
                <tr>
                    <th>Imagen</th>
                    <th>Producto</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                </tr>
                <?php
                    $preciototal = 0;
                    for ($i=0; $i < count($productos); $i++){
                        $img = "res/images/productos/". trim($productos[$i]["OID_PROD"]) .trim(".png");
                        ?>
                        <tr>
                            <td><img class="imagen" src="<?php echo $img?>" alt="No encontramos la imagen. <?php echo $img?>" /></td>
                            <td align="center"><?php echo $productos[$i]["OID_PROD"] ?></td>
                          <?php $conexion = crearConexionBD();
                                $todosProductos = consultarTodosProductos($conexion);
                                cerrarConexionBD($conexion);
                                foreach ($todosProductos as $producto) {
                                    if ($producto["OID_PROD"]== $productos[$i]["OID_PROD"]) {
                                        ?><td><?php echo $producto["NOMBRE"];?></td><?php
                                    }
                                }
                               ?>
                            <td align="center"><?php echo $productos[$i]["CANTIDAD"] ?></td>
                            <td align="center"><b><?php echo $productos[$i]["PRECIO_PRODUCTO"] ?>€ x<?php echo $productos[$i]["CANTIDAD"]?> = <?php $total = tofloat($productos[$i]["PRECIO_PRODUCTO"])*tofloat($productos[$i]["CANTIDAD"]);echo $total; $preciototal=$preciototal+$total ?> €</b></td>
                        </tr>
                     <?php } ?>
            </table>

            <div>
                <h2>Precio Total: <?php echo $preciototal; ?> €</h2>
            </div>

            <script>
              window.print();
              window.location.replace('facturasPaginado.php');
            </script>


          </div>

    </body>

</html>
