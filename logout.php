<?php
    session_start();

    if (isset($_SESSION["carritoProductos"])) {
        unset($_SESSION["carritoProductos"]);
        unset($_SESSION["carritoCantidades"]);
    }


    if (isset($_SESSION['login']))
        $_SESSION['login'] = null;
    header("Location: index.php");

    if (isset($_SESSION['trab'])) {
      unset($_SESSION['trab']);
    }

?>
