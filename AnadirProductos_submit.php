<?php

require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

session_start();

	if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
	// Recogemos los datos del formulario
		$nuevoProducto= $_SESSION["formulario"];
		unset($_SESSION["formulario"]);
		
		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
		}	



	$conexion = crearConexionBD();
	$insercion = AñadirProducto($conexion,$nuevoProducto);					
	cerrarConexionBD($conexion);
	
	
	
	}else{
    	Header("Location: AnadirProductos_form.php"); //Esto carga la web alta si no se llega a accion desde el formulario.
}	
	
if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        
    }else{
        $login = 'No se ha iniciado sesión';
    }	
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
   <link rel="stylesheet" href="css/formularios.css" type="text/css" />

  <title>Añadir Productos - Submit</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>    
	
	<?php if ($insercion <> "") {
		?><h1>Error al meter los datos en la base de datos. </h1><?php echo $insercion;
	}else{ ?>
	<h2> Producto añadido con éxito:</h2>
	<div class="box">
		<h1>Datos personales capturados:</h1>
		<hr>
    <div>
            <li>Nombre:             <?php echo $nuevoProducto["nombre"]; ?></li>
            <li>Precio:             <?php echo $nuevoProducto["precio"]; ?></li>
            <li>Stock:             <?php echo $nuevoProducto["stock"]; ?></li>
            <li>IVA:             <?php echo $nuevoProducto["iva"]; ?></li>
    </div>

   <?php } ?>
 

	<a class="button" href="AnadirProductos_form.php">Volver al formulario para agregar nuevos productos</a>
	</div>
	<?php include_once 'pie.php';?>


</body>
</html>
