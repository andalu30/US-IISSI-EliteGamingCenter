<?php
session_start();
require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

	if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
	// Recogemos los datos del formulario
		$nuevoTrabajador= $_SESSION["formulario"];
		unset($_SESSION["formulario"]);
		
		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
					
		}	
	$conexion = crearConexionBD();
	$insercion = insertarTrabajador($conexion,	$nuevoTrabajador);
	cerrarConexionBD($conexion);
	
	}else{
		
		Header("Location: AnadirClientes_formulario.php");
	}
	
if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        
    }else{
        $login = 'No se ha iniciado sesión';
    }
	
	
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/formularios.css" type="text/css" />

  <meta charset="utf-8">
  <title>Alta de Trabajador - Test</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>    
	
	<?php if ($insercion <> "") {
		?><h1>Error al meter los datos en la base de datos.</h1>
		<h4>Informe del Error:</h4>
		<?php echo $insercion;
	}else{?>
	
	<h2>Trabajador dado de alta con éxito con los siguientes datos:</h2>
	<div class="box">
		<h1>Datos personales capturados:</h1>
		<hr>
    <div>
            <li>DNI:             <?php echo $nuevoTrabajador["dni"]; ?>                     </li>
            <li>Email:           <?php echo $nuevoTrabajador["email"]; ?>                   </li>
            <li>Direccion:       <?php echo $nuevoTrabajador["direccion"]; ?>               </li>
            <li>Telefono:        <?php echo $nuevoTrabajador["telefono"]; ?>                </li>
            <li>Nombre:          <?php echo $nuevoTrabajador["nombre"]; ?>                  </li>
            <li>Apellidos:       <?php echo $nuevoTrabajador["apellidos"]; ?>           	 </li>
            <li>Fecha de nacimiento: <?php echo $nuevoTrabajador["fechanac"]; ?>	 </li>
      </div>
	  <div>
    	<hr>
        <h1>Datos de registro:</h1>
        <hr>
        	<li>Usuario: <?php echo $nuevoTrabajador["email"];?>  </li>
            <li>Password: <?php echo $nuevoTrabajador["pass"]; ?>    </li>
         <a class="button" href="AnadirTrabajadores_formulario.php">Agregar más trabajadores</a> 	
      </div>
    </div>
   <?php } ?>
 
	<?php include_once 'pie.php';?>


</body>
</html>
