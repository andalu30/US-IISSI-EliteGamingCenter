<?php

   function BajaCliente($conexion,$nickname) {
    try {
        $stmt= $conexion->prepare("CALL EliminarCliente(:nickname)");
        $stmt->bindParam(':nickname', $nickname);
        $stmt->execute();
        return "";
    } catch(PDOException $e){
  		$_SESSION['excepcion'] = $e->GetMessage();
  		header("Location: excepcion.php");
  		return 0;
  	}
}

     function BajaTrabajador($conexion,$oid) {
    try {
        $stmt= $conexion->prepare("CALL EliminarTrabajador(:oid)");
        $stmt->bindParam(':oid', $oid);
        $stmt->execute();
        return "";
    } catch(PDOException $e){
  		$_SESSION['excepcion'] = $e->GetMessage();
  		header("Location: excepcion.php");
  		return 0;
  	}
}
function EliminaPC($conexion,$oid) {
    try {
        $stmt= $conexion->prepare("CALL EliminaPC(:oid)");
        $stmt->bindParam(':oid', $oid);
        $stmt->execute();
        return "";
    } catch(PDOException $e){
  		$_SESSION['excepcion'] = $e->GetMessage();
  		header("Location: excepcion.php");
  		return 0;
  	}
}
?>
