<?php

function facturaCarrito($conexion,$email) {
    try {
        $stmt= $conexion->prepare("CALL crearFacturaCarrito(:email)");
        $stmt->bindParam(':email', $email);

        $stmt->execute();
        return "";
    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

function carrito($conexion,$producto,$cantidad) {
    try {
        $stmt= $conexion->prepare("CALL carrito(:producto,:cantidad)");
        $stmt->bindParam(':producto', $producto);
        $stmt->bindParam(':cantidad', $cantidad);
        $stmt->execute();
        return "";
    } catch(PDOException $e) {
        return $e->getMessage();
    }
}


?>
