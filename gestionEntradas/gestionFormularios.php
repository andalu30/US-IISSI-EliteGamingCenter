<?php
function consultarEmail($conexion,$email){
	 $query = "SELECT count(*) AS TOTAL from USUARIOS where EMAIL=:email";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':email',$email);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
		$_SESSION['excepcion'] = $e->GetMessage();
		header("Location: excepcion.php");
		return 0;
	}
}
function consultarNickname($conexion,$nick){
	 $query = "SELECT count(*) AS TOTAL from CLIENTES where NICKNAME=:nick";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':nick',$nick);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
					$_SESSION['excepcion'] = $e->GetMessage();
					header("Location: excepcion.php");
				}
}

function consultarNumSS($conexion,$numSS){
	 $query = "SELECT count(*) AS TOTAL from TRABAJADORES where NUMEROSSSS=:numSS";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':numSS',$numSS);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
	  echo $e->getMessage();
        return 0;

	}
}
function consultarEmailProveedor($conexion,$email){
	 $query = "SELECT count(*) AS TOTAL from PROVEEDORES where EMAIL=:email";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':email',$email);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
	  echo $e->getMessage();
        return 0;

	}
}

function consultarFechaInicio($conexion,$ffincontrato,$finiciocontrato){
	 $consulta = "SELECT count(*) AS TOTAL
	 				 FROM proveedores
	 			WHERE FINICIOCONTRATO<TO_DATE(:finiciocontrato, 'YYYY-MM-DD HH24:MI:SS')
	 			AND	FFINCONTRATO>TO_DATE(:finiciocontrato, 'YYYY-MM-DD HH24:MI:SS')
	 			AND	FFINCONTRATO<>TO_DATE(:ffincontrato, 'YYYY-MM-DD HH24:MI:SS')";



 try{
        $stmt = $conexion->prepare($consulta);
		$stmt->bindParam(':finiciocontrato',$ffincontrato);
		$stmt->bindParam(':ffincontrato',$ffincontrato);
        $stmt->execute();
		return $stmt->fetchColumn();

	}catch(PDOException $e){
	  echo $e->getMessage();
        return 0;

	}
}

function fechaInicioRepetida($conexion,$finicioContrato){
	 $consulta = "SELECT count(*) AS TOTAL
	 		FROM proveedores
			 WHERE  FINICIOCONTRATO=TO_DATE(:finiciocontrato, 'YYYY-MM-DD HH24:MI:SS') ";
 try{
        $stmt = $conexion->prepare($consulta);
        $stmt->bindParam(':finiciocontrato',$finicioContrato);
        $stmt->execute();
		return $stmt->fetchColumn();

	}catch(PDOException $e){
	  echo $e->getMessage();
        return 0;

	}
}

function insertarCliente($conexion,$nuevoCliente) {
    try {
        $stmt= $conexion->prepare("CALL CREARCLIENTE(:dni,:email,:direccion,:telefono,:nombre,:apellidos,:contraseña,TO_DATE(:fechanac, 'YYYY-MM-DD HH24:MI:SS'),:nickname,0)");

        $stmt->bindParam(':dni',$nuevoCliente["dni"]);
        $stmt->bindParam(':email',$nuevoCliente["email"]);
        $stmt->bindParam(':direccion',$nuevoCliente["direccion"]);
        $stmt->bindParam(':telefono',$nuevoCliente["telefono"]);
        $stmt->bindParam(':nombre',     $nuevoCliente["nombre"]);
        $stmt->bindParam(':apellidos',  $nuevoCliente["apellidos"]);
        $stmt->bindParam(':contraseña', $nuevoCliente["pass"]);
        $stmt->bindParam(':fechanac',$nuevoCliente["fechanac"]);
        $stmt->bindParam(':nickname',$nuevoCliente["nick"]);

        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}

function insertarTrabajador($conexion,$nuevoTrabajador) {

	try {
		$stmt= $conexion->prepare("CALL CREARTrabajador(:dni,:email,:direccion,:telefono,:nombre,:apellidos,:contraseña,TO_DATE(:fechanac, 'YYYY-MM-DD HH24:MI:SS'),:numeroSS,:numeroCuenta,TO_DATE(:fechainiciocontrato, 'YYYY-MM-DD HH24:MI:SS'), TO_DATE(:fechafincontrato, 'YYYY-MM-DD HH24:MI:SS'),:salario,:esadmin)");

		$stmt->bindParam(':dni',	$nuevoTrabajador["dni"]);
		$stmt->bindParam(':email',		$nuevoTrabajador["email"]);
		$stmt->bindParam(':direccion',	$nuevoTrabajador["direccion"]);
		$stmt->bindParam(':telefono',	$nuevoTrabajador["telefono"]);
		$stmt->bindParam(':nombre',		$nuevoTrabajador["nombre"]);
		$stmt->bindParam(':apellidos',	$nuevoTrabajador["apellidos"]);
		$stmt->bindParam(':contraseña',	$nuevoTrabajador["pass"]);
		$stmt->bindParam(':fechanac',	$nuevoTrabajador["fechanac"]);
		$stmt->bindParam(':numeroSS',	$nuevoTrabajador["numSS"]);
		$stmt->bindParam(':numeroCuenta',$nuevoTrabajador["cuenta"]);
		$stmt->bindParam(':fechainiciocontrato',$nuevoTrabajador["inicioContrato"]);
		$stmt->bindParam(':fechafincontrato',	$nuevoTrabajador["finContrato"]);
		$stmt->bindParam(':salario',	$nuevoTrabajador["salario"]);
		$stmt->bindParam(':esadmin',	$nuevoTrabajador["esAdmin"]);

		$stmt->execute();
		return "";
	} catch(PDOException $e){
		$_SESSION['excepcion'] = $e->GetMessage();
		header("Location: excepcion.php");
	}
}

function AñadirProveedor($conexion,$nuevoProveedor) {
    try {
        $stmt= $conexion->prepare("CALL AñadirProveedor(:nombre,:direccion,:telefono,:email,TO_DATE(:fechainicio, 'YYYY-MM-DD HH24:MI:SS'),TO_DATE(:fechafin, 'YYYY-MM-DD HH24:MI:SS'))");
        $stmt->bindParam(':nombre', $nuevoProveedor['nombre']);
        $stmt->bindParam(':direccion', $nuevoProveedor['direccion']);
        $stmt->bindParam(':telefono', $nuevoProveedor['telefono']);
        $stmt->bindParam(':email', $nuevoProveedor['email']);
        $stmt->bindParam(':fechainicio', $nuevoProveedor['finiciocontrato']);
        $stmt->bindParam(':fechafin', $nuevoProveedor['ffincontrato']);

        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}

function ModificarSaldo($conexion,$nickname,$cantidad) {
    try {
        $stmt= $conexion->prepare("CALL ModificarSaldo(:nickname,:cantidad)");
        $stmt->bindParam(':nickname', $nickname);
        $stmt->bindParam(':cantidad', $cantidad);

        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}


function consultarOidCliente($conexion,$email){
	 $query = "SELECT oid_cliente FROM USUARIOS NATURAL JOIN CLIENTES where EMAIL=:email";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':email',$email);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
		$_SESSION['excepcion'] = $e->GetMessage();
		header("Location: excepcion.php");
        return 0;

	}
}

function insertarAmonestacion($conexion,$oid,$nuevaAmonestacion) {
    try {
        $stmt= $conexion->prepare("CALL CREARAMONESTACION(:oid,TO_DATE(:fecha, 'YYYY-MM-DD HH24:MI:SS'),:duracion)");

        $stmt->bindParam(':oid',$oid);
        $stmt->bindParam(':fecha',$nuevaAmonestacion["fecha"]);
        $stmt->bindParam(':duracion',$nuevaAmonestacion["duracion"]);
		$stmt->execute();
        return "";
    }catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}
function InsertarPCS($conexion,$estado,$tipo,$precio) {
    try {
        $stmt= $conexion->prepare("CALL InsertarPC(:est,:tipo,:precio)");
        $stmt->bindParam(':est', $estado);
        $stmt->bindParam(':tipo', $tipo);
        $stmt->bindParam(':precio', $precio);

        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}
function InsertarJuego($conexion,$name) {
    try {
        $stmt= $conexion->prepare("CALL InsertarJuego(:name)");
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}
function BorrarJuego($conexion,$name) {
    try {
        $stmt= $conexion->prepare("CALL BorrarJuego(:name)");
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}

function AñadirProducto($conexion,$producto) {
    try {
        $stmt= $conexion->prepare("CALL AñadirProducto(:nombre,:precio,:stock,:iva)");
        $stmt->bindParam(':nombre', $producto["nombre"]);
        $stmt->bindParam(':precio', $producto["precio"]);
        $stmt->bindParam(':stock', $producto["stock"]);
        $stmt->bindParam(':iva', $producto["iva"]);

        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}

function consultarEmailTrab($conexion,$email){
	 $query = "SELECT count(*) AS TOTAL from USUARIOS NATURAL JOIN TRABAJADORES where EMAIL=:email";
 try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':email',$email);
        $stmt->execute();
		return $stmt->fetchColumn();
	}catch(PDOException $e){
		$_SESSION['excepcion'] = $e->GetMessage();
		header("Location: excepcion.php");
	}
}



function consultarPc($conexion, $pc) {
    try {
        $stmt= $conexion->prepare("SELECT COUNT(*) AS TOTAL FROM PCS WHERE OID_PC=:pc");
        $stmt->bindParam(':pc',$pc);

        $stmt->execute();
        return $stmt->fetchColumn();
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}

function insertarReparacion($conexion,$nuevaReparacion) {
    try {
        $stmt= $conexion->prepare("CALL AÑADIRREPARACION(:pc,:email,TO_DATE(:fecha, 'YYYY-MM-DD HH24:MI:SS'))");
        $stmt->bindParam(':pc', $nuevaReparacion["id"]);
        $stmt->bindParam(':email', $nuevaReparacion["email"]);
        $stmt->bindParam(':fecha', $nuevaReparacion["fecha"]);


        $stmt->execute();
        return "";
    } catch(PDOException $e){
			$_SESSION['excepcion'] = $e->GetMessage();
			header("Location: excepcion.php");
		}
}
?>
