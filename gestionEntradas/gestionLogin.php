<?php
function consultarUsuarioSimple($conexion,$email,$pass) {
    $query = "SELECT count(*) AS TOTAL from USUARIOS where EMAIL=:email and CONTRASEÑA=:pass";


    try{
        $stmt = $conexion->prepare($query);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':pass',$pass);
        $stmt->execute();


        return $stmt->fetchColumn();

    }catch(PDOException $e){
        echo $e->getMessage();
        return -1; //Devuelve -1 si no se encuentra el usuario
    }
}
?>
