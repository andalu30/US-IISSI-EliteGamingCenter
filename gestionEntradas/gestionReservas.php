<?php
function consultarTodosPCS($conexion) {
    $consulta = "SELECT * FROM PCS";
    return $conexion->query($consulta);
}

function consultaEstadoReserva($conexion, $pc, $horainicio) {
    try {
        $stmt= $conexion->prepare("SELECT COUNT(*) FROM RESERVAS WHERE OID_PC=:pc
                          AND HORAINICIO=to_date(:hora,'DD-MM-YYYY HH24:MI')");
        $stmt->bindParam(':pc',$pc);
        $stmt->bindParam(':hora',$horainicio);

        $stmt->execute();
        return $stmt->fetchColumn();
    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

function getEstadoReservas($conexion,$login) {

        $stmt= $conexion->prepare("SELECT COUNT(*) FROM AMONESTACIONES WHERE OID_CLIENTE=(SELECT OID_CLIENTE FROM CLIENTES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=:mail)) AND FECHAINICIO+DURACION*7>SYSDATE");
        $stmt->bindParam(':mail',$login);
        $stmt->execute();
        return $stmt->fetchAll();
}


function AñadirReserva1Hora($conexion,$login,$oid,$horainicio) {
    try {
        $stmt= $conexion->prepare("CALL añadirreserva1hora(
                                                          :login,
                                                          :oid,
                                                          to_date(:horainicio,'DD-MM-YYYY HH24:MI'))");
        $stmt->bindParam(':login', $login);
        $stmt->bindParam(':oid', $oid);
        $stmt->bindParam(':horainicio', $horainicio);

        $stmt->execute();
        return "";
    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

function consultarSaldo($conexion, $login) {
    try {
        $stmt= $conexion->prepare("SELECT SALDO FROM CLIENTES NATURAL JOIN USUARIOS WHERE EMAIL=:login");
        $stmt->bindParam(':login',$login);
        $stmt->execute();
        return $stmt->fetchColumn();

    } catch(PDOException $e) {
        return $e->getMessage();
    }
}

//Código del usuario brewal dot renault at gmail dot com
//que se puede encontrar en http://php.net/manual/es/function.floatval.php
//Este código funciona como un floatval() pero con números no obligatoriamente
//en notacion americana (Separador de decimales . en vez de ,)
function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}


function consultarTodosclientes($conexion) {
    $consulta = "SELECT * FROM CLIENTES";
    return $conexion->query($consulta);
}


function consultarTodosusuarios($conexion) {
    $consulta = "SELECT * FROM USUARIOS";
    return $conexion->query($consulta);
}


?>
