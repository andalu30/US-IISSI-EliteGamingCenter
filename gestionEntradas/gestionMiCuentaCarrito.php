<?php

//Código del usuario brewal dot renault at gmail dot com
//que se puede encontrar en http://php.net/manual/es/function.floatval.php
//Este código funciona como un floatval() pero con números no obligatoriamente
//en notacion americana (Separador de decimales . en vez de ,)
function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}

function consultarTodosclientes($conexion) {
    $consulta = "SELECT * FROM CLIENTES";
    return $conexion->query($consulta);
}


function consultarTodosusuarios($conexion) {
    $consulta = "SELECT * FROM USUARIOS";
    return $conexion->query($consulta);
}

function consultarTodosProductos($conexion) {
    $consulta = "SELECT * FROM PRODUCTOS";
    return $conexion->query($consulta);
}

function consultarFacturasUsuario($conexion,$mail) {

        $stmt= $conexion->prepare("SELECT * FROM FACTURAS WHERE OID_CLIENTE =(SELECT OID_CLIENTE FROM CLIENTES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=:email))");
        $stmt->bindParam(':email',$mail);
        $stmt->execute();
        return $stmt->fetchAll();
}

function consultarReservasUsuario($conexion,$mail) {

        $stmt= $conexion->prepare("SELECT * FROM RESERVAS WHERE OID_CLIENTE=(SELECT OID_CLIENTE FROM USUARIOS WHERE EMAIL=:email) AND trunc(DIAINICIO)=(SELECT TRUNC(SYSDATE) from DUAL)");
        $stmt->bindParam(':email',$mail);
        $stmt->execute();
        return $stmt->fetchAll();
}

function productosDeFactura($conexion,$factura) {

        $stmt= $conexion->prepare("SELECT * FROM LINEASFACTURAS WHERE OID_F=:oid");
        $stmt->bindParam(':oid',$factura);
        $stmt->execute();
        return $stmt->fetchAll();
}

function getinfoUsuario($conexion,$login) {

        $stmt= $conexion->prepare("SELECT * FROM USUARIOS WHERE EMAIL=:mail");
        $stmt->bindParam(':mail',$login);
        $stmt->execute();
        return $stmt->fetchAll();
}

function getEstadoReservas($conexion,$login) {

        $stmt= $conexion->prepare("SELECT COUNT(*) FROM AMONESTACIONES WHERE OID_CLIENTE=(SELECT OID_CLIENTE FROM CLIENTES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=:mail)) AND FECHAINICIO+DURACION*7>SYSDATE");
        $stmt->bindParam(':mail',$login);
        $stmt->execute();
        return $stmt->fetchAll();
}


function EliminarReserva($conexion,$reserva) {
    try {
        $stmt= $conexion->prepare("CALL EliminarReservas(:oid)");
        $stmt->bindParam(':oid', $reserva);

        $stmt->execute();
        return "";
    } catch(PDOException $e) {
        return $e->getMessage();
    }
}



 ?>
