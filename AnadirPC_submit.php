<?php
session_start();
require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';


//Comprobamos si hay algun login abierto
    if (isset($_SESSION["login"])) {
        $login = $_SESSION["login"];
    }else{
        $login = 'No se ha iniciado sesión';
    }

    if (isset($_SESSION['errores'])) {
      unset($_SESSION['errores']);
    }


if (isset($_SESSION["formulario"])) { //Se mira si en request hay algun valor (Se ha entrado por el formulario)

    $nuevoPC["estado"] = $_SESSION['formulario']["estado"];
    $nuevoPC["tipo"] = $_SESSION['formulario']["tipo"];
    $nuevoPC["precio"] = $_SESSION['formulario']["precio"];


	$conexion = crearConexionBD();
	$insercion = InsertarPCS($conexion,$nuevoPC["estado"],$nuevoPC["tipo"],$nuevoPC["precio"]);
	cerrarConexionBD($conexion);

	}else{
    	Header("Location: AnadirPCs_form.php"); //Esto carga la web alta si no se llega a accion desde el formulario.
}
if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
// Recogemos los datos del formulario
  $nuevoProveedor= $_SESSION["formulario"];
  unset($_SESSION["formulario"]);

  if(isset($_SESSION["errores"])){
    unset($_SESSION["errores"]);

  }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/testcss.css" type="text/css" />
</head>

<body>
<script>
  window.alert("Se ha añadido el dispositivo");
  window.location.replace("PCsPaginados.php");

</script>
</body>
</html>
