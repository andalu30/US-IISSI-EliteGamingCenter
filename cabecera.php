<head>
  <link rel="stylesheet" href="css/cabeceraYpie.css" type="text/css" />

  <!--Fuente Roboto desde el servidor de Google. https://fonts.google.com/specimen/Roboto?selection.family=Roboto -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body>

<nav id="cabecera">
        <a class="logo" href="index.php"><img src="res/images/logo_elite.png" alt="LOGO" /></a>

        <ul id="lista_enlaces">
            <li><a href="administracionDB_TrabCheck.php"><span>Area de trabajadores</span></a></li>
            <li><a href="contacto.php"><span>Contacto</span></a></li>
            <li><a href="realizaReserva.php"><span>Reservas</span></a></li>
            <li><a href="tienda.php"><span>Tienda</span></a></li>
        </ul>


        <div id="divUsuarioConectado">
            <h4><?php echo $login;

            if (isset($_SESSION['trab'])) {
              $trab = $_SESSION['trab'];
              if ($trab==1) {
                echo ' [TRABAJADOR]';
              }else{
                echo ' [ADMIN]';
              }
            }

            ?></h4>
            <a href="login_form.php"><button class="botonCabecera">Iniciar/Cerrar Sesion</button></a>

            <?php if ($login != 'No se ha iniciado sesión' && !isset($_SESSION['trab'])) {
                ?><a href="miCuentaCarrito.php"><button class="botonCabecera">Mi cuenta/Carrito (

                <?php if (isset($_SESSION["carritoProductos"])) {
                        echo sizeof($_SESSION["carritoProductos"]);
                      }else{
                        echo '0';
                } ?> )</button></a><?php
            } ?>
        </div>
</nav>

</body>
