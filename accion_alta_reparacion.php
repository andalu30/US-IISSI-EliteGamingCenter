<?php


	
	session_start();
	
   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevaReparacion["id"] = $_REQUEST["id"];
		$nuevaReparacion["fecha"] = $_REQUEST["fecha"];
		$nuevaReparacion["email"] = $_REQUEST["email"];

	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirReparaciones_form.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevaReparacion;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevaReparacion);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirReparaciones_form.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirReparaciones_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevaReparacion){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarEmailTrab=consultarEmailTrab($conexion,$nuevaReparacion["email"]);
	 //COMENTARIO PARA JUAN LA FUNCION QUE INVOCO NO SÉ PORQUE NO VA
	 $consultarOidPc=consultarPc($conexion,$nuevaReparacion["id"]);
	 cerrarConexionBD($conexion);
	 

		// Validación del email
		if($nuevaReparacion["email"]==""){ 
			$errores['email'] = "<p>El email no puede estar vacío</p>";
		}else if(!filter_var($nuevaReparacion["email"], FILTER_VALIDATE_EMAIL)){
			$errores['email'] = $error . "<p>El email es incorrecto: " . $nuevaReparacion["email"]. "</p>";
			
		}else if($consultarEmailTrab==0 ){
			$errores['email'] = "<p>NO EXISTE UN TRABAJADOR CON ESE EMAIL</p>";
		}
		// Validación del fecha			
		if($nuevaReparacion["fecha"]=="") {
			$errores['fecha'] = "<p>La fecha no puede estar vacía</p>";
		}
		// Validación del id			
		if($nuevaReparacion["id"]==""){
			$errores['id'] = "<p>El identificador del producto no puede estar vacío</p>";
		}else if(!preg_match("/^[0-9]+/",$nuevaReparacion["id"])){
			$errores['id'] = "<p>El oid son solo dígitos</p>";
		
		}//else if($consultarOidPc==0){
			// $errores['id'] = "<p>No tenemos registrado un pc con ese oid</p>";
			// $errores['id'] =$consultarOidPc;
		// }
			
		

	
		return $errores;
	}

?>

