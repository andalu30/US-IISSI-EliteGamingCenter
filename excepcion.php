<?php
	session_start();

	$excepcion = $_SESSION["excepcion"];
	unset($_SESSION["excepcion"]);

	if (isset ($_SESSION["destino"])) {
		$destino = $_SESSION["destino"];
		unset($_SESSION["destino"]);
	} else
		$destino = "";

		//Comprobamos si hay algun login abierto
				if (isset($_SESSION["login"])) {
						$login = $_SESSION["login"];
				}else{
						$login = 'No se ha iniciado sesión';
				}
?>




<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/test.css" />
  <title>Houston, we have a problem.</title>
  <link rel="stylesheet" href="css/excepcion.css" type="text/css" />
</head>
<body>

<?php
	include_once("cabecera.php");
?>

	<div class="todo">
		<h2>HOLD HOLD HOLD!</h2>
		<h3>Ocurrió un problema para acceder a la base de datos. Pulse <a href="index.php">aquí</a> para volver a la página principal.</h3>
		<div class='excepcion'>
			<?php echo "Información relativa al problema: $excepcion;" ?>
		</div>
		<img loop="false" src="http://pop.h-cdn.co/assets/16/35/1472752799-ezgifcom-optimize-2.gif"/>
		<p>Esto es lo que pasa cuando tus alumnos la lian. </br>
			Juramos que esto nos funcionaba en nuestro PC. </br>
			Enserio... bueno no, pero tampoco es un bug, es una característica.</p>
	</div>

<?php
	include_once("pie.php");
?>

</body>
</html>
