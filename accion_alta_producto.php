<?php


	
	session_start();
	
   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevoProducto["precio"] = $_REQUEST["precio"];
		$nuevoProducto["nombre"] = $_REQUEST["nombre"];
		$nuevoProducto["stock"] = $_REQUEST["stock"];
		$nuevoProducto["iva"] = $_REQUEST["iva"];

	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirProductos_for.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevoProducto;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevoProducto);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirProductos_form.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirProductos_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevoProducto){
		
		// Validación del Nombre
		if($nuevoProducto["nombre"]=="") 
			$errores['nombre'] = "<p>El Nombre no puede estar vacío</p>";
		else if(!preg_match("/[Aa-zZ]+/", $nuevoProducto["nombre"])){
			$errores['nombre'] = "<p>Tiene que introducir caracteres </p>";
		}
		
		// Validación del Precio
		if($nuevoProducto["precio"]=="") 
			$errores['precio'] = "<p>El Precio no puede estar vacío</p>";
		else if(!preg_match("/^[0-9]+/", $nuevoProducto["precio"])){
			$errores['precio'] = "<p>Tiene que introducir solo digitos </p>";
		}
		// Validación del Stock
		if($nuevoProducto["stock"]=="") 
			$errores['stock'] = "<p>El STOCK no puede estar vacío</p>";
		else if(!preg_match("/^[0-9]+/", $nuevoProducto["stock"])){
			$errores['stock'] = "<p>Tiene que introducir solo digitos </p>";
		}
		
		// Validación del IVA
		if($nuevoProducto["iva"]=="") 
			$errores['iva'] = "<p>El IVA no puede estar vacío</p>";
		else if(!preg_match("/^[0-9]+/", $nuevoProducto["iva"])){
			$errores['iva'] = "<p>Tiene que introducir solo digitos </p>";
		}		
	
		return $errores;
	}

?>

