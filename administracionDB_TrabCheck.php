<?php

   session_start();
   require_once 'gestionBD.php';



   //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
           header("Location: login_form.php");
        }


   /*   --  DEFINICION DE LAS FUNCIONES DE IDENTIFICACION DE USUARIO  --
    *
    * Deberia de comentar todo esto para que lo entendieses pero paso
    * Digamos que me he tirado toda la tarde porque se me habia colado
    * una palabra y no la habia visto. FUCK!
    * En fin, funcionar funciona te lo crees y ya esta xDD
    * No, ahora en serio es facil lo entiendes si lo lees.
    * Pero no voy a comentar este spaguetti-code.
    *
    * Al final estan las comprobaciones en si, que son ifs y Javascript.
    * Javascripts por probar la verdad, se puede hacer con un header en un
    * momento pero queria ver como sacar una notificacion y redirigir desde ahí dentro.
    *
    */

    function checkTipoUsuario($conexion,$login) {
        $query = "SELECT COUNT(*) AS TOTAL FROM TRABAJADORES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=:email)";
        try{
            $stmt = $conexion->prepare($query);
            $stmt->bindParam(':email',$login);
            $stmt->execute();
            $esTrabajador =  $stmt->fetchColumn();

            if ($esTrabajador==1){
                $esAdministrador = checkTipoTrabajador($conexion, $login);
                if ($esAdministrador=='Trabajador') {
                    return 'Trabajador';
                }else{
                    return 'Administrador';
                }
            }else{
                return 'Cliente';
            }
        }catch(PDOException $e){
            echo $e->getMessage();
            return -1;
        }
    }

    function checkTipoTrabajador($conexion,$login) {
        try{
            $query = "SELECT COUNT(*) AS TOTAL FROM TRABAJADORES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=:email) AND (ESADMINISTRADOR='SI')";
            $stmt = $conexion->prepare($query);
            $stmt->bindParam(':email',$login);
            $stmt->execute();
            $esAdmin = $stmt->fetchColumn();
            if ($esAdmin==1) {
               return 'Administrador';
            }else{
               return 'Trabajador';
            }
        }catch(PDOException $e){
            echo $e->getMessage();
            return -1;
        }
    }









   $conexion = crearConexionBD();
   $tipoUsuario = checkTipoUsuario($conexion, $login);
   cerrarConexionBD($conexion);

   if ($tipoUsuario == 'Trabajador') {
       //header("Location: administracionDB_Trabajador.php");

       $_SESSION['trab']=1;
       ?>
          <script>
              window.alert("Bienvenido, trabajador <?php echo $login ?>\nSe le va a redirigir a la web de los empleados.");
              document.location.href='clientesPaginado.php'     //TODO: AdministracionBD de los empleados!
          </script>

       <?php


   }else if($tipoUsuario == 'Administrador'){
       //header("Location: administracionDB_Admin.php");
       $_SESSION['trab']=2;

       ?>
          <script>
              window.alert("Bienvenido, <?php echo $login ?>\nEs usted el administrador del sistema.\nSe le va a redirigir a la web del administrador. Cuidado con lo que vaya a tocar!.");
              document.location.href='clientesPaginado.php'     //TODO: AdministracionBD del jefe!
          </script>

       <?php


   }else{
       ?>
          <script>
              window.alert("No tienes permiso para entrar aqui, FUERA!");
              document.location.href='index.php'
          </script>
       <?php
   }

?>
