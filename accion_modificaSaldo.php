<?php


	
	session_start();
	
   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_REQUEST["cantidad"])) {
		// Recogemos los datos del formulario
		
		$nuevoCliente["cantidad"] = $_REQUEST["cantidad"];
		$nuevoCliente["nickname"] = $_REQUEST["nickname"];
		$nuevoCliente["saldoActual"] = $_REQUEST["saldoActual"];
		$_SESSION["nickname"]=$_REQUEST["nickname"];
	}else{// En caso contrario, vamos al formulario
		Header("Location: clientesPaginado.php");
		}
	// Guardar la variable local con los datos del formulario en la sesión.

     $_SESSION["formulario"] = $nuevoCliente;
	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevoCliente);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: clientesPaginado.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:ModificarSaldoCliente_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevoCliente){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarNickname=consultarNickname($conexion,$nuevoCliente['nickname']);
	 cerrarConexionBD($conexion);
	 

		// Validación del Nickname			
		if($nuevoCliente["nickname"]==""){
			$errores['nickname'] = "<p>El nickname no puede estar vacío</p>";
		}else if($consultarNickname==0){
			$errores['nickname'] = "<p>NO EXISTE CLIENTE CON DICHO NICKNAME</p>";
		}

		
		// Validación de la CANTIDAD
		if($nuevoCliente['cantidad']==""){
			$errores['cantidad']="<p> La cantidad tiene que completarse</p>";
		}else if(preg_match("/€/", $nuevoCliente["cantidad"])|| !preg_match("/[0-9]+/", $nuevoCliente["cantidad"])){
			$errores['cantidad'] = "<p>Cantidad a añadir tiene que ser solo dígitos</p>";
		}else if(($nuevoCliente['saldoActual']+$nuevoCliente['cantidad'])<0){
			$errores['cantidad'] = "<p>NO PUEDE RESTAR UNA CANTIDAD MAYOR A LA ACTUAL</p>";
		}
	
		return $errores;
	}

?>

