<?php
	session_start();
    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }

	// Si no existen datos del formulario en la sesión, se crea una entrada con valores por defecto
	if (!isset($_SESSION['formulario'])) {
		$formulario['estado'] = "Disponible";
		$formulario['tipo'] = "";
		$formulario['precio'] = 2;

		$_SESSION['formulario'] = $formulario;
	}
	// Si ya existían valores, los cogemos para inicializar el formulario
	else
		$formulario = $_SESSION['formulario'];

	// Si hay errores de validación, hay que mostrarlos y marcar los campos (El estilo viene dado y ya se explicará)
	if (isset($_SESSION["errores"]))
		$errores = $_SESSION["errores"];




?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Añadir dispositivo - Elite Gaming Center</title>
		<meta name="description" content="">
		<meta name="author" content="andalu30">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/formularios.css" type="text/css" />
	</head>

	<body>


				<?php //include_once('cabecera.php');
				include_once('cabecera.php');
				?>
<div>

			<div class="testbox">
	  		<h1>Registro de dispositivos</h1>

				<form action="accion_alta_pc.php" method="get" accept-charset="utf-8" />
				        <hr>
	    				<h1 class="h1">Datos del dispositivo</h1>
	     				<hr>
				 			<h4>Los campos indicados con un * son obligatorios.</h4>
									<div>
										<label for="estado">Estado:*</label>
	                    <select type="text" name="estado"  id="estado" required="">
	                        <option>Disponible</option>
	                        <option>No Disponible</option>
	                        <option>En reparacion</option>
	                    </select>
	                    <?php if(isset ($errores['tipo'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['estado']."</div>";}?>
	                </div>

	                <div>
										<label for="tipo">Tipo:*</label>
	                    <select type="text" name="tipo"  id="tipo" required="">
	                        <option>Normal</option>
	                        <option>VIP</option>
	                        <option>Switch</option>
	                        <option>PS3</option>
	                        <option>PS4</option>
	                        <option>XBONE</option>
	                        <option>Oculus</option>
	                        <option>HTCViv</option>
	                    </select>
	                    <?php if(isset ($errores['tipo'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['tipo']."</div>";}?>
	                </div>

	                <div>
	                    <input type="number" name="precio" id="precio"
	                     placeholder="Precio*" required="" />
	                    <?php if(isset ($errores['precio'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['precio']."</div>";}?>
	                </div>


			  <p><input class="button" type="submit" value="Añadir dispositivo"/></p>
				</form>
		    </div>
 </div>
		<?php include_once 'pie.php';?>

	</body>
</html>
