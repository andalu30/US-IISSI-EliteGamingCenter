# US-IISSI-EliteGamingCenter

Un trabajo para la asignatura de Introducción a la Ingeniería del Software y los Sistemas de Informacion de la Universidad de Sevilla.

En este trabajo hemos diseñado una web que funcione contra una base de datos encargada de guardar información de una "tienda/ciber", usando como base el modelo de negocio de la empresa Elite Gaming Center*.

Se guardará informacion sobre los clientes, trabajadores, dispositivos, etc... y se tendrá acceso a una tienda online, un sistema de reservas y un panel de administracion para los trabajadores.


\*Trabajadores de la compañia han supervisado el trabajo y lo han aceptado. Aun así, este proyecto es meramente educativo y en el no se distribuyen datos confidenciales (porque para empezar no los conocemos).
