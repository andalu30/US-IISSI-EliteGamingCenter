<?php
session_start();
require("gestionBD.php");
require("gestionEntradas/gestionReservas.php");



    if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            header('Location: login_form.php');
            $login = 'No se ha iniciado sesión';
    }


    if (isset($_GET["oid"])) {
    	$pcSeleccionado = $_GET["oid"];
      $horainicio = $_GET["horainicio"];

        $conexion = crearConexionBD();
        $todosPCs = consultarTodosPCS($conexion);
        cerrarConexionBD($conexion);

        foreach ($todosPCs as $pc) {
            if ($pc["OID_PC"]==$pcSeleccionado) {
                $infoPC = $pc;
                break;
            }
          }
    }else{
        header("Location: realizaReserva.php");
    }

	$conexion=crearConexionBD();
	$consultarSaldo = consultarSaldo($conexion,$login);
	cerrarConexionBD($conexion);
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Reserva_form</title>
        <meta name="description" content="">
        <meta name="author" content="andalu30">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/reservaForm.css" type="text/css" />

    </head>
    <body>

        <div>
            <header>
                <?php include_once('cabecera.php'); ?>
                <h1>Confirmar reserva.</h1>
            </header>
            <div class="contenedorDeTodo">
                    <fieldset>
                        <legend>Datos de la reserva</legend>
                        <h4>Se va a reservar el dispositivo número: <?php echo $pcSeleccionado; ?></h4>
                        <h4>Que es del tipo: <?php echo $infoPC["TIPO_PC"]?></h4>
                        <h4>Durante una hora a partir de las <?php echo $horainicio ?></h4>
                        <h3>Este dispositivo tiene un precio de: <b><?php echo $infoPC["PRECIO"] ?> €/hora</b></h3>
                        <h5>Este precio se le descontará de su saldo.</h5>
                        <?php

                            $conexion = crearConexionBD();
                            $tieneAmonestacion = getEstadoReservas($conexion,$login);
                            cerrarConexionBD($conexion);

                            if ($tieneAmonestacion[0][0]==0) { ?>

                              <a><button  class="confirmaReserva" onclick="preguntaconfirmacion();" >Confirmar Reserva</button></a>


                                    <?php
                                  }else{
                                    ?>
                                      <a href="realizaReserva.php"><button disabled class="realizaReservaAMONESTADO">No puedes realizar una reserva porque has sido amonestado</button></a>

                                  <?php
                                  }
                                  ?>
                    </fieldset>




                    <?php

                    $conexion = crearConexionBD();
                    $Clientes = consultarTodosClientes($conexion);

                    $Usuarios = consultarTodosUsuarios($conexion);
                    foreach ($Usuarios as $u) {
                        if ($u["EMAIL"]==$login) {
                            $informacionUsuario = $u;
                        }
                    }

                    foreach ($Clientes as $c) {
                        if ($c["OID_USUARIO"] == $informacionUsuario["OID_USUARIO"]) {
                            $informacionCliente = $c;
                        }
                    }
                     ?>

                    <script>
                        function preguntaconfirmacion() {
                            var precioTotal = <?php echo tofloat($infoPC["PRECIO"]); ?>;
                            var saldo = <?php echo tofloat($informacionCliente["SALDO"]); ?>;

                            var r = confirm("¿Está seguro de que desea confirmar la reserva?\nEl precio de esta se le descontará de su saldo.");
                            if (r == true) {
                                if (saldo-precioTotal <1) {
                                    window.alert("No dispones del saldo suficiente para realizar la reserva.");
                                    window.location.replace("miCuentaCarrito.php");

                                }else{
                                    window.location.replace("reserva_submit.php?pc=<?php echo $pcSeleccionado ?>&horainicio=<?php echo $horainicio ?>");
                                }
                            }else{
                                window.alert("De acuerdo, no se reservará el dispositivo");
                                window.location.replace("realizaReserva.php");
                            }
                        }
                    </script>








            </div>
                <?php include_once 'pie.php'; ?>
        </div>
    </body>
</html>
