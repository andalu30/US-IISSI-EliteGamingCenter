<?php


	
	session_start();
	
    require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevoProveedor["nombre"] = $_REQUEST["nombre"];
		$nuevoProveedor["email"] = $_REQUEST["email"];
		$nuevoProveedor["direccion"] = $_REQUEST["direccion"];
		$nuevoProveedor["telefono"] = $_REQUEST["telefono"];
		$nuevoProveedor["finiciocontrato"] = $_REQUEST["finiciocontrato"];
		$nuevoProveedor["ffincontrato"] = $_REQUEST["ffincontrato"];
	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirProveedor_form.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevoProveedor;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevoProveedor);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirProveedor_form.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirProveedor_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevoProveedor){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarEmailProveedor=consultarEmailProveedor($conexion,$nuevoProveedor["email"]);
	 $consultarFechaInicio= consultarFechaInicio($conexion,$nuevoProveedor['ffincontrato'],$nuevoProveedor['finiciocontrato']);
	 $fechaInicioRepetida= fechaInicioRepetida($conexion,$nuevoProveedor['finiciocontrato']);
	 cerrarConexionBD($conexion);
	 

		// Validación del Nombre			
		if($nuevoProveedor["nombre"]=="") 
			$errores['nombre'] = "<p>El nombre no puede estar vacío</p>";
		
		// Validación del email
		if($nuevoProveedor["email"]==""){ 
			$errores['email'] = "<p>El email no puede estar vacío</p>";
		}else if(!filter_var($nuevoProveedor["email"], FILTER_VALIDATE_EMAIL)){
			$errores['email'] = $error . "<p>El email es incorrecto: " . $nuevoProveedor["email"]. "</p>";
			
		}else if($consultarEmailProveedor==1 ){
			$errores['email'] = "<p>YA ESXISTE UN USUARIO CON ESE EMAIL REGISTRADO</p>";
		}
		
		// Validación del direccion			
		if($nuevoProveedor["direccion"]=="") 
			$errores['direccion'] = "<p>La direccion no puede estar vacía</p>";
		
		// Validación del numero de telefono
		if($nuevoProveedor['telefono']==""){
		     $errores['telefono']= "<p>El teléfono tiene que completarse</p>";
		}else if (!preg_match("/^[0-9]{9}$/",$nuevoProveedor["telefono"])){
			$errores['telefono']="<p> El telefono tiene menos de 9 cifras:".$nuevoProveedor["telefono"]."</p>";
		}
		
		// Validación de la fecha del contrato
		if($nuevoProveedor['finiciocontrato']=="" ){
			$errores['finiciocontrato']="<p> La fecha de contrato tiene que completarse</p>";

			 }else if($fechaInicioRepetida==1){
			$errores['finiciocontrato'] = "<p>Ya existe un contrato con la fecha de inicio:
			 ".$nuevoProveedor['finiciocontrato']."</p>";
			 
		 		}else if($nuevoProveedor['ffincontrato']==""){
			$errores['ffincontrato']="<p> La fecha de contrato tiene que completarse</p>";
			
         }else if($nuevoProveedor['ffincontrato']<$nuevoProveedor['finiciocontrato']){
	   		$errores['ffincontrato']="<p>La fecha de fin del contrato tiene que ser posterior a la fecha de inicio</p>";
			
		 } else if($consultarFechaInicio==1 or $consultarFechaInicio>1){
			$errores['ffincontrato'] = "<p>Ya exsite un contrato con la fecha de inicio anterior a:
			 ".$nuevoProveedor['finiciocontrato'].", y la fecha fin de contrato posterior o anterior a:".
			 $nuevoProveedor['ffincontrato']."</p>";
		 }
	
		return $errores;
	}

?>

