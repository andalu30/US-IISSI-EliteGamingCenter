<?php
    session_start();
    require_once("gestionBD.php");
    require_once("gestionEntradas/gestionMiCuentaCarrito.php");



    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];

            $conexion = crearConexionBD();
            $Usuarios = consultarTodosUsuarios($conexion);
            foreach ($Usuarios as $u) {
                if ($u["EMAIL"]==$login) {
                    $informacionUsuario = $u;
                }
            }

            $Clientes = consultarTodosClientes($conexion);
            foreach ($Clientes as $c) {
                if ($c["OID_USUARIO"] == $informacionUsuario["OID_USUARIO"]) {
                    $informacionCliente = $c;
                }
            }
            cerrarConexionBD($conexion);
        }else{
           header("Location: login_form.php");
        }
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Mi cuenta / Carrito</title>
        <meta name="description" content="">
        <meta name="author" content="andalu30">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/miCuentaCarrito.css" type="text/css"/>
    </head>

    <body>
        <div>
            <header>
                <?php require_once('cabecera.php');?>
                <h1>Mi cuenta</h1>
            </header>
        </div>

        <div id="misDatos">
           <h3 class="headerApartados">Mis datos</h3>
           <table>
            <tr>
                <th>Nickname</th>
                <td><?php echo $informacionCliente['NICKNAME']?></td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td><?php echo $informacionUsuario["NOMBRE"]?></td>
            </tr>
            <tr>
                <th>Apellidos</th>
                <td><?php echo $informacionUsuario["APELLIDOS"]?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $informacionUsuario["EMAIL"]?></td>
            </tr>
            <tr>
                <th>DNI</th>
                <td><?php echo $informacionUsuario["DNI"]?></td>
            </tr>
            <tr>
                <th>Direccion</th>
                <td><?php echo $informacionUsuario["DIRECCION"]?></td>
            </tr>
            <tr>
                <th>Telefono</th>
                <td><?php echo $informacionUsuario["TELEFONO"]?></td>
            </tr>
            <tr>
                <th>Saldo</th>
                <td><?php echo $informacionCliente["SALDO"]?> €<button class="añadeSaldo" onclick="añadirSaldo()">Añadir Saldo</button></td>
                <script>
                    function añadirSaldo() {
                        window.alert("Para añadir saldo hable con uno de nuestros trabajadores en la tienda")
                    }
                </script>
            </tr>
        </table>
       </div>
        <div id="carritodelacompra">
            <!-- ForEach del carrito -->
            <h3 class="headerApartados">Carrito de la compra</h3>

            <?php if(isset($_SESSION["carritoProductos"])) {
            ?>
                <table>
                    <tr>
                        <th>Imagen</th>
                        <th>Producto</th>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Botones</th>
                    </tr>


                    <?php
                        $precioTotal = 0.00;

                        for ($i=0; $i < count($_SESSION["carritoProductos"]); $i++){
                            $img = "res/images/productos/". trim($_SESSION["carritoProductos"][$i]) .trim(".png");
                            ?>

                            <tr>
                                <td><img style="width: 25ex;" src="<?php echo $img?>" alt="No encontramos la imagen. <?php echo $img?>" /></td>
                                <td align="center"><?php echo $_SESSION["carritoProductos"][$i] ?></td>

                                <!-- Nombre -->

                                    <?php

                                    $conexion = crearConexionBD();
                                    $todosProductos = consultarTodosProductos($conexion);
                                    cerrarConexionBD($conexion);


                                    foreach ($todosProductos as $producto) {
                                        if ($producto["OID_PROD"]== (int) $_SESSION["carritoProductos"][$i]) {
                                            ?><td><?php echo $producto["NOMBRE"];?></td><?php
                                        }
                                    }
                                   ?>


                                <td align="center"><?php echo $_SESSION["carritoCantidades"][$i] ?></td>


                                <!-- Precio -->
                                <?php


                                    $conexion = crearConexionBD();
                                    $todosProductos = consultarTodosProductos($conexion);
                                    cerrarConexionBD($conexion);


                                    foreach ($todosProductos as $producto) {
                                        if ($producto["OID_PROD"]== (int) $_SESSION["carritoProductos"][$i]) {


                                            $precioIndividual = tofloat($producto["PRECIO"]);

                                        }
                                    }
                                    $precioConjunto = $precioIndividual * (int)$_SESSION["carritoCantidades"][$i];
                                    $precioTotal = $precioTotal + $precioConjunto;

                                    ?><td align="center"><b><?php echo $precioIndividual;?> € x<?php echo (int)$_SESSION["carritoCantidades"][$i] .' = ' .$precioConjunto?> €</b></td><?php

                            ?>








                                <td>
                                    <a href="EliminaProductoCarrito.php?producto=<?php echo $_SESSION["carritoProductos"][$i];?>">
                                    <button class="elimina_vaciardelcarrito">Eliminar del carrito</button></a>
                                </td>

                            </tr>



                        <?php } ?>


                </table>
                <div align="center">
                    <h3>Precio total: <?php echo $precioTotal; ?> €</h3>
                    <form action="VaciarCarrito.php" style="display: inline;">
                        <button type="submit" class="elimina_vaciardelcarrito">Vaciar el Carrito</button>
                    </form>
                    <button class="confirmaCarrito" onclick="preguntaconfirmacion();">Confirmar el carrito</button>
                <script>
                    function preguntaconfirmacion() {
                        var precioTotal = <?php echo tofloat($precioTotal); ?>;
                        var saldo = <?php echo tofloat($informacionCliente["SALDO"]); ?>;
                        var r = confirm("¿Está seguro de que desea confirmar el carrito de la compra?\nEl precio total se le descontará de su saldo.");
                        if (r == true) {
                            //El usuario quiere comprar, comprobamos si tiene saldo
                            if (saldo-precioTotal <0) {//No tiene saldo, no puede comprar
                                window.alert("No dispones del saldo suficiente para realizar la compra.")
                            }else{//Si tiene saldo, compra
                                window.location.replace("compraCarrito.php");
                            }
                        }else{
                            window.alert("No se ha procesado el carrito de compra");
                        }
                    }
                </script>
                </div>




            <?php }else{?>
                        <p class="headerApartados">Tu carrito esta vacio, compra algo en nuestra <a href="tienda.php">Tienda</a></p>
                  <?php } ?>

        </div>
        <div id="FacturasUsuario">
          <h3 class="headerApartados">Mis compras/facturas:</h3>
           <?php
            $conexion = crearConexionBD();
            $FacturasUsuario =  consultarFacturasUsuario($conexion,$login);
            ?>
            <table>
                <tr>
                    <th># Factura</th><th>Fecha</th><th>Precio Total</th><th>Botones</th>
                </tr>
                <tr>
            <?php
            for ($i=0; $i < count($FacturasUsuario); $i++) {
                ?>
                <td><?php echo $FacturasUsuario[$i]["OID_F"];?></td>
                <td><?php echo $FacturasUsuario[$i]["FECHA"];?></td>
                <td><?php echo $FacturasUsuario[$i]["PRECIOTOTAL"];?></td>
                <td><a href="verFactura.php?factura=<?php echo $FacturasUsuario[$i]["OID_F"]?>"><button class="imprimir">Ver/imprimir Factura</button></a></td>
                </tr>
                <?php
            }
            ?>
            </table>
            <?php
            cerrarConexionBD($conexion);
            ?>
        </div>

        <div id="reservasUsuario">
            <h3 class="headerApartados">Mis Reservas de hoy:</h3>
            <?php
            $conexion = crearConexionBD();
            $ReservasUsuario =  consultarReservasUsuario($conexion,$login);
            ?>
            <table>
                <tr>
                    <th># Reserva</th><th>PC</th><th>Inicio</th><th>Duración (En horas)</th><th>Precio</th><th>Botones</th>
                </tr>

                <tr>

                    <?php
                    for ($i=0; $i < count($ReservasUsuario); $i++) {
                        ?>
                        <td><?php echo $ReservasUsuario[$i]["OID_RE"];?></td>
                        <td><?php echo $ReservasUsuario[$i]["OID_PC"];?></td>
                        <td><?php echo $ReservasUsuario[$i]["HORAINICIO"];?></td>
                        <td><?php echo $ReservasUsuario[$i]["NUMEROHORAS"];?></td>
                        <td><?php echo $ReservasUsuario[$i]["PRECIO"];?></td>

                        <td>
                            <?php
                            $horaactual = date("H");
                            $hora = $ReservasUsuario[$i]["HORAINICIO"][9].$ReservasUsuario[$i]["HORAINICIO"][10];
                            if ($horaactual<$hora) {
                            ?>
                          <button class="elimina_vaciardelcarrito" onclick="preguntaReservas('<?php echo $ReservasUsuario[$i]["OID_RE"]?>')">Eliminar Reserva</button>
                            <?php
                              }else{
                                echo 'Reserva pasada';
                              }
                            ?>
                        </td>
                        </tr>
                        <?php
                        }
                        ?>

            </table>



            <script>
                function preguntaReservas(oid_re){

                    var r = confirm("¿Está seguro de que desea cancelar la reserva?\nSi faltan menos de 12 horas sólo se le devolverá la mitad del precio");
                        if (r == true) {
                            window.location.replace("eliminaReserva.php?oid="+oid_re);
                        }else{
                            window.alert("No se ha eliminado la reserva.");
                            window.location.replace("miCuentaCarrito.php");
                        }
                }
            </script>




<?php

    $conexion = crearConexionBD();
    $tieneAmonestacion = getEstadoReservas($conexion,$login);
    cerrarConexionBD($conexion);

    if ($tieneAmonestacion[0][0]==0) { ?>

            <div class="divButRes">
              <a href="realizaReserva.php"><button class="realizaReserva">Realizar una reserva</button></a>
            </div>

            <?php
          }else{
            ?>
            <div class="divButRes">
              <a href="realizaReserva.php"><button disabled class="realizaReservaAMONESTADO">No puedes realizar una reserva porque has sido amonestado</button></a>
            </div>

          <?php
          }
          ?>





            <?php
            cerrarConexionBD($conexion);
            ?>
        </div>


            <footer>
              <?php require_once('pie.php'); ?>
            </footer>
    </body>
</html>
