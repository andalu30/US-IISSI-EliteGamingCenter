<?php
	session_start();

   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';

	 if (isset($_SESSION['errores'])) {
	 	unset($_SESSION['errores']);
	 }


	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevaAmonestacion["nombre"] = $_REQUEST["nombre"];
	}
	else{ // En caso contrario, vamos al formulario
		header("Location: AnadirJuegos_form.php");
	}
	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevaAmonestacion;

	// Validamos el formulario en servidor
	$errores = validarJuego($nuevaAmonestacion);


	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirJuegos_form.php');
	} else {

		//echo $_SESSION['estado'],$_SESSION['tipo'],$_SESSION['precio'];
		//echo $nuevaAmonestacion["estado"];
		// Si todo va bien, vamos a la página de éxito
		Header("Location: AnadirJuego_submit.php");
	}






	function validarJuego($nuevaAmonestacion){

				if ($nuevaAmonestacion["nombre"]=="") {
					$errores['nombre'] = "<p>El juego tiene que tener un nombre.</p>";
					}


		return $errores;
	}

?>
