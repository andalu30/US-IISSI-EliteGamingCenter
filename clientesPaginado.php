<?php
    session_start();

	require_once("gestionBD.php");
  require_once("gestionEntradas/paginacion_consulta.php");


    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }
	//COMPROBAMOS ERRORES DE MODIFICA SALDO
   	if (isset($_SESSION["errores"]))
		$errores = $_SESSION["errores"];


    //Gestion de la paginación.

    // ¿Venimos simplemente de cambiar página o de haber seleccionado un registro ?
    // ¿Hay una sesión activa?
    if (isset($_SESSION["paginacion"]))
    $paginacion = $_SESSION["paginacion"];
    $pagina_seleccionada = isset($_GET["PAG_NUM"])? (int)$_GET["PAG_NUM"]:
                                                (isset($paginacion)? (int)$paginacion["PAG_NUM"]: 1);
    $pag_tam = isset($_GET["PAG_TAM"])? (int)$_GET["PAG_TAM"]:
                                        (isset($paginacion)? (int)$paginacion["PAG_TAM"]: 5);
    if ($pagina_seleccionada < 1) $pagina_seleccionada = 1;
    if ($pag_tam < 1) $pag_tam = 5;

    // Antes de seguir, borramos las variables de sección para no confundirnos más adelante
    unset($_SESSION["paginacion"]);



    // La consulta que ha de paginarse
    //SI HEMOS INTRODUCIDO ALGO EN EL INPUT DE BUSQUEDA VOLVEMOS
    //A ESTA PÁGINA Y CAMBIAMOS LA CONSULTA
     //PARA HACER BUSQUEDA LO PONEMOS DIRECTAMENTE AQUÍ
     $conexion = crearConexionBD();
	 if(isset($_REQUEST['keyword'])){
	 $keyword=$_REQUEST['keyword'];

     //******propongo búsqueda general por la tabla por ahora****////
	 $query="SELECT * FROM CLIENTES NATURAL JOIN USUARIOS
	 		WHERE UPPER(NOMBRE) LIKE UPPER('%".$keyword."%')
	 		OR UPPER(APELLIDOS) LIKE UPPER('%".$keyword."%')
	 		OR UPPER(NICKNAME) LIKE UPPER('%".$keyword."%')
	 		OR UPPER(DNI) LIKE UPPER('%".$keyword."%')
	 		OR UPPER(EMAIL) LIKE UPPER('%".$keyword."%')
	 		OR UPPER(DIRECCION) LIKE UPPER('%".$keyword."%')
	 		OR TELEFONO LIKE '%".$keyword."%'";

	 }else{
	 $query = 'SELECT * FROM CLIENTES NATURAL JOIN USUARIOS';
	 }




    // Se comprueba que el tamaño de página, página seleccionada y total de registros son conformes.
    // En caso de que no, se asume el tamaño de página propuesto, pero desde la página 1
    $total_registros = total_consulta($conexion,$query);
    $total_paginas = (int) ($total_registros / $pag_tam);
    if ($total_registros % $pag_tam > 0) $total_paginas++;
    if ($pagina_seleccionada > $total_paginas) $pagina_seleccionada = $total_paginas;

    // Generamos los valores de sesión para página e intervalo para volver a ella después de una operación
    $paginacion["PAG_NUM"] = $pagina_seleccionada;
    $paginacion["PAG_TAM"] = $pag_tam;
    $_SESSION["paginacion"] = $paginacion;

    $filas = consulta_paginada($conexion,$query,$pagina_seleccionada,$pag_tam);

    cerrarConexionBD($conexion);
    if(isset($_SESSION["formulario"])){
    // RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
    // Recogemos los datos del formulario
      $nuevoProveedor= $_SESSION["formulario"];
      unset($_SESSION["formulario"]);

      if(isset($_SESSION["errores"])){
        unset($_SESSION["errores"]);

      }
    }
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/tablasPaginadas.css" type="text/css" />
<meta name="viewport" content="width=device-width; initial-scale=1.0">
  <title>CLIENTES - Elite Gaming Center</title>
    </head>
	<body>
	<?php include_once 'cabecera.php';?>

 <div  class="menuEmbed">
    <h1>Área de Trabajadores</h1>

    <div class="enlaces">
      <?php if($_SESSION['trab']==1){	?>
        <li class="activa"><a href="clientesPaginado.php">Clientes</a></li>
        <li><a href="facturasPaginado.php">Facturas</a></li>
        <li><a href="reservasPaginado.php">Reservas</a></li>
        <li ><a  href="amonestacionesPaginado.php">Amonestaciones</a></li>
        <li ><a href="PCsPaginados.php">PCs</a></li>
        <li><a href="JuegosPaginados.php">Juegos</a></li>
        <li><a href="reparacionesPaginado.php">Reparaciones</a></li>
        <li><a href="productosPaginado.php">Productos</a></li>
      <?php }else if($_SESSION['trab']==2){ ?>
        <li class="activa"><a href="clientesPaginado.php">Clientes</a></li>
         <li><a href="trabajadoresPaginado.php">Trabajadores</a></li>
        <li><a href="facturasPaginado.php">Facturas</a></li>
        <li><a href="reservasPaginado.php">Reservas</a></li>
        <li  ><a href="amonestacionesPaginado.php">Amonestaciones</a></li>
        <li><a href="PCsPaginados.php">PCs</a></li>
        <li ><a href="JuegosPaginados.php">Juegos</a></li>
        <li><a href="reparacionesPaginado.php">Reparaciones</a></li>
        <li><a href="proveedoresPaginado.php">Proveedores</a></li>
        <li><a href="productosPaginado.php">Productos</a></li>
      <?php }?>

      </ul>
    </div>

  </div>
	<h2>Consulta de clientes</h2>

	<!-- Paginacion debajo -->
	 <nav class="pagination">
        <div >
            <a href="clientesPaginado.php?PAG_NUM=<?php echo $pagina_seleccionada-1; ?>&PAG_TAM=<?php echo $pag_tam; ?>"><<</a>
            <?php
                for( $pagina = 1; $pagina <= $total_paginas; $pagina++ )
                    if ( $pagina == $pagina_seleccionada) {     ?>
                        <a><span class="current"><?php echo $pagina; ?></span></a>
            <?php } else { ?>
                        <a href="clientesPaginado.php?PAG_NUM=<?php echo $pagina; ?>&PAG_TAM=<?php echo $pag_tam; ?>"><?php echo $pagina; ?></a>
            <?php } ?>

           <a href="clientesPaginado.php?PAG_NUM=<?php echo $pagina_seleccionada+1; ?>&PAG_TAM=<?php echo $pag_tam; ?>">>></a>




        </div>

        <form class="formPaginas" method="get" action="clientesPaginado.php">
            <input id="PAG_NUM" name="PAG_NUM" type="hidden" value="<?php echo $pagina_seleccionada?>"/>
            Mostrando
            <input id="PAG_TAM" name="PAG_TAM" type="number" min="1" max="<?php echo $total_registros;?>"value="<?php echo $pag_tam?>" />
            entradas de <?php echo $total_registros?>
            <input type="submit" value="Cambiar">
        </form>
    </nav>











    <!-- Tabla que muestra los productos -->
    <table id="tablaGeneral">

            <tr>
            	<th>DNI</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Email</th>
                <th>Nickname</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>Fecha Nacimiento</th>
                <th>Saldo</th>
                <th>Botones</th>
            </tr>

            <?php foreach ($filas as $cliente) { ?>
                <tr class="tablaClientes">

                        <td align="left"><?php echo $cliente["DNI"]?></td>
                        <td align="left"><?php echo $cliente["NOMBRE"]?></td>
                        <td align="left"><?php echo $cliente["APELLIDOS"]?></td>
                        <td align="left"><?php echo $cliente["EMAIL"]?></td>
                        <td align="center"><?php echo $cliente["NICKNAME"]?></td>
                        <td align="center"><?php echo $cliente["TELEFONO"]?></td>
                        <td align="left"><?php echo $cliente["DIRECCION"]?></td>
                        <td align="left"><?php echo $cliente["FECHANACIMIENTO"]?></td>
                        <td align="center">

                        <form action="accion_modificaSaldo.php" method="get" accept-charset="utf-8">
                        <input id="saldo" type="text" name="cantidad" value="<?php echo $cliente["SALDO"].'€'?>"  />
                        <input  type="hidden" name="saldoActual" value="<?php echo $cliente["SALDO"]?>"  />
                        <input id="nickname" type="hidden" name="nickname" value="<?php echo $cliente["NICKNAME"]?>" />
                        <input class="botonModificar" type="submit" value="Modificar saldo"/>
                        <?php if(isset($_SESSION['nickname']) && ($cliente["NICKNAME"]==$_SESSION['nickname'])){?>
                        <?php if(isset ($errores['nickname'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['nickname']."</div>";}?>
                        <?php if(isset ($errores['cantidad'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['cantidad']."</div>";}?>
                        <?php } ?>
                        </form>

                        </td>
                        <td align="center"><a href="BajaCliente_submit.php?nickname=<?php echo trim($cliente["NICKNAME"])?>">
                        	<button class="botonEliminar">Eliminar Cliente</button></a>
                        </td>
                  </tr>


            <?php } ?>
        </table>
 <a href="AnadirClientes_formulario.php"><button class="botonAñadir">Añadir Clientes</button></a>








<!-- Busqueda de productos debajo ~~~ WIP -->
    <div id="busqueda">
    	<h2>Busqueda de clientes:</h2>
    	<p>Introduzca la palabra a buscar: </p>

    	<form action="clientesPaginado.php" class="cuadrodebusqueda">
    	   <input name="keyword" id="inputbusqueda" placeholder="Busqueda por nombre" />


    	</form>
	</div>

	<?php include_once 'pie.php';?>
	</body>
</html>
