<?php
session_start();

  if (isset($_SESSION["login"])) {
      $login = $_SESSION["login"];
  }else{
      $login = 'No se ha iniciado sesión';
  }



  include_once 'cabecera.php';

 ?>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="css/documentacion.css" type="text/css" />
</head>



<body>
  <h1>Documentación</h1>
  <p>
    Toda la documentación se puede encontrar online en nuestro repositorio de GitHub.
    <a href="https://github.com/Andalu30/US-IISSI-EliteGamingCenter">https://github.com/Andalu30/US-IISSI-EliteGamingCenter</a>
  </p>


    <h3>Modelo de negocios y base de datos en 3FN:</h3>
    <object data="docs/Entregable Final.pdf" type="application/pdf" width="100%" height="100%">
      <p>Parece ser que no tu navegador no puede abrir correctamente el PDF.
         No te preocupes <a href="myfile.pdf">haz click aqui para descargarlo.</a></p>
    </object>




</body>


<footer>
  <?php include_once 'pie.php'; ?>
</footer>
</html>
