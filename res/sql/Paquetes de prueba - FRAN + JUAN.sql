CREATE OR REPLACE FUNCTION Assert_Equals (
  salida BOOLEAN,
  salida_esperada BOOLEAN)
  RETURN VARCHAR2 AS
BEGIN
  IF (salida = salida_esperada) THEN
    RETURN 'EXITO';
  END IF;
  IF(salida<>salida_esperada)
  THEN
    RETURN 'FALLO';
  END IF;
END;
/

Create or replace PACKAGE Pruebas_juegos AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,nombreJuego VARCHAR,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2,nombreJuego VARCHAR2,claveAnterior NUMBER,salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2, nombreJuego VARCHAR2, salidaEsperada BOOLEAN);
END  Pruebas_juegos;
/

--BODY!
create or replace package body Pruebas_juegos AS
  --inicializar.
  PROCEDURE INICIALIZAR (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador integer :=0;
  salida BOOLEAN :=true;

  BEGIN
  DELETE FROM JUEGOS;
  SELECT count(*) into contador from juegos;
  if(contador is NULL or contador = 0)THEN
    salida:=true;
  else
    salida:=false;
  end if;
    DBMS_OUTPUT.put_line('Pck: Pruebas_juegos, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
  commit work;
  END inicializar;

--insertar
  Procedure insertar(nombre_prueba VARCHAR2,nombreJuego VARCHAR,salidaEsperada BOOLEAN)AS
    salida Boolean :=true;
    filaJuegos juegos%rowtype;
    BEGIN
    Insert into JUEGOS values (CLAVE_JUEGOS.NEXTVAL,nombreJuego);
    SELECT * into filaJuegos FROM JUEGOS where NOMBRE=nombreJuego;
    IF(filaJuegos.Nombre<>nombreJuego)THEN
      salida :=false;
    end if;
    COMMIT WORK;
    DBMS_OUTPUT.put_line('Pck: Pruebas_Juegos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Juegos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

--actualizacion
PROCEDURE actualizar (nombre_prueba VARCHAR2,nombreJuego VARCHAR2, claveAnterior NUMBER,salidaEsperada BOOLEAN) AS
salida BOOLEAN :=true;
filaJUegos JUEGOS%ROWTYPE;
BEGIN

UPDATE JUEGOS set NOMBRE = nombreJuego WHERE OID_J = CLAVEANTERIOR;
Select * into filaJUegos from JUEGOS where OID_J=CLAVEANTERIOR;

IF(filaJUegos.nombre<>NOMBREJUEGO)THEN
salida:=false;
end if;
commit work;

DBMS_OUTPUT.put_line('Pck: PRuebas_JUEgos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_JUegos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
END actualizar;

PROCEDURE eliminar (nombre_prueba VARCHAR2, nombreJuego VARCHAR2, salidaEsperada BOOLEAN)AS
salida Boolean :=true;
numeroJuegos integer;
begin
DELETE FROM JUEGOS WHERE NOMBRE=nombreJuego;
SElect count(*) into numeroJuegos FROM JUEGOS where NOMBRE=nombreJuego;
if(numeroJuegos<>0)THEN
  salida :=false;
end if;
COMMIT WORK;
DBMS_OUTPUT.put_line('Pck: Pruebas_Juegos, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: PruebasJuegos, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_JUEGOS;
/

--Paquete Pruebas JuegosPC
create or replace PACKAGE Pruebas_juegosPC AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2,salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
END  Pruebas_juegosPC;
/

--Body
create or replace package body Pruebas_juegosPC AS
  --inicializar.
  PROCEDURE INICIALIZAR (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador integer :=0;
  salida BOOLEAN :=true;

  BEGIN
  DELETE FROM JUEGOS;
  DELETE FROM JUEGOSPCS;
  DELETE FROM PCS;
  SELECT count(*) into contador from juegosPCS;
  if(contador is NULL or contador = 0)THEN
    salida:=true;
  else
    salida:=false;
  end if;
    DBMS_OUTPUT.put_line('Pck: Pruebas_juegosPC, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
  commit work;
  END inicializar;

--insertar
  Procedure insertar(nombre_prueba VARCHAR2,salidaEsperada BOOLEAN)AS
    salida Boolean :=true;
    filaJuegosPC integer;
    oidJuego integer;
    oidPC integer;

    BEGIN
    Insert into JUEGOS values (CLAVE_JUEGOS.NEXTVAL,'JuegoAUX');
    Insert into JUEGOS values (CLAVE_JUEGOS.NEXTVAL,'Juego1');
    oidJuego:=CLAVE_JUEGOS.CURRVAL;
    Insert into PCS values (CLAVE_PCS.NEXTVAL,'Disponible','Normal',2);
    oidPC:=CLAVE_PCS.CURRVAL;
    Insert into JUEGOSPCS values(CLAVE_JUEGOSPCS.NEXTVAL,oidJuego,oidPC);
    SELECT count(*) into filaJuegosPC FROM JUEGOSPCS WHERE OID_J=OIDJUEGO;
    IF(filaJuegosPC=0)THEN
      salida :=false;
    end if;
    COMMIT WORK;
    DBMS_OUTPUT.put_line('Pck: Pruebas_JuegosPC, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_JuegosPC, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

--actualizacion
PROCEDURE actualizar (nombre_prueba VARCHAR2,salidaEsperada BOOLEAN) AS
salida BOOLEAN :=true;
filaJUegosPC integer;
oidjuego integer;
BEGIN

UPDATE JUEGOSPCS set OID_J = CLAVE_JUEGOS.CURRVAL-1;
oidjuego:=CLAVE_JUEGOS.CURRVAL-1;
Select count(*) into filaJUegosPC from JUEGOSPCS where OID_J=oidjuego;

IF(filaJUegosPC<>1)THEN
salida:=false;
end if;
commit work;

DBMS_OUTPUT.put_line('Pck: PRuebas_JUEgosPC, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_JUegosPC, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
END actualizar;


PROCEDURE eliminar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN)AS
salida Boolean :=true;
numeroJuegos integer;
oidpc integer;
begin
oidpc:=CLAVE_PCS.CURRVAL;
DELETE FROM JUEGOSPCS WHERE OID_PC=OIDPC;
SElect count(*) into numeroJuegos FROM JUEGOSPCS where OID_PC=oidpc;
if(numeroJuegos<>0)THEN
  salida :=false;
end if;
COMMIT WORK;
DBMS_OUTPUT.put_line('Pck: Pruebas_JuegosPC, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: PruebasJuegosPC, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_JUEGOSPC;
/

----------------------------------------------------------
--Paquete de pruebas PCS
create or replace PACKAGE PRUEBAS_PCS AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_pc IN pcs.oid_pc%type,w_estado IN pcs.estado%type,w_tipo_pc IN pcs.tipo_pc%type,
   w_precio IN pcs.precio%type,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2, w_oid_pc pcs.oid_pc%type,w_estado pcs.estado%type,w_tipo_pc pcs.tipo_pc%type,
   w_precio pcs.precio%type, salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_pc pcs.oid_pc%type, salidaEsperada BOOLEAN);
END PRUEBAS_PCS;
/
create or replace PACKAGE BODY PRUEBAS_PCS AS

  /* INICIALIZACIÃ“N */
  PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador    INTEGER :=0;
  salida  BOOLEAN :=true;
  BEGIN
     DELETE FROM JUEGOSPCS;
     DELETE FROM pcs; --  Dejar la tabla vacÃ­a de pcs
     SELECT COUNT(*) INTO contador FROM pcs;
     IF (contador IS NULL OR contador = 0) THEN
        salida := true;
     ELSE
        salida := false;
     END IF;
     DBMS_OUTPUT.put_line('Pck: Pruebas_Pcs, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
     COMMIT WORK;
  END inicializar;

/* INSERCIÃ“N DE PCS */
  PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_pc IN pcs.oid_pc%type,w_estado IN pcs.estado%type,w_tipo_pc IN pcs.tipo_pc%type,
  w_precio IN pcs.precio%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_pcs pcs%ROWTYPE;

  BEGIN
    /* Insertar pc */
    INSERT INTO pcs(oid_pc,estado,tipo_pc,precio) VALUES(w_oid_pc,w_estado,w_tipo_pc,w_precio);

    /* Seleccionar pc y comprobar que los datos se insertaron correctamente */
    SELECT * INTO Row_pcs FROM pcs WHERE oid_pc=w_oid_pc;
    IF (Row_pcs.estado<>w_estado
        or Row_pcs.tipo_pc<>w_tipo_pc
        or Row_pcs.precio<> w_precio) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Pcs, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Pcs, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

/* PRUEBA PARA LA ACTUALIZACIÃ“N DE PCS */
  PROCEDURE actualizar (nombre_prueba VARCHAR2, w_oid_pc pcs.oid_pc%type,w_estado pcs.estado%type,w_tipo_pc pcs.tipo_pc%type,
  w_precio pcs.precio%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_pcs pcs%ROWTYPE;
  BEGIN
    /* Actualizar PC */
    UPDATE pcs SET estado=w_estado,tipo_pc=w_tipo_pc,precio=w_precio  WHERE oid_pc=w_oid_pc;
    /* Seleccionar PC y comprobar que los campos se actualizaron correctamente */
    SELECT * INTO Row_pcs FROM pcs WHERE oid_pc=w_oid_pc;
    IF (Row_pcs.estado<>w_estado
        or Row_pcs.tipo_pc<>w_tipo_pc
        or Row_pcs.precio<> w_precio) THEN
      salida := false;
    END IF;
    COMMIT WORK;

    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Pcs, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_PCs, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END actualizar;
/* PRUEBA PARA LA ELIMINACIÃ“N DE PCS */
  PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_pc pcs.oid_pc%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    numero_pcs INTEGER;
  BEGIN
    /* Eliminar pc */
    DELETE FROM pcs WHERE oid_pc=w_oid_pc;
    /* Verificar que el PC no se encuentra en la BD */
    SELECT COUNT(*) INTO numero_pcs FROM pcs WHERE oid_pc=w_oid_pc;
    IF (numero_pcs <> 0) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_PCs, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Pcs, Proc:: eliminar-->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_PCS;
/
------------------------------------------------------
--PRUEBAS EN TABLAS PROVEEDORES--

  CREATE OR REPLACE PACKAGE PRUEBAS_PROVEEDORES AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_prov IN proveedores.oid_prov%type,w_nombre IN proveedores.nombre%type,w_direccion IN proveedores.direccion%type,w_telefono IN proveedores.telefono%type,w_email IN proveedores.email%type,w_finiciocontrato IN proveedores.finiciocontrato%type,w_ffincontrato IN proveedores.ffincontrato%type,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2,w_oid_prov proveedores.oid_prov%type,w_nombre proveedores.nombre%type,w_direccion proveedores.direccion%type,w_telefono proveedores.telefono%type,w_email proveedores.email%type,w_finiciocontrato proveedores.finiciocontrato%type,w_ffincontrato proveedores.ffincontrato%type, salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_prov proveedores.oid_prov%type, salidaEsperada BOOLEAN);
END PRUEBAS_PROVEEDORES;
/
 CREATE OR REPLACE PACKAGE BODY PRUEBAS_PROVEEDORES AS

  /* INICIALIZACIÃ“N */
  PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador    INTEGER :=0;
  salida  BOOLEAN :=true;
  BEGIN

     DELETE FROM PROVEEDORES; --  Dejar la tabla vacÃ­a de PROVEEDORES

     SELECT COUNT(*) INTO contador FROM proveedores;
     IF (contador IS NULL OR contador = 0) THEN
        salida := true;
     ELSE
        salida := false;
     END IF;
     DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedores, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
     COMMIT WORK;
  END inicializar;

/* INSERCIÃ“N DE PROVEEDORES */
  PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_prov IN proveedores.oid_prov%type,w_nombre IN proveedores.nombre%type,w_direccion IN proveedores.direccion%type,w_telefono IN proveedores.telefono%type,w_email IN proveedores.email%type,w_finiciocontrato IN proveedores.finiciocontrato%type,w_ffincontrato IN proveedores.ffincontrato%type,salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_proveedores proveedores%ROWTYPE;

  BEGIN
    /* Insertar proveedor */
    INSERT INTO proveedores VALUES(w_oid_prov,w_nombre,w_direccion,w_telefono,w_email,w_finiciocontrato,w_ffincontrato);

    /* Seleccionar proveedor y comprobar que los datos se insertaron correctamente */
    SELECT * INTO Row_proveedores FROM proveedores WHERE oid_prov=w_oid_prov;
    IF ( Row_proveedores.nombre<>w_nombre) THEN
      salida := false;
    END IF;
        IF (  Row_proveedores.direccion<>w_direccion) THEN
      salida := false;
    END IF;
        IF (Row_proveedores.telefono<>w_telefono) THEN
      salida := false;
    END IF;
        IF ( Row_proveedores.email<>w_email) THEN
      salida := false;
    END IF;
        IF (Row_proveedores.finiciocontrato<>w_finiciocontrato) THEN
      salida := false;
    END IF;
        IF (Row_proveedores.ffincontrato<>w_ffincontrato) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedore, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedores, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

/* PRUEBA PARA LA ACTUALIZACIÃ“N DE PROVEEDORES */
  PROCEDURE actualizar (nombre_prueba VARCHAR2,w_oid_prov proveedores.oid_prov%type,w_nombre proveedores.nombre%type,w_direccion proveedores.direccion%type,w_telefono proveedores.telefono%type,w_email proveedores.email%type,w_finiciocontrato proveedores.finiciocontrato%type,w_ffincontrato proveedores.ffincontrato%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_proveedores proveedores%ROWTYPE;
  BEGIN
    /* Actualizar Proveedor */
    UPDATE proveedores SET nombre=w_nombre,direccion=w_direccion,telefono=w_telefono,email=w_email,finiciocontrato=w_finiciocontrato,ffincontrato=w_ffincontrato
    WHERE oid_prov =w_oid_prov;
    /* Seleccionar PROVEEDOR y comprobar que los campos se actualizaron correctamente */
    SELECT * INTO Row_proveedores FROM proveedores WHERE oid_prov=w_oid_prov;
    IF ( Row_proveedores.nombre<>w_nombre
          or Row_proveedores.direccion<>w_direccion
          or Row_proveedores.telefono<>w_telefono
          or Row_proveedores.email<>w_email
          or Row_proveedores.finiciocontrato<>w_finiciocontrato
          or Row_proveedores.ffincontrato<>w_ffincontrato) THEN
      salida := false;
    END IF;
    COMMIT WORK;

    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Preoveedores, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedores, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END actualizar;
/* PRUEBA PARA LA ELIMINACIÃ“N DE proveedores */
  PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_prov proveedores.oid_prov%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    numero_proveedores INTEGER;
  BEGIN
    /* Eliminar PROVEEDOR */
    DELETE FROM proveedores WHERE oid_prov=w_oid_prov;
    /* Verificar que el PROVEEDOR no se encuentra en la BD */
    SELECT COUNT(*) INTO numero_proveedores FROM proveedores WHERE oid_prov=w_oid_prov;
    IF (numero_proveedores <> 0) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedores, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Proveedores, Proc:: eliminar-->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_PROVEEDORES;
/
-----------------------------------------------------------
--Pedidos
  CREATE OR REPLACE PACKAGE PRUEBAS_PEDIDOS AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_ped pedidos.oid_ped%type,w_orden_p pedidos.orden_p%type,w_oid_prov  pedidos.oid_prov%type,w_fechapedido pedidos.fechapedido%type,w_oid_trabajador pedidos.OID_TRABAJADOR%type,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2,w_oid_ped pedidos.oid_ped%type,w_orden_p pedidos.orden_p%type,w_oid_prov  pedidos.oid_prov%type,w_fechapedido pedidos.fechapedido%type,w_oid_trabajador pedidos.OID_TRABAJADOR%type,w_preciototal pedidos.preciototal%type, salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_ped pedidos.oid_ped%type, salidaEsperada BOOLEAN);
END PRUEBAS_PEDIDOS;
/

create or replace PACKAGE BODY PRUEBAS_PEDIDOS AS

  /* INICIALIZACIÃ“N */
  PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador    INTEGER :=0;
  salida  BOOLEAN :=true;
  BEGIN

     DELETE FROM PEDIDOS; --  Dejar la tabla vacÃ­a de PEDIDOS
     SELECT COUNT(*) INTO contador FROM PEDIDOS;
     IF (contador IS NULL OR contador = 0) THEN
        salida := true;
     ELSE
        salida := false;
     END IF;
     DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
     COMMIT WORK;
  END inicializar;

/* INSERCIÃ“N DE PEDIDOS */
  PROCEDURE insertar (nombre_prueba VARCHAR2, w_oid_ped pedidos.oid_ped%type,w_orden_p pedidos.orden_p%type,w_oid_prov  pedidos.oid_prov%type,w_fechapedido pedidos.fechapedido%type,w_oid_trabajador pedidos.oid_trabajador%type,salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_pedidos pedidos%ROWTYPE;

  BEGIN
    /* Insertar PEDIDO */
   insert into pedidos values(w_oid_ped,w_orden_p,w_oid_prov,w_fechapedido,w_oid_trabajador,0);

    /* Seleccionar PEDIDO y comprobar que los datos se insertaron correctamente */
    SELECT * INTO Row_pedidos FROM pedidos WHERE oid_ped=w_oid_ped;
    IF ( Row_pedidos.orden_p<>w_orden_p
        or Row_pedidos.oid_prov<>w_oid_prov
        or Row_pedidos.fechapedido<>w_fechapedido
        or Row_pedidos.oid_trabajador<>w_oid_trabajador
        ) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

/* PRUEBA PARA LA ACTUALIZACIÃ“N DE PEDIDOS */
  PROCEDURE actualizar (nombre_prueba VARCHAR2, w_oid_ped pedidos.oid_ped%type,w_orden_p pedidos.orden_p%type,w_oid_prov  pedidos.oid_prov%type,w_fechapedido pedidos.fechapedido%type,w_oid_trabajador pedidos.oid_trabajador%type,w_preciototal pedidos.preciototal%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_pedidos pedidos%ROWTYPE;
  BEGIN
    /* Actualizar PEDIDO */
    UPDATE pedidos SET orden_p=w_orden_p,oid_prov=w_oid_prov,fechapedido=w_fechapedido,oid_trabajador=w_oid_trabajador,preciototal=w_preciototal
    WHERE oid_ped =w_oid_ped;
    /* Seleccionar PEDIDO y comprobar que los campos se actualizaron correctamente */
    SELECT * INTO Row_pedidos FROM pedidos WHERE oid_ped =w_oid_ped;
    IF ( Row_pedidos.orden_p<>w_orden_p
        or Row_pedidos.oid_prov<>w_oid_prov
        or Row_pedidos.fechapedido<>w_fechapedido
        or Row_pedidos.oid_trabajador<>w_oid_trabajador
        or Row_pedidos.preciototal<>w_preciototal) THEN
      salida := false;
    END IF;
    COMMIT WORK;

    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END actualizar;
/* PRUEBA PARA LA ELIMINACIÃ“N DE PEDIDOS */
  PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_ped pedidos.oid_ped%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    numero_pedidos INTEGER;
  BEGIN
    /* Eliminar PEDIDO */
    DELETE FROM pedidos WHERE oid_ped=w_oid_ped;
    /* Verificar que el PEDIDO no se encuentra en la BD */
    SELECT COUNT(*) INTO numero_pedidos FROM pedidos WHERE oid_ped=w_oid_ped;
    IF (numero_pedidos <> 0) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Pedidos, Proc:: eliminar-->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_PEDIDOS;
/
-----------------------------
--LineasPedidos
  CREATE OR REPLACE PACKAGE PRUEBAS_LINEASPEDIDOS AS
   PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN);
   PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type,w_orden_lp lineaspedidos.orden_lp%type,w_oid_prod lineaspedidos.oid_prod%type,w_oid_ped lineaspedidos.oid_ped%type,w_cantidad lineaspedidos.cantidad%type,w_precio_producto lineaspedidos.precio_producto%type,salidaEsperada BOOLEAN);
   PROCEDURE actualizar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type,w_orden_lp lineaspedidos.orden_lp%type,w_oid_prod lineaspedidos.oid_prod%type,w_oid_ped lineaspedidos.oid_ped%type,w_cantidad lineaspedidos.cantidad%type,w_precio_producto lineaspedidos.precio_producto%type, salidaEsperada BOOLEAN);
   PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type, salidaEsperada BOOLEAN);
END PRUEBAS_LINEASPEDIDOS;
/
CREATE OR REPLACE PACKAGE BODY PRUEBAS_LINEASPEDIDOS AS

  /* INICIALIZACIÃ“N */
  PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  contador    INTEGER :=0;
  salida  BOOLEAN :=true;
  BEGIN

     DELETE FROM LINEASPEDIDOS; --  Dejar la tabla vacÃ­a de LINEASPEDIDOS

     SELECT COUNT(*) INTO contador FROM LINEASPEDIDOS;
     IF (contador IS NULL OR contador = 0) THEN
        salida := true;
     ELSE
        salida := false;
     END IF;
     DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
     COMMIT WORK;
  END inicializar;

/* INSERCIÃ“N DE PEDIDOS */
  PROCEDURE insertar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type,w_orden_lp lineaspedidos.orden_lp%type,w_oid_prod lineaspedidos.oid_prod%type,w_oid_ped lineaspedidos.oid_ped%type,w_cantidad lineaspedidos.cantidad%type,w_precio_producto lineaspedidos.precio_producto%type,salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_lineaspedidos lineaspedidos%ROWTYPE;

  BEGIN
    /* Insertar LINEASPEDIDO */
 insert into lineaspedidos values(w_oid_lp,w_orden_lp,w_oid_prod,w_oid_ped,w_cantidad,w_precio_producto);

    /* Seleccionar LINEASPEDIDO y comprobar que los datos se insertaron correctamente */
    SELECT * INTO Row_lineaspedidos FROM lineaspedidos WHERE oid_lp=w_oid_lp;
    IF ( Row_lineaspedidos.oid_lp<>w_oid_lp
        or  Row_lineaspedidos.orden_lp<>w_orden_lp
        or  Row_lineaspedidos.oid_prod<>w_oid_prod
        or  Row_lineaspedidos.oid_ped<>w_oid_ped
        or  Row_lineaspedidos.cantidad<>w_cantidad
        or  Row_lineaspedidos.precio_producto<>w_precio_producto) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END insertar;

/* PRUEBA PARA LA ACTUALIZACIÃ“N DE PEDIDOS */
  PROCEDURE actualizar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type,w_orden_lp lineaspedidos.orden_lp%type,w_oid_prod lineaspedidos.oid_prod%type,w_oid_ped lineaspedidos.oid_ped%type,w_cantidad lineaspedidos.cantidad%type,w_precio_producto lineaspedidos.precio_producto%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    Row_lineaspedidos lineaspedidos%ROWTYPE;
  BEGIN
    /* Actualizar LINEASPEDIDO */
    UPDATE lineaspedidos SET oid_lp=w_oid_lp,orden_lp=w_orden_lp,oid_prod=w_oid_prod,oid_ped=w_oid_ped,cantidad=w_cantidad,precio_producto=w_precio_producto
    WHERE oid_lp =w_oid_lp;
    /* Seleccionar LINEASPEDIDO y comprobar que los campos se actualizaron correctamente */
    SELECT * INTO Row_lineaspedidos FROM lineaspedidos WHERE oid_lp =w_oid_lp;
    IF (Row_lineaspedidos.oid_lp<>w_oid_lp
        or  Row_lineaspedidos.orden_lp<>w_orden_lp
        or  Row_lineaspedidos.oid_prod<>w_oid_prod
        or  Row_lineaspedidos.oid_ped<>w_oid_ped
        or  Row_lineaspedidos.cantidad<>w_cantidad
        or  Row_lineaspedidos.precio_producto<>w_precio_producto) THEN
      salida := false;
    END IF;
    COMMIT WORK;

    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END actualizar;
/* PRUEBA PARA LA ELIMINACIÃ“N DE LINEASPEDIDOS */
  PROCEDURE eliminar (nombre_prueba VARCHAR2,w_oid_lp lineaspedidos.oid_lp%type, salidaEsperada BOOLEAN) AS
    salida BOOLEAN := true;
    numero_lineaspedidos INTEGER;
  BEGIN
    /* Eliminar INEASPEDIDO */
    DELETE FROM lineaspedidos WHERE oid_lp=w_oid_lp;
    /* Verificar que el PEDIDO no se encuentra en la BD */
    SELECT COUNT(*) INTO numero_lineaspedidos FROM lineaspedidos WHERE oid_lp=w_oid_lp;
    IF (numero_lineaspedidos <> 0) THEN
      salida := false;
    END IF;
    COMMIT WORK;
    /* Mostrar resultado de la prueba */
    DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_LineasPedidos, Proc:: eliminar-->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  END eliminar;
END PRUEBAS_LINEASPEDIDOS;
/
-----------------------
--Usuarios
create or replace PACKAGE Pruebas_Usuarios AS
  PROCEDURE inicializar (nombre_prueba VARCHAR2,salidaEsperada BOOLEAN);
  PROCEDURE insertar (nombre_prueba VARCHAR2, dni char,  salidaEsperada BOOLEAN);
  PROCEDURE actualizar (nombre_prueba VARCHAR2, dni char, salidaEsperada BOOLEAN);
  PROCEDURE eliminar (nombre_prueba VARCHAR2, dni char, salidaEsperada BOOLEAN);
END  Pruebas_Usuarios;
/

--Body
create or replace package body Pruebas_Usuarios AS

  --inicializar.
  Procedure inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  salida boolean;
  cont integer;
  begin
  DELETE FROM USUARIOS CASCADE;
  Select count(*) into cont from usuarios;
  IF(cont<>0)then
    salida:=false;
  else
    salida:=true;
  end if;
  Commit work;
      DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
  WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end inicializar;

  --insertar
  procedure insertar(nombre_prueba VARCHAR2,dni char,salidaEsperada BOOLEAN)AS
  salida boolean;
  cont integer;
  begin
  INSERT INTO USUARIOS values (clave_usuarios.CURRVAL, dni, 'emailtest1@etsii.com','Direccion','123456798','Nombre','Apellidos','contraseña1',sysdate);
  SELECT count(*) into cont from usuarios where DNI=dni;
  if(cont=0)then
    salida :=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end insertar;

  --actualizar
  procedure actualizar(nombre_Prueba VARCHAR2,dni char,salidaEsperada BOOLEAN)AS
  filausuarios USUARIOS%ROWTYPE;
  salida boolean;
  begin

  UPDATE USUARIOS SET Nombre='NombreNuevo' WHERE DNI=dni;
  SELECT * into filausuarios FROM USUARIOS WHERE DNI=dni;
  IF(filausuarios.NOMBRE<>'NombreNuevo')THEN
    salida:=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
   WHEN OTHERS THEN
       DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
       ROLLBACK;
  end actualizar;
  --eliminar
  procedure eliminar (nombre_prueba VARCHAR2,dni char,salidaEsperada BOOLEAN)AS
    contador integer;
    salida boolean;
  begin
    DELETE FROM USUARIOS WHERE DNI=dni;
    SELECT count(*) into contador FROM USUARIOS WHERE DNI=dni;
    IF(contador<>0)THEN
      salida:=false;
    else
      salida:=true;
    end if;
    commit work;
    DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('Pck: Pruebas_Usuarios, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
      ROLLBACK;
  end eliminar;
  END Pruebas_Usuarios;
  /

  --CLIENTES
create or replace PACKAGE Pruebas_Clientes AS
  PROCEDURE inicializar (nombre_prueba VARCHAR2,salidaEsperada BOOLEAN);
  PROCEDURE insertar (nombre_prueba VARCHAR2,dni char,nickname VARCHAR2,salidaEsperada BOOLEAN);
  PROCEDURE actualizar (nombre_Prueba VARCHAR2,dni char,nuevoNickname VARCHAR2,salidaEsperada BOOLEAN);
  PROCEDURE eliminar (nombre_prueba VARCHAR2, oid char, salidaEsperada BOOLEAN);
END  Pruebas_Clientes;
/

--Body
create or replace package body Pruebas_Clientes AS

  --inicializar.
  Procedure inicializar (nombre_prueba VARCHAR2, salidaEsperada BOOLEAN) AS
  salida boolean;
  cont integer;
  begin
  DELETE FROM USUARIOS CASCADE;
  DELETE FROM CLIENTES CASCADE;
  DELETE FROM TRABAJADORES CASCADE;
  Select count(*) into cont from CLIENTES;
  IF(cont<>0)then
    salida:=false;
  else
    salida:=true;
  end if;
  Commit work;
      DBMS_OUTPUT.put_line('Pck: Pruebas_Clientes, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
  WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Clientes, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end inicializar;

  --insertar
  procedure insertar(nombre_prueba VARCHAR2,dni char,nickname VARCHAR2,salidaEsperada BOOLEAN)AS
  salida boolean;
  cont integer;
  begin
  crearcliente('12345678A','email','direccion','12345678','Nombre','Apellidos','ed076287532e86365e841e92bfc50d8c',Sysdate,'nickname',0);
  SELECT count(*) into cont from CLIENTES where DNI=dni;
    if(cont=0)then
    salida :=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_Clientes, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_CLientes, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end insertar;

  --actualizar
  procedure actualizar(nombre_Prueba VARCHAR2,dni char,nuevoNickname VARCHAR2,salidaEsperada BOOLEAN)AS
  filausuarios CLIENTES%ROWTYPE;
  salida boolean;
  begin

  UPDATE CLIENTES SET NICKNAME=nuevoNickname WHERE DNI=dni;
  SELECT * into filausuarios FROM CLIENTES WHERE DNI=dni;
  IF(filausuarios.NICKNAME<>nuevoNickname)THEN
    salida:=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_CLIENTES, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
   WHEN OTHERS THEN
       DBMS_OUTPUT.put_line('Pck: Pruebas_CLIENTES, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
       ROLLBACK;
  end actualizar;
  --eliminar
  procedure eliminar (nombre_prueba VARCHAR2,oid char,salidaEsperada BOOLEAN)AS
    contador integer;
    salida boolean;
  begin
    DELETE FROM CLIENTES WHERE OID_CLIENTE=oid;
    DELETE FROM USUARIOS WHERE OID_USUARIO=oid;
    SELECT count(*) into contador FROM CLIENTES WHERE OID_CLIENTE=oid;
    IF(contador<>0)THEN
      salida:=false;
    else
      salida:=true;
    end if;
    commit work;
    DBMS_OUTPUT.put_line('Pck: Pruebas_clientes, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('Pck: Pruebas_clientes, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
      ROLLBACK;
  end eliminar;
  END Pruebas_clientes;
  /

  --Trabajadores
  create or replace PACKAGE Pruebas_Trabajadores AS
  PROCEDURE inicializar (nombre_prueba VARCHAR2, salidaEsperada boolean);
  PROCEDURE insertar (nombre_prueba VARCHAR2,dni char,numSS CHAR,salidaEsperada BOOLEAN);
  PROCEDURE actualizar (nombre_Prueba VARCHAR2,oid char,nuevoSS VARCHAR2,salidaEsperada BOOLEAN);
  PROCEDURE eliminar (nombre_prueba VARCHAR2,oid char,salidaEsperada BOOLEAN);
END  Pruebas_Trabajadores;
/

--Body
create or replace package body Pruebas_Trabajadores AS

  --inicializar.
  Procedure inicializar (nombre_prueba VARCHAR2, salidaEsperada boolean) AS
  salida boolean;
  cont integer;
  begin
  DELETE FROM USUARIOS CASCADE;
  DELETE FROM CLIENTES CASCADE;
  DELETE FROM TRABAJADORES CASCADE;
  Select count(*) into cont from TRABAJADORES;
  IF(cont<>0)then
    salida:=false;
  else
    salida:=true;
  end if;
  Commit work;
      DBMS_OUTPUT.put_line('Pck: Pruebas_TRabajadores, Proc:: inicializar -->'||nombre_prueba|| ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
  WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end inicializar;

  --insertar
  procedure insertar(nombre_prueba VARCHAR2,dni char,numSS CHAR,salidaEsperada BOOLEAN)AS
  salida boolean;
  cont integer;
  begin

  creartrabajador(dni,'emailTrabajador@gmail.com','direccion','123456789','Nombre','Apellidos','Contraseña',sysdate,numSS,123456789,sysdate,sysdate,1000,'NO');


  SELECT count(*) into cont from USUARIOS where DNI=dni;
    if(cont=0)then
    salida :=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: insertar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
          ROLLBACK;
  end insertar;

  --actualizar
  procedure actualizar(nombre_Prueba VARCHAR2,oid char,nuevoSS VARCHAR2,salidaEsperada BOOLEAN)AS
  filausuarios TRABAJADORES%ROWTYPE;
  salida boolean;
  begin

  UPDATE TRABAJADORES SET NUMEROSSSS=nuevoSS WHERE OID_TRABAJADOR=oid;
  SELECT * into filausuarios FROM TRABAJADORES WHERE OID_TRABAJADOR=oid;
  IF(filausuarios.NUMEROSSSS<>nuevoSS)THEN
    salida:=false;
  else
    salida:=true;
  end if;
  commit work;
  DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
  EXCEPTION
   WHEN OTHERS THEN
       DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: actualizar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
       ROLLBACK;
  end actualizar;
  --eliminar
  procedure eliminar (nombre_prueba VARCHAR2,oid char,salidaEsperada BOOLEAN)AS
    contador integer;
    salida boolean;
  begin
    DELETE FROM TRABAJADORES WHERE OID_TRABAJADOR=oid;
    DELETE FROM USUARIOS WHERE DNI=dni;
    SELECT count(*) into contador FROM TRABAJADORES WHERE OID_TRABAJADOR=oid;
    IF(contador<>0)THEN
      salida:=false;
    else
      salida:=true;
    end if;
    commit work;
    DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: eliminar -->'||nombre_prueba || ':' || ASSERT_EQUALS(salida,salidaEsperada));
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.put_line('Pck: Pruebas_Trabajadores, Proc:: inicializar -->'||nombre_prueba || ':' || ASSERT_EQUALS(false,salidaEsperada));
      ROLLBACK;
  end eliminar;
  END Pruebas_Trabajadores;
  /
