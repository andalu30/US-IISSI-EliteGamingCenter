-----------------Set ServerOutput-------------------------------------
SET SERVEROUTPUT ON;

DECLARE
claveanterior integer;
oid_pc_primero integer;
oid_pc_segundo integer;
w_oid_prov_guillermo integer;
w_oid_prov_daniel integer;
w_oid_prov_manuel varchar2(50);
w_oid_prov_rosano varchar2(50);
w_oid_ped1 integer;
w_oid_ped2 integer;
w_oid_ped3 integer;
w_oid_pc integer;
w_oid_prov integer;
w_oid_prov1 integer;
w_oid_prod integer;
w_oid_prod1 integer;
w_oid_prod2 integer;
w_oid_prod3 integer;
w_oid_ped integer;
w_oid_ped1a integer;
w_oid_lp integer;
w_oid_lp1 integer;
variable_trabajador integer;
variable_trabajador1 integer;
BEGIN
delete pedidos;
delete lineaspedidos;

delete pcs;
delete proveedores;
delete trabajadores;
delete clientes;
delete usuarios;
--Juegos
DBMS_OUTPUT.put_line('PRUEBAS DE JUEGOS');
PRUEBAS_JUEGOS.INICIALIZAR('Inicializacion de tabla.',true);
PRUEBAS_JUEGOS.INSERTAR('Introducir 1 juego.','League of Legends',true);
claveanterior:=CLAVE_JUEGOS.CURRVAL;
PRUEBAS_JUEGOS.ACTUALIZAR('Actualizar el nombre de un juego.','League of Legends',claveanterior,true);
PRUEBAS_JUEGOS.ELIMINAR('Eliminar un juego','League of Legends',true);
PRUEBAS_JUEGOS.INSERTAR('Introducir otro juego con nombre null.',null,false);
PRUEBAS_JUEGOS.INSERTAR('Introducir otro juego con nombre repetido.','League of Legends',true);

--JuegosPCS
DBMS_OUTPUT.put_line('PRUEBAS DE JUEGOSPCS');
PRUEBAS_JUEGOSPC.INICIALIZAR('Inicializacion',true);
PRUEBAS_JUEGOSPC.INSERTAR('Insertar',true);
PRUEBAS_JUEGOSPC.ACTUALIZAR('Actualizar',false);
PRUEBAS_JUEGOSPC.ELIMINAR('Eliminar',true);

--PCS
DBMS_OUTPUT.put_line('PRUEBAS DE PCS');
PRUEBAS_PCS.INICIALIZAR('P0-PCs: Vaciar tabla',true);
PRUEBAS_PCS.INSERTAR('P1-Pcs:Insercciñn de Pc normal-Y-',clave_pcs.nextval,'disponible','normal','200,5',false);
oid_pc_primero:=clave_pcs.currval;
PRUEBAS_PCS.INSERTAR('P1b-Pcs:Insercciñn de Pc normal -Y-',clave_pcs.nextval,'disponible','normal','2,5',false);
oid_pc_segundo:=clave_pcs.currval;
PRUEBAS_PCS.INSERTAR('P3-Pcs:Insercciñn de Pc con atributo estado null -N-',clave_pcs.nextval,null,'normal','2,5', false);
PRUEBAS_PCS.INSERTAR('P4-Pcs:Insercciñn de Pc con atributo tipo_pc null -N-',clave_pcs.nextval,'disponible',null,'2,5', false);
PRUEBAS_PCS.INSERTAR('P4-Pcs:Insercciñn de Pc con atributo precio null -Y-',clave_pcs.nextval,'disponible','vip',null, false);
PRUEBAS_PCS.ACTUALIZAR('P5-Pcs: Actualizar datos del pc con identificador '||oid_pc_primero||'-Y-',oid_pc_primero,'disponible','vip','3,5', false);
PRUEBAS_PCS.ACTUALIZAR('P6-Pcs: Actualizar estado del pc con identificador '||oid_pc_primero||' a null -N-',oid_pc_primero,null,'vip','2,5', false);
PRUEBAS_PCS.ACTUALIZAR('P7-Pcs: Actualizar tipo_pc del pc con identificador '||oid_pc_primero||' a null -N-',oid_pc_primero,'Disponible',null,'2,5', false);
PRUEBAS_PCS.ELIMINAR('P8-Pcs: Eliminar tipo_pc del pc con identificador '||oid_pc_segundo||'-Y-',oid_pc_segundo, true);


--Proveedores
DBMS_OUTPUT.put_line('PRUEBAS DE PROVEEDORES');
PRUEBAS_PROVEEDORES.INICIALIZAR('P0-Proveedores: Vaciar tabla',true);

PRUEBAS_PROVEEDORES.INSERTAR('P1a-Proveedores:Insercciñn de proveedor"Francisco Bennitez" -Y-',clave_proveedores.NEXTVAL,'Francisco Benitez','calle triana nñ 5','123467842','francis@gmail.es',TO_DATE('20/10/2014','DD/MM/YYYY'),TO_DATE('20/11/2015','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P1b-Proveedores:Insercciñn de proveedor "Guilermo Jimenez"-Y-',clave_proveedores.NEXTVAL,'Guillermo Jimenez','calle giralda nñ 5','123467842','guille1@gmail.es',TO_DATE('20/01/2016','DD/MM/YYYY'),TO_DATE('20/11/2017','DD/MM/YYYY'),false);
w_oid_prov_guillermo:=clave_proveedores.currval;
PRUEBAS_PROVEEDORES.INSERTAR('P1c-Proveedores:Insercciñn de proveedor "Daniel Vazquez" -Y-',clave_proveedores.NEXTVAL,'Daniel Vazquez','calle pietro nñ 10','123467842','daniel@gmail.es',TO_DATE('20/01/2018','DD/MM/YYYY'),TO_DATE('20/11/2019','DD/MM/YYYY'),false);
w_oid_prov_daniel:=clave_proveedores.currval;
PRUEBAS_PROVEEDORES.INSERTAR('P2-Proveedores:Insercciñn de proveedor con nombre null -N-',clave_proveedores.NEXTVAL,null,'calle triana nñ 5','123467842','francis1@gmail.es',TO_DATE('20/10/2013','DD/MM/YYYY'),TO_DATE('20/10/2014','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P3-Proveedores:Insercciñn de proveedor con direccion null -N-',clave_proveedores.NEXTVAL,'Francisco Alonso',null,'123467842','francis2@gmail.es',TO_DATE('20/10/2012','DD/MM/YYYY'),TO_DATE('20/10/2013','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P4-Proveedores:Insercciñn de proveedor con telefono null -N-',clave_proveedores.NEXTVAL,'Francisco Alonso','calle triana nñ 5',null,'francis3@gmail.es',TO_DATE('20/10/2011','DD/MM/YYYY'),TO_DATE('20/10/2012','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P5-Proveedores:Insercciñn de proveedor con email null -N-',clave_proveedores.NEXTVAL,'Francisco Alonso','calle triana nñ 5',null,'francis4@gmail.es',TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P6-Proveedores:Insercciñn de proveedor "Juan Gonzales" con fechaInicio entre fechaInicio y fechaFin, y con fechaFin mayor que fehchaFin de un proveedor ya existente  -N-',
clave_proveedores.NEXTVAL,'Juan Gonzales','calle marisma nñ 5','123467842','juan@gmail.es',TO_DATE('29/11/2014','DD/MM/YYYY'),TO_DATE('20/12/2015','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.INSERTAR('P7-Proveedores:Insercciñn de proveedor con fechaInicio entre fechaInicio y fechaFin, y con fechaFin igual que fehchaFin de un proveedor ya existente  -Y-',clave_proveedores.NEXTVAL,'Juan Gonzales','calle marisma nñ 5','123467842','juan@gmail.es',TO_DATE('20/04/2015','DD/MM/YYYY'),TO_DATE('20/11/2015','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ACTUALIZAR('P8-Proveedores:Actualizaciñn de proveedor "Guillermo Jimenez "  -Y-',w_oid_prov_guillermo,'Guillermo Jimenez','calle trajano nñ89','122989852','guille6@gmail.es',TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ACTUALIZAR('P9-Proveedores:Actualizaciñn de proveedor "Guillermo Jimenez con nombre null " -N-',w_oid_prov_guillermo,null,'calle trajano nñ89','122989852','guille6@gmail.es',TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ACTUALIZAR('P10-Proveedores:Actualizaciñn de proveedor "Guillermo Jimenez " con calle null -N-',w_oid_prov_guillermo,'Guillermo Jimenez',null,'122989852','guille6@gmail.es',TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ACTUALIZAR('P11-Proveedores:Actualizaciñn de proveedor "Guillermo Jimenez " con telefono null-N-',w_oid_prov_guillermo,'Guillermo Jimenez','calle trajano nñ89',null,'guille6@gmail.es',TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ACTUALIZAR('P12-Proveedores:Actualizaciñn de proveedor "Guillermo Jimenez " con email null -N-',w_oid_prov_guillermo,'Guillermo Jimenez','calle trajano nñ89','122989852',null,TO_DATE('20/10/2010','DD/MM/YYYY'),TO_DATE('20/11/2011','DD/MM/YYYY'),false);
PRUEBAS_PROVEEDORES.ELIMINAR('P13-Proveedores: Eliminar proveedor "Daniel"-Y-',w_oid_prov_daniel, true);


--Pedidos

DBMS_OUTPUT.put_line('PRUEBAS DE PEDIDOS');

CREARTRABAJADOR('87654321Z','franap@gmail.com','calle francia','123456789','Francisco',' Belmomnte','qw12345678',to_date('03/06/1997','DD/MM/YYYY'),
'324152678','1234 1234 1234 5678',to_date('01/01/2016','DD/MM/YYYY'),to_date('01/01/2017','DD/MM/YYYY'),1500,'NO');
variable_trabajador:=clave_trabajadores.currval;
INSERT INTO proveedores values(clave_proveedores.nextval,'Manuel Alonso','calle italia nñ 5','123467842','manu@gmail.es',TO_DATE('20/01/2016','DD/MM/YYYY'),TO_DATE('20/11/2017','DD/MM/YYYY'));
w_oid_prov_manuel:=clave_proveedores.currval;
INSERT INTO proveedores values(clave_proveedores.nextval,'Rosano Beltrñn','calle italia nñ 5','123467842','rosano@gmail.es',TO_DATE('21/11/2017','DD/MM/YYYY'),TO_DATE('20/11/2018','DD/MM/YYYY'));
w_oid_prov_rosano:=clave_proveedores.currval;
PRUEBAS_PEDIDOS.INICIALIZAR('P0-Pedidos: Vaciar tabla',true);
PRUEBAS_PEDIDOS.INSERTAR('P1a-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel Alonso" -Y-',clave_pedidos.nextval,'1',w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
w_oid_ped1:=clave_pedidos.currval;
PRUEBAS_PEDIDOS.INSERTAR('P1b-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel Alonso" -Y-',clave_pedidos.nextval,'2',w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
w_oid_ped2:=clave_pedidos.currval;
PRUEBAS_PEDIDOS.INSERTAR('P1c-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel Alonso" -Y-',clave_pedidos.nextval,'3',w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
w_oid_ped3:=clave_pedidos.currval;
PRUEBAS_PEDIDOS.INSERTAR('P2-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel Alonso"con orden_p null -N-',clave_pedidos.nextval,null,w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
PRUEBAS_PEDIDOS.INSERTAR('P3-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel"con clave_ped null -N-',null,'2',w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
PRUEBAS_PEDIDOS.INSERTAR('P4-Pedidos:Insercciñn de un pedido normal a un proveedor no existente -N-',clave_pedidos.nextval,'2',w_oid_prov_manuel+4,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,false);
PRUEBAS_PEDIDOS.INSERTAR('P5-Pedidos:Insercciñn de un pedido normal a proveedor "Manuel Alonso", con trabajador no existente -N-',clave_pedidos.nextval,'2',w_oid_prov_manuel,to_date('20/05/2015','DD/MM/YYYY'),500,false);
PRUEBAS_PEDIDOS.ACTUALIZAR('P6-Pedidos:Actualizaciñn del pedido con oid_ped='||w_oid_ped2||' -Y- ',w_oid_ped2,'1',w_oid_prov_rosano,to_date('30/06/2015','DD/MM/YYYY'),variable_trabajador,0,false);
PRUEBAS_PEDIDOS.ACTUALIZAR('P7--Pedidos:Actualizaciñn de dni_t a un dni_t no existente,del pedido con oid_ped='||w_oid_ped2||', a proveedor "Rosano"-Y- ',w_oid_ped2,'1',w_oid_prov_rosano,to_date('30/06/2015','DD/MM/YYYY'),500,0,false);
PRUEBAS_PEDIDOS.ACTUALIZAR('P8-Pedidos:Actualizaciñn de clave_ped a null del pedido con oid_ped='||w_oid_ped2||', a proveedor "Rosano" -N-',null,'1',w_oid_prov_rosano,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,0,false);
PRUEBAS_PEDIDOS.ACTUALIZAR('P9-Pedidos:Actualizaciñn de orden_p a null del pedido con oid_ped='||w_oid_ped2||', a proveedor "Rosano" -N-',w_oid_ped2,null,w_oid_prov_rosano,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,0,false);
PRUEBAS_PEDIDOS.ACTUALIZAR('P10-Pedidos:Actualizaciñn de oid_prov a una oid_prov no existente del pedido con oid_ped='||w_oid_ped2||', a proveedor "Rosano" -N-',w_oid_ped2,'1',w_oid_prov_rosano+5,to_date('20/05/2015','DD/MM/YYYY'),variable_trabajador,0,false);
PRUEBAS_PEDIDOS.ELIMINAR('P11-Pedidos: Eliminar pedido con oid_ped='||w_oid_ped3||' a proveedor Rosano-Y-', w_oid_ped3, true);
--LineasPedidos.

DBMS_OUTPUT.put_line('PRUEBAS DE LINEA PEDIDOS');
crearTrabajador('07654321Z','franap42@gmail.com','calle principado nñ2','098076054','Francisco Josñ','Alonso','123456hjy',to_date('03/06/1997','DD/MM/YYYY'),
'1357912','1262 12762 123',to_date('10/02/2016','DD/MM/YYYY'),to_date('10/02/2017','DD/MM/YYYY'),2000,'no');
variable_trabajador1:=clave_trabajadores.CURRVAL;

INSERT INTO PCS VALUES (clave_pcs.nextval,'disponible','normal',2.5);
w_oid_pc:=clave_pcs.currval;

INSERT INTO PROVEEDORES VALUES(clave_proveedores.nextval,'Natalia Condñn','calle guadalajara','135790012','natalia@hotmail.com',to_date('04/03/2013','DD/MM/YYYY'),to_date('04/09/2013','DD/MM/YYYY'));
w_oid_prov:=clave_proveedores.currval;

INSERT INTO PROVEEDORES VALUES(clave_proveedores.nextval,'Natalia Herrera','calle guatemala','135790012','nataliaherr@hotmail.com',to_date('20/05/2013','DD/MM/YYYY'),to_date('04/09/2013','DD/MM/YYYY'));
w_oid_prov1:=clave_proveedores.currval;

INSERT INTO PRODUCTOS VALUES(clave_productos.nextval,'teclado mecanico',70.5,50,3);
w_oid_prod:=clave_productos.currval;

INSERT INTO PRODUCTOS VALUES(clave_productos.nextval,'teclado mecanico1',62.5,50,3);
w_oid_prod1:=clave_productos.currval;

INSERT INTO PRODUCTOS VALUES(clave_productos.nextval,'teclado mecanico2',62.5,50,3);
w_oid_prod2:=clave_productos.currval;

INSERT INTO PRODUCTOS VALUES(clave_productos.nextval,'teclado mecanico3',62.5,50,3);
w_oid_prod2:=clave_productos.currval;

insert into pedidos values (clave_pedidos.nextval,'1',w_oid_prov,to_date('07/05/2013','DD/MM/YYYY'),variable_trabajador1,0.0);
w_oid_ped:=clave_pedidos.currval;

insert into pedidos values (clave_pedidos.nextval,'1',w_oid_prov1,to_date('09/05/2013','DD/MM/YYYY'),variable_trabajador1,0.0);
w_oid_ped1a:=clave_pedidos.currval;

PRUEBAS_LINEASPEDIDOS.INICIALIZAR('P0-Lienaspedidos: Vaciar tabla',true);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P1a-Lineaspedidos:Insercciñn normal de una linea de pedido con oid_ped='||w_oid_ped||'-Y-',clave_lineaspedidos.nextval,'1',w_oid_prod,w_oid_ped,2,'25,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P1b-Lineaspedidos:Insercciñn normal en de una linea de pedido  con oid_ped='||w_oid_ped||'-Y-',clave_lineaspedidos.nextval,'2',w_oid_prod1,w_oid_ped,2,'26,5',false);
w_oid_lp:=clave_lineaspedidos.currval;
PRUEBAS_LINEASPEDIDOS.INSERTAR('P1c-Lineaspedidos:Insercciñn normal en de una linea de pedido  con oid_ped='||w_oid_ped1||'-Y-',clave_lineaspedidos.nextval,'3',w_oid_prod1,w_oid_ped1a,2,'26,5',false);
w_oid_lp1:=clave_lineaspedidos.currval;
PRUEBAS_LINEASPEDIDOS.INSERTAR('P2-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped='||w_oid_ped||'con producto repetido-N-',clave_lineaspedidos.nextval,'3',w_oid_prod1,w_oid_ped,2,'26,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P3-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped='||w_oid_ped||'con orden_lp repetida-N-',clave_lineaspedidos.nextval,'2',w_oid_prod2,w_oid_ped,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P4-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped='||w_oid_ped||'con orden_lp null-N-',clave_lineaspedidos.nextval,null,w_oid_prod2,w_oid_ped,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P5-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped='||w_oid_ped||'con oid_prod null-N-',clave_lineaspedidos.nextval,'4',null,w_oid_ped,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P6-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped null-N-',clave_lineaspedidos.nextval,'5',w_oid_prod2,null,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P7-Lineaspedidos:Insercciñn de una linea de pedido con oid_ped='||w_oid_ped||'con cantidad null-N-',clave_lineaspedidos.nextval,'6',w_oid_prod2,w_oid_ped,null,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P8-Lineaspedidos:Insercciñn de una linea de pedido con pedido inexistente -N-',clave_lineaspedidos.nextval,'7',w_oid_prod2,w_oid_ped+5,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.INSERTAR('P9-Lineaspedidos:Insercciñn de una linea de pedido con producto inexistente -N-',clave_lineaspedidos.nextval,'8',w_oid_prod2+5,w_oid_ped,2,'30,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P10-Lineaspedidos:Actualizaciñn de una linea de pedido',w_oid_lp,'1',w_oid_prod2,w_oid_ped1a,4,'30,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P11-Lineaspedidos:Actualizaciñn de una linea de pedido con oid_lp null -N-',null,'2',w_oid_prod,w_oid_ped1a,4,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P12-Lineaspedidos:Actualizaciñn de una linea de pedido con oid_prod null -N-',w_oid_lp,'2',null,w_oid_ped1a,4,'30,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P13Lineaspedidos:Actualizaciñn de una linea de pedido con oid_ped null -N-',w_oid_lp,'2',w_oid_prod,null,4,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P14-Lineaspedidos:Actualizaciñn de una linea de pedido con cantidad null -N-',w_oid_lp,'2',w_oid_prod,w_oid_ped1a,null,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P15-Lineaspedidos:Actualizaciñn de una linea de pedido con orden_lp repetida -N-',w_oid_lp,'1',w_oid_prod,w_oid_ped1a,4,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P16-Lineaspedidos:Actualizaciñn de una linea de pedido con producto inexistente -N-',w_oid_lp,'2',w_oid_prod+5,w_oid_ped1a,4,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ACTUALIZAR('P17-Lineaspedidos:Actualizaciñn de una linea de pedido con pedido inexistente -N-',w_oid_lp,'2',w_oid_prod,w_oid_ped1a+5,4,'25,5',false);
PRUEBAS_LINEASPEDIDOS.ELIMINAR('P18-ineaspedidos:Eliminaciñn de una linea de pedido con pedido no perteneciente al proveedor -N-',w_oid_lp,true);

--Usuarios
DBMS_OUTPUT.put_line('PRUEBAS DE USUARIOS');

PRUEBAS_USUARIOS.INICIALIZAR('Inicializar -->',true);
PRUEBAS_USUARIOS.INSERTAR('Insertar--->','12345678A',true);
PRUEBAS_USUARIOS.INSERTAR('Insertar--->',null,false);
PRUEBAS_USUARIOS.ACTUALIZAR('Actualizar--->','123456789A',true);
PRUEBAS_USUARIOS.ELIMINAR('Eliminar--->','12345678A',true);


--CLIENTES
DBMS_OUTPUT.put_line('PRUEBAS DE CLIENTES');
PRUEBAS_CLIENTES.INICIALIZAR('Inicializar: ',true);
PRUEBAS_CLIENTES.INSERTAR('Insertar: ','12345678G','nicknameYEAH',true);
PRUEBAS_CLIENTES.INSERTAR('Insertar null: ','87654321D',null,false);
PRUEBAS_CLIENTES.ACTUALIZAR('actualizar','12345678G','nicknameNuevo',true);
PRUEBAS_CLIENTES.ACTUALIZAR('actualizar','12345678G',null,false);
PRUEBAS_CLIENTES.ELIMINAR('Eliminar','12345678G',true);
PRUEBAS_CLIENTES.ELIMINAR('Eliminar','87654321D',true);

--Trabajadores
DBMS_OUTPUT.put_line('PRUEBAS DE TRABAJADORES');
PRUEBAS_TRABAJADORES.INICIALIZAR('Inicializar',true);
PRUEBAS_TRABAJADORES.INSERTAR('Insertar ','12345678A','3216549685',true);
PRUEBAS_TRABAJADORES.INSERTAR('Insertar null',null,'3216549685',false);
PRUEBAS_TRABAJADORES.ACTUALIZAR('Actualizar ','12345678A','98168138',false);
PRUEBAS_TRABAJADORES.ELIMINAR('Eliminar ','12345678A',true);

END;
