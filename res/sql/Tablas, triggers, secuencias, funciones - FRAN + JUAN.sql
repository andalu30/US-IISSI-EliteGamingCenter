--------------------------------Drop tables---------------------------
----------------------------------------------------------------------
drop table usuarios cascade constraints;
drop table clientes cascade constraints;
drop table trabajadores cascade constraints;
drop table pedidos cascade constraints;
drop table lineasPedidos cascade constraints;
drop table proveedores cascade constraints;
drop table productos cascade constraints;
drop table facturas cascade constraints;
drop table lineasFacturas cascade constraints;
drop table reservas cascade constraints;
drop table juegosPcs cascade constraints;
drop table juegos cascade constraints;
drop table amonestaciones cascade constraints;
drop table reparaciones cascade constraints;
drop table pcs cascade constraints;

-----------------------------Creacion de tablas-----------------------
----------------------------------------------------------------------
--Usuarios
CREATE TABLE USUARIOS(
OID_USUARIO CHAR(20) PRIMARY KEY NOT NULL,
dni CHAR(9) NOT NULL,
email VARCHAR2(50) UNIQUE NOT NULL,
direccion VARCHAR2(100) NOT NULL,
telefono CHAR(9) NOT NULL,
nombre VARCHAR2(50)NOT NULL,
apellidos VARCHAR2(100)NOT NULL,
contraseña VARCHAR2(50)NOT NULL,
fechaNacimiento DATE,
constraint DNI CHECK (REGEXP_LIKE(DNI, '^(\d{8}([A-Z]|[a-z]))$'))

--comento los constraints porque he caido que aqui tenemos que guardar la
--contraseña encriptada, no en claro.
--constraint contraseña CHECK (REGEXP_LIKE(contraseña,'[[:alnum:]]')),
--constraint contraseñaSoloNumeros CHECK (REGEXP_LIKE(contraseña,'[[:alpha:]]')),
--constraint contraseñaSoloLetras CHECK (REGEXP_LIKE(contraseña,'[[:digit:]]'))
);
----------------------------------------------------------------------
--PCs
CREATE TABLE PCS (
OID_PC char (20) PRIMARY KEY NOT NULL,
estado VARCHAR2(15) NOT NULL,
tipo_pc VARCHAR2(6) NOT NULL,
precio NUMBER (10,2)
);
----------------------------------------------------------------------
--Juegos
CREATE TABLE JUEGOS(
OID_J char(20) PRIMARY KEY NOT NULL,
nombre VARCHAR2(50) NOT NULL UNIQUE
);
----------------------------------------------------------------------
--Proveedores
CREATE TABLE PROVEEDORES (
OID_PROV char(20) PRIMARY KEY NOT NULL,
nombre VARCHAR2(50) NOT NULL,
direccion VARCHAR2(100)NOT NULL,
telefono char(9) NOT NULL,
email VARCHAR2(50) UNIQUE NOT NULL,
fInicioContrato DATE UNIQUE,--PONGO FECHA DE CONTRATO UNIQUE PARA QUE NO SE REPITAN FECHA FIN Y FECHA INICIO EN OTRO POSIBLE CONTRATO DE PROVEEDOR
fFinContrato DATE
);
----------------------------------------------------------------------
--Productos
CREATE TABLE PRODUCTOS (
OID_PROD CHAR (20) PRIMARY KEY NOT NULL,
nombre VARCHAR2(50) NOT NULL,
precio NUMBER(10,2) NOT NULL,
stock NUMBER(10,2) NOT NULL ,
constraint stockNegativo CHECK(stock>0 or stock=0),--regla de negocio que no estaba implementada
iva NUMBER(10,2)
);

----------------------------------------------------------------------
--Trabajadores
CREATE TABLE TRABAJADORES (

OID_TRABAJADOR CHAR(20) PRIMARY KEY NOT NULL,
OID_USUARIO CHAR(20) NOT NULL,
numeroSSSS char(35) UNIQUE NOT NULL,
numeroCuenta char(35) NOT NULL,
fInicioContrato date NOT NULL,
fFinContrato date NOT NULL,
salario number(10,2)NOT NULL,
esAdministrador CHAR(20) ,
FOREIGN KEY (OID_USUARIO) REFERENCES USUARIOS(OID_USUARIO) on delete cascade
);

----------------------------------------------------------------------
--Clientes
CREATE TABLE CLIENTES (

OID_CLIENTE CHAR(20) PRIMARY KEY NOT NULL,
OID_USUARIO CHAR(20) NOT NULL,
nickname varchar2(50) UNIQUE NOT NULL,
saldo number(10,2)NOT NULL,
FOREIGN KEY (OID_USUARIO) REFERENCES USUARIOS(OID_USUARIO) on delete cascade
);

----------------------------------------------------------------------
--Amonestaciones
CREATE TABLE AMONESTACIONES (
OID_A CHAR(20)PRIMARY KEY NOT NULL,
OID_CLIENTE CHAR(20) NOT NULL,
OID_TRABAJADOR CHAR(20) /*NOT NULL*/,
fechaInicio DATE DEFAULT SYSDATE,
duracion number(10)NOT NULL,--la duracion esta en semanas
--FOREIGN KEY (OID_TRABAJADOR) REFERENCES TRABAJADORES(OID_TRABAJADOR) on delete cascade,
FOREIGN KEY (OID_CLIENTE) REFERENCES CLIENTES(OID_CLIENTE)
/*UNIQUE(OID_TRABAJADOR, OID_CLIENTE, fechaInicio)*/
);

----------------------------------------------------------------------
--Reparaciones
CREATE TABLE REPARACIONES (
OID_R CHAR(20)PRIMARY KEY NOT NULL,
OID_PC char (20)  NOT NULL,
OID_TRABAJADOR CHAR(20) NOT NULL,
fecha DATE DEFAULT SYSDATE,
UNIQUE(OID_PC,OID_TRABAJADOR,fecha),
FOREIGN KEY (OID_TRABAJADOR) REFERENCES TRABAJADORES(OID_TRABAJADOR) on delete cascade,
FOREIGN KEY (OID_PC) REFERENCES PCS(OID_PC)
);

----------------------------------------------------------------------
--JuegosPC
CREATE TABLE JUEGOSPCS(

OID_JP CHAR(20)PRIMARY KEY NOT NULL,
OID_J char (20) NOT NULL,
OID_PC char (20) NOT NULL,
UNIQUE(OID_J,OID_PC),
FOREIGN KEY (OID_J) REFERENCES JUEGOS(OID_J),
FOREIGN KEY (OID_PC) REFERENCES PCS(OID_PC)
);

----------------------------------------------------------------------
--Reservas
CREATE TABLE RESERVAS(

OID_RE CHAR(20)PRIMARY KEY NOT NULL,
OID_CLIENTE CHAR(20) NOT NULL,
OID_PC char (20) NOT NULL,
horaInicio TIMESTAMP NOT NULL,
diaInicio DATE NOT NULL,
numeroHoras NUMBER(10,2) NOT NULL,
precio NUMBER(10,2),
UNIQUE(OID_CLIENTE,OID_PC, horaInicio, diaInicio),
FOREIGN KEY (OID_CLIENTE) REFERENCES CLIENTES(OID_CLIENTE),
FOREIGN KEY (OID_PC) REFERENCES PCS(OID_PC)
);

----------------------------------------------------------------------
--Pedidos
CREATE TABLE PEDIDOS(

OID_PED CHAR(20) PRIMARY KEY NOT NULL,
ORDEN_P CHAR (20) NOT NULL,
OID_PROV CHAR(20)NOT NULL,
fechaPedido DATE,
OID_TRABAJADOR CHAR(20)/* NOT NULL*/,
precioTotal number(10,2),
UNIQUE(OID_PROV,ORDEN_P),
FOREIGN KEY (OID_PROV) REFERENCES PROVEEDORES(OID_PROV)
/*FOREIGN KEY (OID_TRABAJADOR) REFERENCES TRABAJADORES(OID_TRABAJADOR)*/
);

----------------------------------------------------------------------
--LineaPedidos

CREATE TABLE LINEASPEDIDOS(
OID_LP char(20)PRIMARY KEY NOT NULL,
ORDEN_LP char(20) /*NOT NULL*/,
OID_PROD CHAR (20) NOT NULL,
OID_PED Char (20) NOT NULL,--faltaba oid pedido
cantidad NUMBER(10,2) NOT NULL,
precio_producto NUMBER(10,2) NOT NULL,
/*UNIQUE(OID_PED,ORDEN_LP)*/
FOREIGN KEY (OID_PED) REFERENCES PEDIDOS(OID_PED)on delete cascade,--faltaba el cascade
FOREIGN KEY (OID_PROD) REFERENCES PRODUCTOS(OID_PROD),--FALTABA CLAVE FORANEA
UNIQUE(OID_PROD,OID_PED)--PARA QUE NO SE REPITAN PRODUCTOS EN UN MISMO PEDIDO, HAY QUE PONERLO ASI
--PORQUE SI SE AÑADE ARRIBA SE PODRñ?A REPETIR EL PRODUCTO EN OTRA LINEA DEL MISMO PEDIDO
);

----------------------------------------------------------------------
--Facturas
CREATE TABLE FACTURAS(
OID_F VARCHAR2(20) PRIMARY KEY NOT NULL,
fecha DATE DEFAULT SYSDATE NOT NULL,
precioTotal NUMBER(10,2),
OID_CLIENTE CHAR(20) NOT NULL,
OID_TRABAJADOR CHAR(20), /* NOT NULL,*/
/*FOREIGN KEY (OID_TRABAJADOR) REFERENCES TRABAJADORES(OID_TRABAJADOR),*/
FOREIGN KEY (OID_CLIENTE) REFERENCES CLIENTES(OID_CLIENTE)
);

----------------------------------------------------------------------
--LineasFacturas
CREATE TABLE LINEASFACTURAS(
OID_LF char(20)PRIMARY KEY NOT NULL,
OID_F VARCHAR2(20) NOT NULL,
ORDEN_LF CHAR(20) /*NOT NULL*/,
cantidad NUMBER(10) NOT NULL,
OID_PROD CHAR (20)  NOT NULL,
precio_producto NUMBER(10,2),
/*UNIQUE(OID_F,ORDEN_LF),*/
FOREIGN KEY (OID_F) REFERENCES FACTURAS(OID_F) on delete cascade,
FOREIGN KEY (OID_PROD) REFERENCES PRODUCTOS(OID_PROD),
UNIQUE(OID_PROD,OID_F)
);

---------------------------Creacion de secuencias---------------------
----------------------------------------------------------------------
DROP SEQUENCE clave_usuarios;
DROP SEQUENCE clave_clientes;
DROP SEQUENCE clave_trabajadores;


DROP SEQUENCE clave_proveedores;
DROP SEQUENCE clave_amonestaciones ;
DROP SEQUENCE clave_pcs;
DROP SEQUENCE clave_juegos;
DROP SEQUENCE clave_productos;
DROP SEQUENCE clave_reparaciones;
DROP SEQUENCE clave_juegosPcs;
DROP SEQUENCE clave_reservas;
DROP SEQUENCE clave_pedidos;
DROP SEQUENCE clave_lineasPedidos;
DROP SEQUENCE clave_facturas;
DROP SEQUENCE clave_lineasFacturas;

CREATE SEQUENCE clave_usuarios INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_clientes INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_trabajadores INCREMENT BY 1 START WITH 1;


CREATE SEQUENCE clave_proveedores INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_amonestaciones INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_pcs INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_juegos INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_productos INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_reparaciones INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_juegosPcs INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_reservas INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_pedidos INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_lineasPedidos INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_facturas INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE clave_lineasFacturas INCREMENT BY 1 START WITH 1;


----------------Creacion de triggers asociados a secuencias-----------
----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER crea_oid_usuarios
BEFORE INSERT ON USUARIOS
FOR EACH ROW
BEGIN
 :NEW.OID_USUARIO:=clave_usuarios.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_clientes
BEFORE INSERT ON CLIENTES
FOR EACH ROW
BEGIN
 :NEW.OID_CLIENTE:=clave_clientes.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_trabajadores
BEFORE INSERT ON TRABAJADORES
FOR EACH ROW
BEGIN
 :NEW.OID_TRABAJADOR:=clave_trabajadores.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_proveedores
BEFORE INSERT ON proveedores
FOR EACH ROW
BEGIN
 :NEW.OID_PROV:=clave_proveedores.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_amonestaciones
BEFORE INSERT ON amonestaciones
FOR EACH ROW
BEGIN
 :NEW.OID_A:=clave_amonestaciones.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_pcs
BEFORE INSERT ON pcs
FOR EACH ROW
BEGIN
  :NEW.OID_PC:=clave_pcs.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_juegos
BEFORE INSERT ON juegos
FOR EACH ROW
BEGIN
  :NEW.OID_J:=clave_juegos.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_productos
BEFORE INSERT ON productos
FOR EACH ROW
BEGIN
  :NEW.OID_PROD:=clave_productos.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_reparaciones
BEFORE INSERT ON reparaciones
FOR EACH ROW
BEGIN
  :NEW.OID_R:=clave_reparaciones.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_juegosPcs
BEFORE INSERT ON juegosPcs
FOR EACH ROW
BEGIN
  :NEW.OID_JP:=clave_juegosPcs.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_reservas
BEFORE INSERT ON reservas
FOR EACH ROW
BEGIN
  :NEW.OID_RE:=clave_reservas.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_pedidos
BEFORE INSERT ON pedidos
FOR EACH ROW
BEGIN
  :NEW.OID_PED:=clave_pedidos.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_lineasPedidos
BEFORE INSERT ON lineasPedidos
FOR EACH ROW
BEGIN
  :NEW.OID_LP:=clave_lineasPedidos.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_facturas
BEFORE INSERT ON facturas
FOR EACH ROW
BEGIN
  :NEW.OID_F:=clave_facturas.NEXTVAL;
  END;
/

CREATE OR REPLACE TRIGGER crea_oid_lineasFacturas
BEFORE INSERT ON lineasFacturas
FOR EACH ROW
BEGIN
  :NEW.OID_LF:=clave_lineasFacturas.NEXTVAL;
  END;
/


---------------Creacion de triggers no asociados a secuencias---------
----------------------------------------------------------------------

-- REGLA DE NEGOCIO RN-06: CONTRASEñAS SEGURAS.--
CREATE OR REPLACE TRIGGER contraseñas_8Caracteres
BEFORE INSERT OR UPDATE ON usuarios
FOR EACH ROW
DECLARE
cadena integer;
BEGIN
cadena:=LENGTH(:NEW.contraseña);
IF ( cadena<8)
THEN
RAISE_APPLICATION_ERROR(-20000,'contraseña con menos de 8 caracteres');
END IF;
END;
/

--REGLA DE NEGOCIO RN-01: Actualización de Stock
--PARA PEDIDOS
create or replace TRIGGER actualizacionStockPedidos
AFTER INSERT OR DELETE OR UPDATE ON LINEASPEDIDOS
FOR EACH ROW

BEGIN
IF (inserting or updating)
then
update productos set stock= stock + :new.cantidad where oid_prod=:new.oid_prod;
end if;
IF (deleting or updating)
then
update productos set stock= stock - :old.cantidad where oid_prod=:old.oid_prod;
END IF;

END;
/

--PARA FACTURAS
create or replace TRIGGER actualizacionStockFacturas
AFTER INSERT OR DELETE OR UPDATE ON LINEASFACTURAS
FOR EACH ROW

BEGIN
IF (inserting or updating)
then
update productos set stock= stock - :new.cantidad where oid_prod=:new.oid_prod;
end if;

IF (deleting or updating)
then
update productos set stock= stock + :old.cantidad where oid_prod=:old.oid_prod;
END IF;
END;
/

--RN 07:PCS diferentes a la vez
create or replace TRIGGER colision_de_reservas
BEFORE DELETE OR INSERT ON RESERVAS
FOR EACH ROW
DECLARE
contador integer;
contador1 integer;
contador2 integer;
contador3 integer;
BEGIN
IF(inserting)
THEN
select count(*) into contador from reservas where oid_pc=:new.oid_pc and diainicio=:new.diainicio;
select count(*) into contador1 from reservas where oid_pc=:new.oid_pc and diainicio<:new.diainicio and :new.diainicio<diainicio+numerohoras/24;
select count(*) into contador2 from reservas where OID_CLIENTE=:new.OID_CLIENTE and diainicio=:new.diainicio ;
select count(*) into contador3 from reservas where OID_CLIENTE=:new.OID_CLIENTE and diainicio<:new.diainicio and :new.diainicio<diainicio+numerohoras/24;

IF ( contador=1 or contador1=1 )
THEN
RAISE_APPLICATION_ERROR(-20004,'El pc ya esta reservado a esa hora');
END IF;
IF(contador2=1 or contador3=1)
THEN
RAISE_APPLICATION_ERROR(-20005,'El cliente: '||:new.OID_CLIENTE||', ya tiene una reserva en el horario seleccionado');
END IF;
END IF;
END;
/

--RN 08 : único proveedor
--se puede crear en caso de que nos falle un proveedor , un contrato con un proveedor diferente cuya fecha de inicio
--este entre la fechainicio del otro y fecha fin del otro, y la fecha fin sea la misma del otro.(cuando hablo de otro es alguno ya existente en la tabla)
create or replace TRIGGER unicoProveedor
BEFORE INSERT OR UPDATE ON  proveedores
FOR EACH ROW
DECLARE
existeFila integer;
BEGIN
select count(*) into existeFila from proveedores where :new.finiciocontrato>finiciocontrato and :new.finiciocontrato<ffincontrato and ffincontrato<>:new.ffincontrato;
IF (inserting)
THEN
IF(existeFila=1 or existeFila>1)
THEN
RAISE_APPLICATION_ERROR(-20002,'Ya existe un proveedor con contrato existente, por tanto no puede registrarse ningun otro');
END IF;
END IF;
END;
/

--Restriccion de reservas si se tiene una amonestación activa.

create or replace trigger ImpedirReservaPorInfraccion
BEFORE INSERT ON RESERVAS
FOR EACH ROW
DECLARE
numeroAmonestaciones integer;
fechaAmonestacion DATE;
duracionAmonestacion NUMBER(2,0);

BEGIN
SELECT count(*) into numeroAmonestaciones FROM AMONESTACIONES Where OID_CLIENTE=:new.OID_CLIENTE;
IF(numeroAmonestaciones>0)
THEN
SELECT FECHAINICIO into fechaAmonestacion FROM AMONESTACIONES Where OID_CLIENTE=:new.OID_CLIENTE;
SELECT DURACION into duracionAmonestacion FROM AMONESTACIONES Where OID_CLIENTE=:new.OID_CLIENTE;

IF( SYSDATE BETWEEN fechaAmonestacion and fechaAmonestacion+duracionAmonestacion*7)
THEN --La duracion esta en semanas, CUIDAAAAAAOOO
  RAISE_APPLICATION_ERROR(-20000,'El usuario no puede reservar porque tiene una amonestaciñn.');
END IF;
END IF;

END;
/

--Solucion al problema de las tablas mutantes----------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PACKAGE penalizacion_reserva as
NewReserva reservas%rowtype;
contador integer;
end;
/

create or replace trigger penalizacion_reserva_before
BEFORE DELETE OR INSERT ON reservas
FOR EACH ROW
DECLARE

BEGIN
penalizacion_reserva.NewReserva.precio:=:old.precio;
penalizacion_reserva.NewReserva.oid_cliente:=:old.oid_cliente;
penalizacion_reserva.NewReserva.diainicio:=:old.diainicio;
END;
/

--RN 11: PENALIZACION POR BORRAR RESERVA

create or replace TRIGGER penalizacion_reserva_after
after delete on reservas
for each row


begin
IF(penalizacion_reserva.contador>0)
THEN
UPDATE CLIENTES SET SALDO=saldo+(penalizacion_reserva.NewReserva.precio/2) where oid_cliente=penalizacion_reserva.NewReserva.oid_cliente;
END IF;

IF(penalizacion_reserva.contador=0)THEN
UPDATE CLIENTES SET SALDO=saldo+(penalizacion_reserva.NewReserva.precio) where oid_cliente=penalizacion_reserva.NewReserva.oid_cliente;
END IF;
end;
/

------------------------------------------------------------------------


--Precio RESERVA automático.
create or replace TRIGGER calculopreciototalreservas
BEFORE INSERT ON RESERVAS
FOR EACH ROW
DECLARE
columnaPcs   pcs%ROWTYPE;
contador integer;
BEGIN
select count(*) into contador from pcs where oid_pc=:new.oid_pc;
if(contador>0)
then
select *into columnaPcs from Pcs where oid_pc=:new.oid_pc;
:new.precio:=:new.numeroHoras*columnaPcs.precio;
end if;
END;
/


--Precio PRODUCTO automático.
create or replace TRIGGER calculoprecioproducto
BEFORE INSERT ON lineasfacturas
FOR EACH ROW
DECLARE
columnaProductos   PRODUCTOS%ROWTYPE;

BEGIN
select *into columnaProductos from productos where oid_prod=:new.oid_prod;
:new.precio_producto:=columnaProductos.precio;

END;
/

---------------------------Creacion de funciones----------------------
----------------------------------------------------------------------
--CALCULA PRECIO RESERVA
CREATE OR REPLACE FUNCTION calculoReserva(
  numeroHoras reservas.numeroHoras%TYPE,
  precioPc Pcs.precio%TYPE )
  RETURN  NUMBER IS w_precioReserva  reservas.precio%TYPE;
BEGIN
w_precioReserva:=numeroHoras*precioPc;
RETURN (w_precioReserva);
END;
/

----------------------------------------------------------------------
--Procedure para crear Clientes automaticamente
create or replace procedure crearCliente(

    dni             IN USUARIOS.DNI%TYPE,
    email           IN USUARIOS.EMAIL%TYPE,
    direccion       IN USUARIOS.DIRECCION%TYPE,
    telefono        IN USUARIOS.TELEFONO%TYPE,
    nombre          IN USUARIOS.NOMBRE%TYPE,
    apellidos       IN USUARIOS.APELLIDOS%TYPE,
    contraseña      IN USUARIOS.CONTRASEñA%TYPE,
    fechanacimiento IN USUARIOS.FECHANACIMIENTO%TYPE,/*<-Usuarios || Cliente ->*/
    nickname        IN CLIENTES.NICKNAME%TYPE,
    saldo           IN CLIENTES.SALDO%TYPE
    ) IS

BEGIN
  INSERT INTO USUARIOS VALUES (clave_usuarios.NEXTVAL,dni,email,direccion,telefono,nombre,apellidos,contraseña,fechanacimiento);
  INSERT INTO CLIENTES VALUES (clave_clientes.NEXTVAL,clave_usuarios.CURRVAL,nickname,saldo);
  COMMIT WORK;
end CrearCliente;
/

----------------------------------------------------------------------
--Procedure para crear trabajadores automaticamente.
create or replace procedure crearTrabajador(

    DNI             IN USUARIOS.DNI%TYPE,
    email           IN USUARIOS.EMAIL%TYPE,
    direccion       IN USUARIOS.DIRECCION%TYPE,
    telefono        IN USUARIOS.TELEFONO%TYPE,
    nombre          IN USUARIOS.NOMBRE%TYPE,
    apellidos       IN USUARIOS.APELLIDOS%TYPE,
    contraseña      IN USUARIOS.CONTRASEñA%TYPE,
    fechanacimiento IN USUARIOS.FECHANACIMIENTO%TYPE,/*<-Usuarios || trabajador ->*/
    numeroSSSS      IN TRABAJADORES.NUMEROSSSS%TYPE,
    numeroCuenta    IN TRABAJADORES.NUMEROCUENTA%TYPE,
    fechainiciocontrato IN TRABAJADORES.FINICIOCONTRATO%TYPE,
    fechafincontrato IN TRABAJADORES.FFINCONTRATO%TYPE,
    salario         IN TRABAJADORES.SALARIO%TYPE,
    esAdministrador IN TRABAJADORES.ESADMINISTRADOR%TYPE) IS

BEGIN
  INSERT INTO USUARIOS VALUES (clave_usuarios.NEXTVAL,DNI,email,direccion,telefono,nombre,apellidos,contraseña,fechanacimiento);
  INSERT INTO TRABAJADORES VALUES (clave_trabajadores.NEXTVAL,clave_usuarios.CURRVAL,numeroSSSS, numeroCuenta,fechainiciocontrato,fechafincontrato,salario,esAdministrador);
  COMMIT WORK;
End crearTrabajador;
/


----------------------------------------------------------------------
--Procedure para crear RESERVAS
create or replace procedure crearReservas(
      oid_re reservas.oid_re%type,
      OID_CLIENTE reservas.OID_CLIENTE%type,
      oid_pc reservas.oid_pc%type,
      horainicio reservas.horainicio%type,
      diainicio reservas.diainicio%type,
      numerohoras reservas.numerohoras%type) IS

BEGIN

  INSERT INTO RESERVAS VALUES (oid_re,OID_CLIENTE,oid_pc,horainicio,diainicio,numerohoras,0);
  COMMIT WORK;
End crearReservas;
/

-------------------------------------------------------------------
--CAMPO AUTOCALCULADO PRECIO TOTAL FACTURA
--EL TRIGGER DE CALCULAR PRECIOTOTAL DE PEDIDOS ES MEJOR HACERLO A MANOL YA QUE EL PRECIO QUE METEMOS AHñ ES EL PRECIO AL QUE COMPRAMOS
--LOS PRODUCTOS Y NO SE PUEDEN CONSULTAR DE LA TABLA PRODUCTOS

create or replace TRIGGER calculopreciototalfacturas
AFTER INSERT OR DELETE OR UPDATE ON LINEASFACTURAS
FOR EACH ROW
DECLARE
columnaProductos  PRODUCTOS%ROWTYPE;
BEGIN
IF (inserting or updating)
then
select *into columnaProductos from productos where oid_prod=:new.oid_prod;
update facturas set preciototal=preciototal+ (:new.cantidad*columnaProductos.precio) where oid_f=:new.oid_f;
END IF;
IF (updating )
then
select *into columnaProductos from productos where oid_prod=:new.oid_prod;
update facturas set preciototal=preciototal-(:old.cantidad*columnaProductos.precio) where oid_f=:old.oid_f;
END IF;
IF(deleting)
then
select *into columnaProductos from productos where oid_prod=:old.oid_prod;
update facturas set preciototal=preciototal-(:old.cantidad*columnaProductos.precio) where oid_f=:old.oid_f;
END IF;
END;
/

-----------------------------------------
--Procedure para crear Facturas
create or replace procedure crearFactura(
      fecha facturas.fecha%type,
      preciototal facturas.preciototal%type,
      OID_CLIENTE facturas.OID_CLIENTE%type,
      OID_TRABAJADOR facturas.OID_TRABAJADOR%type) IS

BEGIN
  INSERT INTO FACTURAS VALUES (clave_facturas.NEXTVAL,fecha,0,OID_CLIENTE,OID_TRABAJADOR);
  COMMIT WORK;
End crearFactura;
/

-----------------------------------------
--PROCEDURE PARA CREAR LINEAS DE FACTURAS
create or replace procedure crearLineasFacturas(
      oid_f lineasfacturas.oid_f%type,
      orden_lf lineasfacturas.orden_lf%type,
      cantidad lineasfacturas.cantidad%type,
      oid_prod lineasfacturas.oid_prod%type,
      precio_producto lineasfacturas.precio_producto%type) IS

BEGIN

  INSERT INTO LINEASFACTURAS VALUES (clave_lineasFacturas.NEXTVAL,oid_f,orden_lf,cantidad,oid_prod,0);
  COMMIT WORK;
End crearLineasFacturas;
/
-------------------------------------------
create or replace procedure eliminarReservas(
      oid_hola reservas.oid_re%type) IS

BEGIN
  SELECT COUNT(*) INTO penalizacion_reserva.contador from reservas where oid_re=oid_hola and diainicio<(sysdate+12/24) ;
  DELETE RESERVAS where oid_re=oid_hola;
  COMMIT WORK;
End eliminarReservas;
/






















--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------------------------------PROCEDURES NUEVOS---------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


CREATE OR REPLACE PROCEDURE ModificarSaldo(
nicknameCliente CLIENTES.NICKNAME%TYPE,
cantidad NUMBER
)AS
BEGIN
UPDATE CLIENTES SET SALDO=SALDO+cantidad WHERE NICKNAME=nicknameCliente;

END ModificarSaldo;
/

CREATE OR REPLACE PROCEDURE InsertarJuego(
name JUEGOS.NOMBRE%TYPE
)AS
BEGIN
INSERT INTO JUEGOS VALUES(clave_juegos.NEXTVAL,name);
END InsertarJuego;
/

CREATE OR REPLACE PROCEDURE BorrarJuego(
name JUEGOS.NOMBRE%TYPE
)AS
BEGIN
DELETE FROM JUEGOS WHERE NOMBRE LIKE name;
END BorrarJuego;
/

CREATE OR REPLACE PROCEDURE InsertarPC(
estado PCS.ESTADO%TYPE,
tipo PCS.TIPO_PC%type,
precio PCS.precio%type
)AS
BEGIN
INSERT INTO PCS VALUES(clave_pcs.NEXTVAL,estado,tipo,precio);
END InsertarPC;
/

create or replace PROCEDURE MODIFICAPC(
oid PCS.OID_PC%TYPE,
estadonuevo PCS.ESTADO%TYPE
)
AS
BEGIN
UPDATE PCS SET ESTADO=estadonuevo WHERE OID_PC=oid;
END MODIFICAPC;
/


CREATE OR REPLACE PROCEDURE EliminaPC(
oid PCS.OID_PC%TYPE
)AS
BEGIN
DELETE FROM PCS WHERE OID_PC =oid;
END ELIMINAPC;
/

create or replace PROCEDURE AMONESTARUSUARIO(
cliente AMONESTACIONES.OID_CLIENTE%TYPE,
trabajador AMONESTACIONES.OID_TRABAJADOR%TYPE,
duracion AMONESTACIONES.duracion%type
)AS
BEGIN
INSERT INTO AMONESTACIONES VALUES (CLAVE_AMONESTACIONES.NEXTVAL,cliente,trabajador,SYSDATE,duracion);
END AMONESTARUSUARIO;
/

create or replace PROCEDURE BorrarAmonestaciones (
oidcli AMONESTACIONES.OID_CLIENTE%TYPE
)AS
BEGIN
DELETE FROM AMONESTACIONES WHERE OID_CLIENTE=oidcli;
END BorrarAmonestaciones;
/

CREATE OR REPLACE PROCEDURE AñadirProveedor(
nombre PROVEEDORES.NOMBRE%TYPE,
direccion PROVEEDORES.DIRECCION%TYPE,
telefono PROVEEDORES.TELEFONO%TYPE,
email PROVEEDORES.EMAIL%TYPE,
finicio PROVEEDORES.FINICIOCONTRATO%TYPE,
ffin PROVEEDORES.FFINCONTRATO%TYPE
)AS
BEGIN
INSERT INTO PROVEEDORES VALUES (CLAVE_PROVEEDORES.NEXTVAL,nombre,direccion, telefono,email,finicio,ffin);
END AñadirProveedor;
/

CREATE OR REPLACE PROCEDURE BorrarProveedor(
oid PROVEEDORES.OID_PROV%TYPE
)AS
BEGIN
DELETE FROM PROVEEDORES WHERE OID_PROV=oid;
END BorrarProveedor;
/

create or replace PROCEDURE AñadirProducto(
nombre PRODUCTOS.NOMBRE%Type,
precio PRODUCTOS.PRECIO%Type,
stock PRODUCTOS.STOCK%Type,
iva PRODUCTOS.IVA%type
)AS
BEGIN
INSERT INTO PRODUCTOS VALUES(CLAVE_PRODUCTOS.NEXTVAL,nombre,precio,stock,iva);
END AñadirProducto;
/

create or replace PROCEDURE BorrarProducto(
oid PRODUCTOS.OID_PROD%TYPE
)AS
BEGIN
DELETE FROM PRODUCTOS WHERE OID_PROD=oid;
END BorrarProducto;
/

create or replace PROCEDURE AñadirReparacion(
pc REPARACIONES.OID_PC%TYPE,
m USUARIOS.EMAIL%TYPE,
fecha REPARACIONES.FECHA%type
--FECHA POR DEFECTO = SYSDATE
)AS
BEGIN
INSERT INTO REPARACIONES VALUES (CLAVE_REPARACIONES.NEXTVAL,pc,(SELECT OID_TRABAJADOR FROM TRABAJADORES NATURAL JOIN USUARIOS WHERE EMAIL=m) ,fecha);
END AñadirReparacion;
/

create or replace PROCEDURE EliminarReparacion(
oid REPARACIONES.OID_R%TYPE
)AS
BEGIN
DELETE FROM REPARACIONES WHERE OID_R=oid;
END EliminarReparacion;
/


create or replace PROCEDURE EliminarCliente(
oid CLIENTES.NICKNAME%TYPE
)AS
BEGIN
DELETE FROM USUARIOS WHERE OID_USUARIO=(SELECT OID_USUARIO FROM CLIENTES WHERE NICKNAME=oid);
END EliminarCliente;
/

create or replace PROCEDURE EliminarTrabajador(
oid TRABAJADORES.OID_TRABAJADOR%TYPE
)AS
BEGIN
DELETE FROM USUARIOS WHERE OID_USUARIO=(SELECT OID_USUARIO FROM TRABAJADORES WHERE OID_TRABAJADOR=oid);
END EliminarTrabajador;
/


--Procedures de fran para factura y linea factura.
create or replace PACKAGE procedures as
oid_usuarioQueEstaEnCliente CLIENTES%ROWTYPE;
oid_usuarioCarrito USUARIOS.OID_USUARIO%TYPE;
oid_clienteCarrito CLIENTES.OID_CLIENTE%TYPE;
fecha FACTURAS.FECHA%TYPE;
clave_facturas FACTURAS.OID_F%TYPE;
precio_producto PRODUCTOS.PRECIO%TYPE;
contador INTEGER;
end procedures;
/

create or replace PROCEDURE crearFacturaCarrito(
correo USUARIOS.EMAIL%type
)AS
BEGIN
SELECT OID_USUARIO INTO procedures.oid_usuarioCarrito FROM USUARIOS WHERE EMAIL LIKE correo;
SELECT OID_CLIENTE INTO procedures.oid_clienteCarrito FROM CLIENTES WHERE OID_USUARIO=procedures.oid_usuarioCarrito;
INSERT INTO FACTURAS VALUES(CLAVE_FACTURAS.NEXTVAL,SYSDATE,0,procedures.oid_clienteCarrito,null);
procedures.clave_facturas:=CLAVE_FACTURAS.CURRVAL;


  COMMIT WORK;
END crearFacturaCarrito;
/


create or replace PROCEDURE carrito(
oid_producto LINEASFACTURAS.OID_PROD%type,
cant LINEASFACTURAS.CANTIDAD%type
)AS
BEGIN

INSERT INTO LINEASFACTURAS VALUES(CLAVE_LINEASFACTURAS.NEXTVAL,procedures.clave_facturas,null,cant,oid_producto,0);
SELECT PRECIO INTO procedures.precio_producto FROM PRODUCTOS WHERE OID_PROD=oid_producto;
UPDATE CLIENTES SET SALDO=SALDO-(cant*procedures.precio_producto) WHERE OID_CLIENTE=procedures.oid_clienteCarrito;
  COMMIT WORK;
END carrito;
/



create or replace PROCEDURE AñadirReserva1hora(
login USUARIOS.EMAIL%TYPE,
oidpc RESERVAS.OID_PC%TYPE,
horainicio RESERVAS.HORAINICIO%TYPE
)AS
BEGIN

INSERT INTO RESERVAS VALUES (null,(
                                  SELECT OID_CLIENTE FROM CLIENTES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=login)),
                                  oidpc,
                                  horainicio,
                                  horainicio,
                                  1,
                                  null);
UPDATE CLIENTES SET SALDO=SALDO-(SELECT PRECIO FROM PCS WHERE OID_PC=oidpc) WHERE OID_CLIENTE=(SELECT OID_CLIENTE FROM CLIENTES WHERE OID_USUARIO=(SELECT OID_USUARIO FROM USUARIOS WHERE EMAIL=login));
COMMIT WORK;
END AñadirReserva1hora;
/

create or replace PROCEDURE crearAmonestacion (
oidcli AMONESTACIONES.OID_CLIENTE%TYPE,
fecha AMONESTACIONES.FECHAINICIO%TYPE,
DURACION AMONESTACIONES.DURACION%TYPE
)AS
BEGIN
INSERT INTO AMONESTACIONES VALUES(CLAVE_AMONESTACIONES.nextval,oidcli,NULL,fecha,duracion);

END crearAmonestacion;
/
