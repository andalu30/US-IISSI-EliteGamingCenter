<?php
	session_start();
    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }


	// Si no existen datos del formulario en la sesión, se crea una entrada con valores por defecto
	if (!isset($_SESSION['formulario'])) {

		$formulario['nombre'] = "";
		$formulario['direccion'] = "";
		$formulario['telefono']="";
		$formulario['email'] = "";
		$formulario['finiciocontrato']="";
		$formulario['ffincontrato']="";

		$_SESSION['formulario'] = $formulario;
	}
	// Si ya existían valores, los cogemos para inicializar el formulario
	else
		$formulario = $_SESSION['formulario'];

	// Si hay errores de validación, hay que mostrarlos y marcar los campos (El estilo viene dado y ya se explicará)
	if (isset($_SESSION["errores"]))
		$errores = $_SESSION["errores"];



?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Añadir Proveedor - Elite Gaming Center</title>
		<meta name="description" content="">
		<meta name="author" content="andalu30">
        <link rel="stylesheet" href="css/formularios.css" type="text/css" />

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->

	</head>

	<body>
		<div>
			<header>
				<?php include_once('cabecera.php');?>
			</header>

			<div class="testbox">
  				<h1>Registro de Proveedores</h1>

			<form action="accion_alta_proveedor.php" method="get" accept-charset="utf-8">

			        <hr>
    				<h1 class="h1">Datos del Proveedor</h1>
     				<hr>
                    <h4>Los campos indicados con un * son obligatorios.</h4>
                <div>
                    <label for="nombre"></label>
                    <input type="text" value ="<?php echo $formulario['nombre'] ?>" name="nombre" id="nombre" required=""
                    placeholder="Nombre*" title="El nombre tiene que completarse" />
                     <?php if(isset ($errores['nombre'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['nombre']."</div>";}?>
                </div>
                <div>
                    <label for="direccion"></label>
                    <input type="text" value ="<?php echo $formulario['direccion'] ?>" name="direccion" id="direccion" required=""
                     placeholder="Dirección*" title="La direccion tiene que completarse"/>
                     <?php if(isset ($errores['direccion'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['direccion']."</div>";}?>

                </div>
                <div>
                    <label for="telefono"></label>
                    <input type="text" value="<?php echo $formulario['telefono'];?>" name="telefono" id="telefono" pattern="^[0-9]{9}"  required=""
                    placeholder="Teléfono*" title="El telefono tiene que tener 9 dígitos"/>
                    <?php if(isset ($errores['telefono'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['telefono']."</div>";}?>

                </div>
                <div>
                    <label for="email"></label>
                    <input type="email" value="<?php echo $formulario['email'];?>" name="email" id="email" placeholder="Email*" required="" />
                    <?php if(isset ($errores['email'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['email']."</div>";}?>
                </div>

                	<hr>
    				<h1 class="h1">Datos del Contrato</h1>
     				<hr>
                <div>
                    <label for="finiciocontrato">Fecha Inicio contrato:*</label>
                    <input type="date" name="finiciocontrato" value="<?php echo $formulario['finiciocontrato'];?>" id="finiciocontrato" required=""/>
                    <?php if(isset ($errores['finiciocontrato'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['finiciocontrato']."</div>";}?>
                </div>
                <div>
                    <label for="ffincontrato">Fecha fin contrato:*</label>
                    <input type="date" name="ffincontrato" value="<?php echo $formulario['ffincontrato'];?>" id="ffincontrato" required=""/>
                    <?php if(isset ($errores['ffincontrato'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['ffincontrato']."</div>";}?>
                </div>


			<p><input class="button" type="submit" value="Añadir Proveedor"/></p>
			</form>
			</div>
		</div>
		<?php include_once 'pie.php';?>

	</body>
</html>
