
<?php
    session_start();
   require_once 'gestionEntradas/gestionCarrito.php';
   require_once 'gestionBD.php';

    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
           header("Location: login_form.php");
        }

        require_once 'cabecera.php';

?>

<!-- HE PUESTO EL CODIGO HTML POR SI APARTE QUEREMOS PONER ALGUN MENSAJEEN PANTALLA -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Compra Carrito</title>
        <meta name="description" content="">
        <meta name="author" content="andalu30">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <link rel="stylesheet" href="css/testcss.css" type="text/css" />



    </head>


    <body>

   <?php
        $conexion=crearConexionBD();
			 $factura=facturaCarrito($conexion,$login);

			 cerrarConexionBD($conexion);
			$arrayArticulosEnCarrito=$_SESSION["carritoProductos"] ;
            $cantidades=$_SESSION["carritoCantidades"] ;

	 for ($i=0; $i < count($arrayArticulosEnCarrito); $i++) {

	 	    carrito($conexion,$arrayArticulosEnCarrito[$i],$cantidades[$i]);
		 //ESTO PARA VER LO QUE SE ESTÁ CAPTURANDO
			//	echo $arrayArticulosEnCarrito[$i]." ".$cantidades[$i]." ";
			 }


       cerrarConexionBD($conexion);
       //BORRAMOS SSESION UNA VEZ QUE SE HACE LA COMPRA
       //TADAVÍA HABRIA QUE MIRAR LAS EXCEPCIONES CON RESPECTO AL SALDO ETC

        unset($_SESSION["carritoProductos"]);
    	unset($_SESSION["carritoCantidades"]);
        header('Location: miCuentaCarrito.php');
   ?>




<?php require_once 'pie.php'; ?>
    </body>
</html>
