<?php
    session_start();

    if (isset($_GET["producto"])) {
        
        $OIDproducto = $_GET["producto"];
        
        
        ?>
            <script>
                window.alert("El producto que se tiene que retirar es el: <?php echo $OIDproducto ?>")
            </script>
        <?php
        
        $arrayArticulosEnCarrito = $_SESSION["carritoProductos"];
        $carritocantidades = $_SESSION["carritoCantidades"];


        if (in_array($OIDproducto, $arrayArticulosEnCarrito)) {  
     
            $cont = 0;
            foreach ($arrayArticulosEnCarrito as $prod) {
                $cont++;
                if ($prod == $OIDproducto) {
                    break;
                }
            }
            //En cont esta la posicion del producto que queremos borrar.
            
            ?>
                <script>
                    window.alert("Se encuentra en la posicion: <?php echo $cont ?>")
                </script>
            <?php
            
            
            unset($arrayArticulosEnCarrito[$cont-1]);
            $arrayArticulosEnCarrito = array_values($arrayArticulosEnCarrito); //Se usa para que las keys se autoordenen, sino habria un hueco
            
             if(!empty($arrayArticulosEnCarrito)){
            $_SESSION["carritoProductos"] = $arrayArticulosEnCarrito;
            
            
            unset($carritocantidades[$cont-1]);
            $carritocantidades = array_values($carritocantidades);
            
            $_SESSION["carritoCantidades"] = $carritocantidades;
            }else{
				    unset($_SESSION["carritoProductos"]);
    				unset($_SESSION["carritoCantidades"]);
    				header('Location: miCuentaCarrito.php');
			}
        }
    }
  header('Location: miCuentaCarrito.php');
    
?>


<a  href="miCuentaCarrito.php">Volver</a>

