<?php
	session_start();
    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }

	// Si no existen datos del formulario en la sesión, se crea una entrada con valores por defecto
	if (!isset($_SESSION['formulario'])) {
		$formulario['id'] = "";
		$formulario['fecha'] = "";
		$formulario['email'] = "";
		$_SESSION['formulario'] = $formulario;
	}
	// Si ya existían valores, los cogemos para inicializar el formulario
	else
		$formulario = $_SESSION['formulario'];

	// Si hay errores de validación, hay que mostrarlos y marcar los campos (El estilo viene dado y ya se explicará)
	if (isset($_SESSION["errores"]))
		$errores = $_SESSION["errores"];




?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Añadir Reparacion - Elite Gaming Center</title>
		<meta name="description" content="">
		<meta name="author" content="andalu30">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/formularios.css" type="text/css" />
	</head>

	<body>


				<?php //include_once('cabecera.php');
				include_once('cabecera.php');
				?>
<div>



			<div class="testbox">
	  		<h1>Registro de Reparaciones</h1>

				<form action="accion_alta_reparacion.php" method="get" accept-charset="utf-8"/>
				        <hr>
	    				<h1 class="h1">Datos de la reparacion</h1>
	     				<hr>
				 			<h4>Los campos indicados con un * son obligatorios.</h4>
					<div>
	                    <input type="email" name="email" id="email"
	                     placeholder="Email Trabajador*" value="<?php echo $formulario['email'];?>"required="" />
	                    <?php if(isset ($errores['email'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['email']."</div>";}?>
	                </div>

					<div>
	                    <input type="text" name="id" id="id" value="<?php echo $formulario['id'];?>" placeholder="ID equipo*" required=""
	                   pattern="^[0-9]+" title=" El precio solo puede contener dígitos"   />
	                    <?php if(isset ($errores['id'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['id']."</div>";}?>
	                </div>
					<div>
						<label for="fecha">Fecha de reparación</label>
	                    <input type="date" name="fecha" id="fecha" value="<?php echo $formulario['fecha'];?>"
	                     required="" />
	                    <?php if(isset ($errores['fecha'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['fecha']."</div>";}?>
	                </div>
			  <p><input class="button" type="submit" value="Añadir Reparacion"/></p>
				</form>
		    </div>


 </div>
		<?php include_once 'pie.php';?>

	</body>
</html>
