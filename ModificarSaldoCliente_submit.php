<?php

require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

session_start();

		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
					
		}	
		if(isset($_SESSION['nickname'])){
			unset($_SESSION['nickname']);
		}
if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
           // header('Location: logout.php');
        
    }else{
        $login = 'No se ha iniciado sesión';
    }
			
if (isset($_SESSION['formulario'])) { //Se mira si en request hay algun valor (Se ha entrado por el formulario)
    $ParamModificacion=$_SESSION['formulario'];    


	
	$conexion = crearConexionBD();
	
	$modificacion = ModificarSaldo($conexion,	$ParamModificacion["nickname"],
												$ParamModificacion["cantidad"]
												);
								
	cerrarConexionBD($conexion);
	
	
	
	}else{
    	Header("Location: clientesPaginado.php"); //Esto carga la web alta si no se llega a accion desde el formulario.
}	
	
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/formularios.css" type="text/css" />

  <meta charset="utf-8">
  <title>Modificacion de saldo - Confirmacion</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>    
	
	<?php if ($modificacion <> "") {
		?><h1>Error al meter los datos en la base de datos.</h1><?php echo $modificacion;
	}else{?>
	
<div class="boxClientesSaldo">
		<h1>Modificacion saldo satisfactoria: </h1>
		<hr>
    <div>
            <li>Nickname: <?php echo $ParamModificacion["nickname"]; ?>              </li>
            <li> Se ha añadido <?php echo $ParamModificacion["cantidad"].' €'; ?> al saldo    </li>
           <a class="button" href="clientesPaginado.php">Ir clientes</a>
    </div>
</div>
    
    
   <?php } ?>

	<?php include_once 'pie.php';?>


</body>
</html>
