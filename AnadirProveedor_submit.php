<?php

require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

session_start();

	if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
	// Recogemos los datos del formulario
		$nuevoProveedor= $_SESSION["formulario"];
		unset($_SESSION["formulario"]);

		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);

		}

	$conexion = crearConexionBD();
	$insercion = AñadirProveedor($conexion, $nuevoProveedor);
	cerrarConexionBD($conexion);

	}else{
    	Header("Location: AnadirProveedor_form.php"); //Esto carga la web alta si no se llega a accion desde el formulario.
}


	if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];

    }else{
        $login = 'No se ha iniciado sesión';
    }

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/formularios.css" type="text/css" />

  <meta charset="utf-8">
  <title>Añadir Proveedor - Submit</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>

	<?php if ($insercion <> "") {
		?><h1>Error al meter los datos en la base de datos. </h1><?php echo $insercion;?>
		<h4>Informe del Error:</h4>
	<?php }else{ ?>

	<div class="box">
		<h1>Datos: </h1>
		<hr>
     <div>
            <li>Nombre:             <?php echo  $nuevoProveedor["nombre"]?></li>
            <li>Direccion:             <?php echo  $nuevoProveedor["direccion"]?></li>
            <li>Telefono:             <?php echo  $nuevoProveedor["telefono"]?></li>
            <li>Email:             <?php echo  $nuevoProveedor["email"]?></li>
            <li>Fecha inicio contrato:             <?php echo  $nuevoProveedor["finiciocontrato"]?></li>
            <li>Fecha fin contrato:             <?php echo  $nuevoProveedor["ffincontrato"]?></li>
            <li>  <a class="button" href="AnadirProveedor_form.php">Agregar más proveedores</a> </li>
     </div>
   </div>
   <?php } ?>


	<?php include_once 'pie.php';?>


</body>
</html>
