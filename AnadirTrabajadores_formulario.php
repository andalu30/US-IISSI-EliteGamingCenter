<?php

	session_start();

    //Comprobamos si hay algun login abierto
        if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        }else{
            $login = 'No se ha iniciado sesión';
        }

	// Si no existen datos del formulario en la sesión, se crea una entrada con valores por defecto
	//COMPLETAR BIEN!!!!!!!
	if (!isset($_SESSION['formulario'])) {
		$formulario['dni'] = "";
		$formulario['nombre'] = "";
		$formulario['apellidos'] = "";
		$formulario['direccion'] = "";
		$formulario['telefono']="";
		$formulario['fechanac'] = "";
		$formulario['email'] = "";
		$formulario['pass'] = "";
	    $formulario['numSS'] = "";
		$formulario['cuenta'] = "";
		$formulario['inicioContrato'] = "";
		$formulario['finContrato'] = "";
		$formulario['esAdmin'] = "";
		$formulario['salario'] = "";



		$_SESSION['formulario'] = $formulario;
	}
	// Si ya existían valores, los cogemos para inicializar el formulario
	else{
		$formulario = $_SESSION['formulario'];
}

	// Si hay errores de validación, hay que mostrarlos y marcar los campos (El estilo viene dado y ya se explicará)
	if (isset($_SESSION["errores"]))
		$errores = $_SESSION["errores"];
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Añadir Trabajadores - Elite Gaming Center</title>
		<meta name="description" content="">
		<meta name="author" content="andalu30">
        <link rel="stylesheet" href="css/formularios.css" type="text/css" />


		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	</head>

	<body>
			<header>
				<?php include_once('cabecera.php');?>
			</header>

<?php
		/*Mostrar los erroes de validación (Si los hay)
		if (isset($errores) && count($errores)>0) {
	    	echo "<div id=\"div_errores\" class=\"error\">";
			echo "<h4> Errores en el formulario:</h4>";
    		foreach($errores as $error) echo $error;
    		echo "</div>";
  		}

		 */
?>


			<div class="testbox">
  				<h1>Registro de Trabajadores</h1>

			<form action="accion_alta_trabajador.php" method="get" accept-charset="utf-8">
			      <hr>
    					<h1 class="h1">Datos Personales</h1>
     				<hr>
			 <h4>Los campos indicados con un * son obligatorios.</h4>
			 	<div>
                    <label for="dni"></label>
                    <input type="text" value="<?php echo $formulario['dni'];?>" name="dni" id="dni" placeholder="DNI*" pattern="^[0-9]{8}[A-Z]"  required=""
                    title="El DNI tiene que tener 8 dígitos y una letra"/>
                     <?php if(isset ($errores['dni'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['dni']."</div>";}?>
                </div>
                <div>
                    <label for="email"></label>
                    <input type="email" value="<?php echo $formulario['email'];?>" name="email" id="email" placeholder="Email*" required="" />
                    <?php if(isset ($errores['email'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['email']."</div>";}?>
                </div>
                <div>
                    <label for="direccion"></label>
                    <input type="text" value="<?php echo $formulario['direccion'];?>" name="direccion"  id="direccion" required="" placeholder="Dirección*"/>
                    <?php if(isset ($errores['direccion'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['direccion']."</div>";}?>
                </div>
                <div>
                    <label for="telefono"></label>
                    <input type="text" value="<?php echo $formulario['telefono'];?>" name="telefono" id="telefono" pattern="^[0-9]{9}"
                     placeholder="Teléfono*" required="" title="El telefono tiene que tener 9 dígitos"/>
                    <?php if(isset ($errores['telefono'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['telefono']."</div>";}?>

                </div>
                <div>
                    <label for="nombre"></label>
                    <input type="text" value="<?php echo $formulario['nombre'];?>" name="nombre" id="nombre" placeholder="Nombre*" required="" />
 					<?php if(isset ($errores['nombre'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['nombre']."</div>";}?>

                </div>
                <div>
                    <label for="apellidos"></label>
                    <input type="text" value="<?php echo $formulario['apellidos'];?>" name="apellidos" id="apellidos" placeholder="Apellidos*" required="" />
                    <?php if(isset ($errores['apellidos'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['apellidos']."</div>";}?>

                </div>
                <div>
                    <label for="fechanac">Fecha de Nacimiento</label>
                    <input type="date" value="<?php echo $formulario['fechanac'];?>"  name="fechanac"  id="fechanac" placeholder="Fecha Nac""/>
                </div>


			 		<hr>
    				<h1 class="h1">Datos de Usuario:</h1>
     				<hr>
                <div>
                    <label for="pass"></label>
                    <input type="password" name="pass" id="pass"  pattern="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,25})$"
                    required="" placeholder="Pasword*"
                    title="La contraseña tiene que contener caracteres,dígitos y una longitud de al menos 8 caracteres"/>
                    <?php if(isset ($errores['contraseña'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['contraseña']."</div>";}?>

                </div>
                <div>
                    <label for="confirm"> </label>
                    <input type="password"  name = "confirmpass" id="confirm" placeholder="Confirm Password" required="" />
                    <?php if(isset ($errores['contraseña1'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['contraseña1']."</div>";}?>

                </div>
			 	<div>
                    <label for="numSS"></label>
                    <input type="text" value="<?php echo $formulario['numSS'];?>" name="numSS" id="numSS" pattern="^[0-9]{11}" required=""
                    placeholder="Número de la Seguridad Social*" title="El número de seguridad social tiene 11 dígitos" />
                     <?php if(isset ($errores['numSS'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['numSS']."</div>";}?>
                </div>
                <div>
                    <label for="cuenta"></label>
                    <input type="text" value="<?php echo $formulario['cuenta'];?>" name="cuenta" id="cuenta"  pattern="[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}"
                    placeholder="IBAN:ES6621000418401234567891*" title="El numero de cuanta tiene que ser con formato IBAN" required=""/>

                    <?php if(isset ($errores['cuenta'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['cuenta']."</div>";}?>
                </div>
                <div>
                    <label for="inicioContrato">Fecha Inicio de Contrato</label>
                    <input type="date" value="<?php echo $formulario['inicioContrato'];?>" name="inicioContrato" id="inicioContrato" required=""
                    placeholder="Fecha Inicio de Contrato*" title="Las fechas del contrato tienen que completarse"/>
                    <?php if(isset ($errores['contrato'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['contrato']."</div>";}?>
                </div>
                <div>
                    <label for="finContrato">Fecha Fin de Contrato</label>
                    <input type="date" value="<?php echo $formulario['finContrato'];?>" name="finContrato" id="finContrato" required=""
                    placeholder="Fecha Fin del Contrato*" />
                	 <?php if(isset ($errores['contratoTiempo'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['contratoTiempo']."</div>";}?>
                </div>
                <div>
                    <label for="salario"></label>
                    <input type="text" value="<?php echo $formulario['salario'];?>" name="salario" id="salario" required=""
                    placeholder="Salario en Euros*"/>
                     <?php if(isset ($errores['salario'])){ echo "<div id=\"div_errores1\" class=\"errores1\">"; echo $errores['salario']."</div>";}?>
                </div>

			  <div class="esAdmin">

			  	<h1 class="h1">Registrar como administrador</h1>
			  	<hr>

			  	<?php if(isset ($errores['esAdmin'])){ echo "<div id=\"div_errores1\" class=\"errores1\"> "; echo $errores['esAdmin']."</div>";}?>

				 <label for="NO" class="radio">NO</label>
				 <input type="radio" value="NO" id="NO" name="esAdmin" />

					<label for="SI" class="radio">SI</label>
			    <input type="radio" value="SI" id="SI" name="esAdmin" />
      </div>




			<p><input class="button" type="submit" value="Añadir Trabajador"/></p>
			</form>
		</div>


		<?php include_once 'pie.php';?>
		</div>
	</body>
</html>
