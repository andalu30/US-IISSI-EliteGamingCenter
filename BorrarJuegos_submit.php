<?php

require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

session_start();

if (isset($_REQUEST["nombre"])) { //Se mira si en request hay algun valor (Se ha entrado por el formulario)
    $nuevoJuego["nombre"] = $_REQUEST["nombre"];

	$conexion = crearConexionBD();
	$delete = BorrarJuego($conexion,$nuevoJuego["nombre"]);
	cerrarConexionBD($conexion);

	}else{
    	Header("Location: JuegosPaginados.php"); //Esto carga la web alta si no se llega a accion desde el formulario.
}

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="css/testcss.css" type="text/css" />

  <meta charset="utf-8">
  <title>Borrar Juego - Submit</title>
<link rel="stylesheet" href="css/testcss.css" type="text/css" />
  <?php include_once 'cabecera.php';?>
</head>

<body>
<script>
    window.alert("Se ha eliminado el juego");
    window.location.replace("JuegosPaginados.php");

</script>
</body>
<footer>
	<a href="BorrarJuego_form.php">Volver al formulario para agregar nuevos juegos</a>
	<?php include_once 'pie.php';?>
</footer>
</html>
