<?php
	session_start();

    if (isset($_SESSION["error"])) {
        $error = $_SESSION["error"];
    }

    if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
            header('Location: logout.php');

    }else{
        $login = 'No se ha iniciado sesión';
    }
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Login - Elite Gaming Center</title>
		<meta name="author" content="andalu30">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link rel="stylesheet" href="css/login.css" type="text/css" />

	</head>

	<body>
		<main>
		<div>
			<?php
    			include_once('cabecera.php');

    			if (isset($error)) {
						?>
                    <div class="error">"Error en la contraseña, no existe el usuario, se nos ha caido la base de datos
                    o a alguien se le ha pasado un punto y coma. Cualquiera de esas cosas... en verdad
                    esto es un coñazo de la hostia. Esperemos que el error sea porque te has equivocado
                    de email o contraseña (馬鹿!) porque sino... Anda, vuelve a rellenar el formulario.";
                    </div>

										<?php
                    unset($_SESSION["error"]);
                }
            ?>

			<form id="formularioCompleto" action="login_submit.php" method="post" accept-charset="utf-8">
			 <fieldset>
			 	<legend>Introduzca sus datos:</legend>

						<div id="contenedorImagenLogin">
								<img class="avatar" src="res/images/google_avatar.png"/>
						</div>

								<div>
                    <label for "email">Email:</label>
                    <input type="email" name="email" id="email" required="" placeholder="Introduzca su email"/>
                </div>
                <div>
                    <label for="password">Contraseña:</label>
                    <input type="password" name="password" id="password" required="" placeholder="Introduzca su contraseña"/>
                </div>
								<input type="submit" value="Iniciar Sesion"/>
					<p class="pregunta">¿No tienes cuenta? <a href="AnadirClientes_formulario.php">Haz click aquí para registrarte</a></p>
			 </fieldset>
			</form>
		</main>

		<?php include_once ('pie.php');?>
		</div>
	</body>
</html>
