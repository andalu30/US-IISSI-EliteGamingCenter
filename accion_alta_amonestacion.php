<?php


	
	session_start();
	
   require_once 'gestionEntradas/gestionFormularios.php';
   require_once 'gestionBD.php';
   
	
  
	 
	// Comprobar que hemos llegado a esta página porque se ha rellenado el formulario
	if (isset($_SESSION["formulario"])) {
		// Recogemos los datos del formulario
		$nuevaAmonestacion["email"] = $_REQUEST["email"];
		$nuevaAmonestacion["duracion"] = $_REQUEST["duracion"];
		$nuevaAmonestacion["fecha"] = $_REQUEST["fecha"];

	}
	else // En caso contrario, vamos al formulario
		Header("Location: AnadirAmonestaciones_form.php");

	// Guardar la variable local con los datos del formulario en la sesión.
	$_SESSION["formulario"] = $nuevaAmonestacion;

	// Validamos el formulario en servidor 
	$errores = validarDatosUsuario($nuevaAmonestacion);
	
	
	// Si se han detectado errores
	if (count($errores)>0) {
		// Guardo en la sesión los mensajes de error y volvemos al formulario
		$_SESSION["errores"] = $errores;
		Header('Location: AnadirAmonestaciones_form.php');
	} else
		// Si todo va bien, vamos a la página de éxito (inserción del usuario en la base de datos)
		Header('Location:AnadirAmonestaciones_submit.php');
		

	///////////////////////////////////////////////////////////
	// Validación en servidor del formulario de alta de usuario
	///////////////////////////////////////////////////////////
	function validarDatosUsuario($nuevaAmonestacion){
		
   //Guardamos si esta el email ya en la base de datos
	 $conexion=crearConexionBD();
	 $consultarEmail=consultarEmail($conexion,$nuevaAmonestacion["email"]);
	 cerrarConexionBD($conexion);
	 


		// Validación del email
		if($nuevaAmonestacion["email"]==""){ 
			$errores['email'] = "<p>El email no puede estar vacío</p>";
		}else if(!filter_var($nuevaAmonestacion["email"], FILTER_VALIDATE_EMAIL)){
			$errores['email'] = $error . "<p>El email es incorrecto: " . $nuevaAmonestacion["email"]. "</p>";
			
		}else if($consultarEmail==0){
			$errores['email'] = "<p>Ese email no está registrado como cliente</p>";
		}
		// Validación de la duracion		
		if($nuevaAmonestacion["duracion"]==""){
			$errores['duracion'] = "<p>La duracion no puede estar vacía</p>";
			}else if(!preg_match("/[0-9]+/", $nuevaAmonestacion["duracion"])){
			$errores['duracion'] = "<p>No puedes meter caracteres</p>";
			}else if(!isset($nuevaAmonestacion["duracion"]) || strlen($nuevaAmonestacion["duracion"])>2){
			$errores['duracion'] = "<p>La duracion de las amonestacones como máximo son 99 semanas</p>";
		}
			
		// Validación del fecha			
		if($nuevaAmonestacion["fecha"]==""){
			$errores['fecha'] = "<p>La fecha no puede estar vacía</p>";
		}
			
	
	
		return $errores;
	}

?>

