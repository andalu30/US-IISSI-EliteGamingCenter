	function confirmacion(){
		// Obtenemos el campo de password y su valor

		var password = getElementById("pass");
		var passwordValue = password.value;
		// Obtenemos el campo de confirmación de password y su valor
		var confirmPass = getElementById("confirmpass");
		var confirmPassValue = confirmPass.value;
	//	Los comparamos y devolvemos un mensaje de error en caso de que no sean iguales
		if(passwordValue != confirmPassValue){
			 var error= "Por favor introduzca el mismo password";
		 }else{
			 var error= "";
		}
		confirmPass.setCustomValidity(error);
		return error;

	}


	function passwordValidation(){
		var valid = true;
		// Accedemos al campo de contraseña y su valor
		var password = document.getElementById("pass");
		var passwordValue = password.value;
		// Comprobamos la longitud de la contraseña
		valid = valid && (passwordValue.length>=8);
		// Comprobamos si contiene letras mayúsculas, minúsculas y números
		var tieneMayusculas = /[A-Z]/;
		var tieneMinusculas = /[a-z]/;
		var tieneNumeros = /\d/;

		valid = valid && tieneMayusculas.test(passwordValue)
									&& tieneMinusculas.test(passwordValue)
									&& tieneNumeros.test(passwordValue);

		// Si no cumple las restricciones, devolvemos un mensaje de error
		if(!valid){
			
			var error = "Por favor ingrese una contrasña valida";
		}else{
			var error = "";
		}
		password.setCustomValidity(error); //Ventana del navegador para que marque el error.
		return error;

	}
	

