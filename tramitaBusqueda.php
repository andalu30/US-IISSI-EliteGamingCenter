<?php
    session_start();

    if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];

    }else{
        $login = 'No se ha iniciado sesión';
    }




    require_once("gestionBD.php");
    require_once("gestionEntradas/gestionTienda.php");
    require_once("gestionEntradas/paginacion_consulta.php");

    if (isset($_REQUEST["keyword"])) {
        $keyword = $_REQUEST["keyword"];
    }else{
        header("Location:tienda.php");
    }


    //Gestion de la paginación.

    // ¿Venimos simplemente de cambiar página o de haber seleccionado un registro ?
    // ¿Hay una sesión activa?
    if (isset($_SESSION["paginacion"])) $paginacion = $_SESSION["paginacion"];
    $pagina_seleccionada = isset($_GET["PAG_NUM"])? (int)$_GET["PAG_NUM"]:
                                                (isset($paginacion)? (int)$paginacion["PAG_NUM"]: 1);
    $pag_tam = isset($_GET["PAG_TAM"])? (int)$_GET["PAG_TAM"]:
                                        (isset($paginacion)? (int)$paginacion["PAG_TAM"]: 5);
    if ($pagina_seleccionada < 1) $pagina_seleccionada = 1;
    if ($pag_tam < 1) $pag_tam = 5;

    // Antes de seguir, borramos las variables de sección para no confundirnos más adelante
    unset($_SESSION["paginacion"]);

    $conexion = crearConexionBD();

    // La consulta que ha de paginarse
    $query = "SELECT * FROM PRODUCTOS WHERE UPPER(NOMBRE) LIKE UPPER('%".$keyword."%')";

    // Se comprueba que el tamaño de página, página seleccionada y total de registros son conformes.
    // En caso de que no, se asume el tamaño de página propuesto, pero desde la página 1
    $total_registros = total_consulta($conexion,$query);
    $total_paginas = (int) ($total_registros / $pag_tam);
    if ($total_registros % $pag_tam > 0) $total_paginas++;
    if ($pagina_seleccionada > $total_paginas) $pagina_seleccionada = $total_paginas;

    // Generamos los valores de sesión para página e intervalo para volver a ella después de una operación
    $paginacion["PAG_NUM"] = $pagina_seleccionada;
    $paginacion["PAG_TAM"] = $pag_tam;
    $_SESSION["paginacion"] = $paginacion;

    $filas = consulta_paginada($conexion,$query,$pagina_seleccionada,$pag_tam);



?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/tienda.css" type="text/css" />

  <title>Tienda - Elite Gaming Center</title>
</head>
    <body>
    <?php include_once 'cabecera.php';?>

    <!-- Busqueda de productos debajo -->
          <form action="TramitaBusqueda.php" class="cuadrodebusqueda">
             <input name="keyword" id="inputbusqueda" placeholder="Busqueda..."/>
          </form>
      </div>



    <h4><a href="tienda.php"> << Volver a ver todos los productos</a></h4>
    <h2>Consulta de productos paginada.</h2>

    <!-- Paginacion debajo -->
     <nav class="pagination">
        <div >

            <a href="TramitaBusqueda.php?PAG_NUM=<?php echo $pagina_seleccionada-1; ?>&PAG_TAM=<?php echo $pag_tam; ?>&keyword=<?php echo $keyword; ?>"><<</a>

            <?php
                for( $pagina = 1; $pagina <= $total_paginas; $pagina++ )
                    if ( $pagina == $pagina_seleccionada) {     ?>
                        <a><span class="current"><?php echo $pagina; ?></span></a>
            <?php } else { ?>
                        <a href="TramitaBusqueda.php?PAG_NUM=<?php echo $pagina; ?>&PAG_TAM=<?php echo $pag_tam; ?>&keyword=<?php echo $keyword;?>"><?php echo $pagina; ?></a>
            <?php } ?>

            <a href="TramitaBusqueda.php?PAG_NUM=<?php echo $pagina_seleccionada+1; ?>&PAG_TAM=<?php echo $pag_tam; ?>&keyword=<?php echo $keyword;?>">>></a>


        </div>

        <form class="formPaginas" method="get" action="TramitaBusqueda.php?PAG_NUM=<?php echo $pagina; ?>&PAG_TAM=<?php echo $pag_tam; ?>&keyword=<?php echo $keyword;?>">
            <input id="PAG_NUM" name="PAG_NUM" type="hidden" value="<?php echo $pagina_seleccionada?>"/>
            Mostrando
            <input id="PAG_TAM" name="PAG_TAM" type="number" min="1" max="<?php echo $total_registros;?>"value="<?php echo $pag_tam?>" />
            entradas de <?php echo $total_registros?>
            <input type="submit" value="Cambiar">
        </form>
    </nav>




    <!-- Tabla que muestra los productos -->
    <table id="productosTienda">

            <tr>
                <th>Imagen:</th><th>OID Producto:</th><th>Producto</th> <th>Precio (IVA incluido)</th> <th>Cantidad Disponible</th> <th>IVA</th><th>Botones</th>
            </tr>

            <?php foreach ($filas as $producto) { ?>
                <tr class="producto"></tr>

                    <?php
                        $img = "res/images/productos/". trim($producto["OID_PROD"]) .trim(".png");
                    ?>


                    <form method="post" action="anadirAlcarrito.php?producto=<?php echo $producto['OID_PROD']?>">

                        <td><img style="width: 25ex;" src="<?php echo $img?>" alt="No encontramos la imagen. <?php echo $img?>" /></td>
                        <td align="center"><?php echo $producto["OID_PROD"]?></td>
                        <td align="left"><?php echo $producto["NOMBRE"]?></td>
                        <td align="center"><b><?php echo $producto["PRECIO"]."€"?></b></td>
                        <td align="center"><?php echo $producto["STOCK"]?></td>
                        <td align="right"><?php echo $producto["IVA"]?>% (ya incluido)</td>


                        <td align="right">
                            <label for "Cantidad">Cantidad:</label>
                            <input style="width: 25%;" class="inputcantidad" type="number" name="cantidad" value="1" />
                            <button class="añadirCarrito" type="submit">Añadir al carrito</button>
                        </td>
                    </form>

            <?php } ?>
        </table>


<?php include_once 'pie.php'; ?>

</html>
