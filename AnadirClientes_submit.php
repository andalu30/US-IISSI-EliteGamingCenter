<?php
session_start();
require_once 'gestionBD.php';
require_once 'gestionEntradas/gestionFormularios.php';

	if(isset($_SESSION["formulario"])){
// RECOGER LOS DATOS Y ANULAR LOS DATOS DE SESIÓN (FORMULARIO Y ERRORES)
	// Recogemos los datos del formulario
		$nuevoCliente= $_SESSION["formulario"];
		unset($_SESSION["formulario"]);
		
		if(isset($_SESSION["errores"])){
			unset($_SESSION["errores"]);
		}	
	$conexion = crearConexionBD();
	$insercion = insertarCliente($conexion,	$nuevoCliente);
	cerrarConexionBD($conexion);
	
	}else{	
		Header("Location: AnadirClientes_formulario.php");
	}

if (isset($_SESSION["login"])) {
            $login = $_SESSION["login"];
        
    }else{
        $login = 'No se ha iniciado sesión';
    }
	

	
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/formularios.css" type="text/css" />

  <title>Alta de Cliente - Test</title>
  <?php include_once 'cabecera.php';?>
</head>

<body>    
	
	<?php if ($insercion <> "") {
		?><h1>Error al meter los datos en la base de datos.</h1>
			<?php	 echo $insercion;
	}else{?>
	
	<h2>Ciente dado de alta con éxito con los siguientes datos:</h2>
	<div class="box">
		<h1>Datos personales capturados:</h1>
		<hr>
    <div>
            <li>DNI:             <?php echo $nuevoCliente["dni"]; ?>                     </li>
            
            <li>Direccion:       <?php echo $nuevoCliente["direccion"]; ?>               </li>
            <li>Telefono:        <?php echo $nuevoCliente["telefono"]; ?>                </li>
            <li>Nombre:          <?php echo $nuevoCliente["nombre"]; ?>                  </li>
            <li>Apellidos:       <?php echo $nuevoCliente["apellidos"]; ?>           	 </li>
           <?php if(isset($nuevoCliente['fechanac'])){;?>
            <li>Fecha de nacimiento: <?php echo $nuevoCliente["fechanac"]; ?>	 </li>
           <?php } ?>
    </div>
 	<div>
    	<hr>
        <h1>Datos de registro:</h1>
        <hr>
        	<li>Email: <?php echo $nuevoCliente["email"]; ?>    </li>
            <li>Nickname: <?php echo $nuevoCliente["nick"]; ?>    </li>
            <li>Password: <?php echo $nuevoCliente["pass"]; ?>    </li>
     
         <a class="button" href="AnadirClientes_formulario.php">Agregar más clientes</a> 	
   </div>   
   </div> 
       <?php } ?>

    
 

	
	<?php include_once 'pie.php';?>


</body>
</html>
